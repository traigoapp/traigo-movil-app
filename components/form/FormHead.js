import React, {Component} from 'react';
import { StyleSheet, Text, View, Dimensions} from 'react-native';

import FacebookLogo from '../logos/FacebookLogo';
import EmailLogo from '../logos/EmailLogo';

export default class FormHead extends Component {

  constructor(props, env){
    super(props, env);

  }

  onPressButton(){

     
  }

  render() {
    return (
      <View>
        <View style = {[styles.head, {backgroundColor: '#ffffff', width: (Dimensions.get('window').width * 0.8), height: 50 }]} onPress = {this.onPressButton.bind(this)}>
          <Text style = {styles.textButton}>
            {this.props.text}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  head: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButton: {
    color: '#363530',
    fontSize: 18,
    textAlign: 'center',
    flex: 1,
    fontWeight: 'bold' 
  },

  icon: {
    width: 60
  }

});
