import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Image} from 'react-native';

import FacebookLogo from '../logos/FacebookLogo';
import EmailLogo from '../logos/EmailLogo';

export default class SocialButton extends Component {

  constructor(props, env){
    super(props, env);

  }

  onPressButton(){
    this.props.next();
     
  }

  icon(){

    if(this.props.icon == 'add'){
      return (<Image
        style = {styles.icon}
            source = {require('../../resources/socialIcons/location.png')}
            resizeMode = 'center'
          />)
    }if(this.props.icon == 'location'){
      return (<Image
            style = {styles.icon}
            source = {require('../../resources/socialIcons/add.png')}
            resizeMode = 'center'
          />)
    }

    
  }

  render() {
    return (
      <View>

        

        <TouchableOpacity style = {[(this.props.only != 'true' ? styles.button: styles.buttonOnly), {backgroundColor: '#ffffff', width: (Dimensions.get('window').width * 0.8), height: 50 }]} onPress = {this.onPressButton.bind(this)}>
          
          {this.icon()}

          <Text style = {styles.textButton}>
            {this.props.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  button: {
    paddingLeft: 20,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  buttonOnly: {
    paddingLeft: 20,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButton: {
    marginLeft: 5,
    color: '#363530',
    fontSize: 18,
    textAlign: 'left',
    flex: 1
  },

  icon: {
    width: 30,
    height: 30
  }

});
