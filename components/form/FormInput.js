import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions, TextInput} from 'react-native';

import FacebookLogo from '../logos/FacebookLogo';
import EmailLogo from '../logos/EmailLogo';

export default class FormInput extends Component {

  constructor(props, env){
    super(props, env);
    this.state = {text: ''}
  }

  render() {
    return (
      <View>
        <TextInput
          value = {this.state.text}
          style = {[(this.props.pos == 'center' ? styles.center: styles.finish), {backgroundColor: '#ffffff', width: (Dimensions.get('window').width * 0.8), height: 60 }]}
          placeholder = {this.props.text}
          placeholderTextColor = '#01a9da'
          underlineColorAndroid='transparent'
          keyboardType = {this.props.type}
          onChangeText = {(text) => {this.setState({text})}}
          >
        </TextInput>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  finish: {
    paddingLeft: 20,
    borderBottomRightRadius: 15,
    borderBottomLeftRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  center: {
    paddingLeft: 20,
    alignItems: 'center',
    flexDirection: 'row'
  }

});
