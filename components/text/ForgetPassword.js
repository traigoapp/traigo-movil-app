import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions, Image} from 'react-native';

export default class BasicInput extends Component {

  constructor(props){
    super(props);

    this.recuperarContrasenia = this.recuperarContrasenia.bind(this);

  }

  recuperarContrasenia(){
    console.log("Funciona");
  }

  render() {
    return (
      <View style = {styles.container}>

      <Image
          style = {{width: 30, height: 30}}
          source = {require('../../resources/socialIcons/lock.png')}
          resizeMode = 'center'
        />

        <Text
        onPress = {this.recuperarContrasenia}
        style = {styles.text}>

          Olvidé mi contraseña
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 8,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  text: {
    color: '#fff'
  }

});
