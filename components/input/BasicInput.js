import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions} from 'react-native';

export default class BasicInput extends Component {
  render() {
    return (
      <View>
        <TextInput
        placeholder = {this.props.text}
        placeholderTextColor = '#8b8b8b'
        underlineColorAndroid='transparent'
        keyboardType = {this.props.type}
        onChangeText = {(text) => {this.setState({text})}}
        style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 60 }]}>

        </TextInput>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  input: {
    textAlign: 'center',
    color: '#363530', 
    fontSize: 25,
    borderRadius: 15,
    backgroundColor: '#fff'
  }

});
