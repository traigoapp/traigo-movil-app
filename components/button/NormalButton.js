import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions} from 'react-native';

import FacebookLogo from '../logos/FacebookLogo';
import EmailLogo from '../logos/EmailLogo';

export default class NormalButton extends Component {

  constructor(props, env){
    super(props, env);


  }

  render() {
    return (
      <View>
        <View style = {[styles.button, {backgroundColor: this.props.color, width: (Dimensions.get('window').width * 0.55), height: 50 }]}>
          <Text style = {styles.textButton}>
            {this.props.text}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButton: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    flex: 1
  },

  logoBox: {
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  }

});
