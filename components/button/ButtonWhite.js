import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions} from 'react-native';



export default class ButtonWhite extends Component {

  constructor(props, env){
    super(props, env);
  }


  social(){
    if(this.props.social == 'facebook'){
      return <FacebookLogo/>
    }else if(this.props.social == 'email'){
      return <EmailLogo/>
    }
  }

  render() {
    return (
      <View>
        <View style = {[styles.button, {backgroundColor: 'white', width: (Dimensions.get('window').width * 0.8), height: 60 }]}>
          
          <Text style = {styles.textButton}>
            {this.props.text}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },

  textButton: {
    color: '#01a9da',
    fontSize: 15,
    textAlign: 'center',
    flex: 1
  }

});
