import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions} from 'react-native';

import FacebookLogo from '../logos/FacebookLogo';
import EmailLogo from '../logos/EmailLogo';

export default class SocialButton extends Component {

  constructor(props, env){
    super(props, env);

    if(this.props.social == 'facebook'){
      this.state = {color: '#3c5192', fondoLogo: '#3c4387'};
    }else if(this.props.social == 'email'){
      this.state = {color: '#01a9da', fondoLogo: '#00a3d8'};
    }

  }


  social(){
    if(this.props.social == 'facebook'){
      return <FacebookLogo/>
    }else if(this.props.social == 'email'){
      return <EmailLogo/>
    }
  }

  render() {
    return (
      <View>
        <View style = {[styles.button, {backgroundColor: this.state.color, width: (Dimensions.get('window').width * 0.55), height: 60 }]}>
          
          <View style = { [styles.logoBox, {width: 60, height: 60, backgroundColor: this.state.fondoLogo}] }>
           {this.social()}
          </View>

          <Text style = {styles.textButton}>
            {this.props.text}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButton: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
    flex: 1
  },

  logoBox: {
    borderBottomLeftRadius: 15,
    borderTopLeftRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  }

});
