import React, {Component} from 'react';
import { StyleSheet, View, Image} from 'react-native';

export default class Logo extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style = {[styles.center, {width: this.props.width, height: this.props.height}]}>
        <Image
          source = {require('../../resources/logos/only.png')}
          resizeMode = 'center'
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
