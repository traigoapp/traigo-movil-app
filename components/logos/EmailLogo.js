import React, {Component} from 'react';
import { StyleSheet, View, Image} from 'react-native';

export default class EmailLogo extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      
        <Image
          source = {require('../../resources/socialIcons/email.png')}
          resizeMode = 'center'
        />
    
    );
  }
}

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    justifyContent: 'center'
  },
});
