import React, {Component} from 'react';
import { StyleSheet, View, Image} from 'react-native';

export default class FacebookLogo extends Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      
        <Image
          source = {require('../../resources/socialIcons/facebook.png')}
          resizeMode = 'center'
        />
    
    );
  }
}

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
});
