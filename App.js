import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, Dimensions} from 'react-native';


import Welcome from './pages/Welcome'
import AddressSelection from './pages/AddressSelection'
import Register from './pages/Register'
import Login from './pages/Login'
import MapRegister from './pages/MapRegister'
import Home from './pages/Home'
import HomeRider from './pages/HomeRider'
import MenuClient from './pages/MenuClient'
import MenuRider from './pages/MenuRider'
import Loquesea from './pages/Loquesea'
import ConfirmDeseo from './pages/ConfirmDeseo'
import Categoria from './pages/Categoria'
import Tienda from './pages/Tienda'
import SelectDirection from './pages/SelectDirection'
import LoginRider from './pages/LoginRider'
import PedidosRider from './pages/PedidosRider'
import PedidoEmpresarial from './pages/PedidoEmpresarial'
import PedidoDeseo from './pages/PedidoDeseo'
import PedidoTienda from './pages/PedidoTienda'
import PedidoEncomienda from './pages/PedidoEncomienda'
import CuentaRider from './pages/CuentaRider'
import AgregarFactura from './pages/AgregarFactura'
import PedidosCliente from './pages/PedidosCliente'
import InfoPedidoDeseo from './pages/InfoPedidoDeseo'
import InfoPedidoEncomienda from './pages/InfoPedidoEncomienda'
import InfoPedidoTienda from './pages/InfoPedidoTienda'
import Producto from './pages/Producto'
import Carrito from './pages/Carrito'
import Checkout from './pages/Checkout'

import {createStackNavigator} from 'react-navigation'

import Mandados from './pages/Mandados'
export default createStackNavigator(
  {
    welcome: Welcome,
    addressSelection: AddressSelection,
    register: Register,
    login: Login,
    mapRegister: MapRegister,
    home: Home,
    homeRider: HomeRider,
    menuClient: MenuClient,
    loquesea: Loquesea,
    mandados: Mandados, 
    selectDirection: SelectDirection,
    confirmDeseo: ConfirmDeseo,
    categoria: Categoria,
    tienda: Tienda,
    loginRider: LoginRider,
    menuRider: MenuRider,
    pedidosRider: PedidosRider,
    pedidoEmpresarial: PedidoEmpresarial,
    pedidoDeseo: PedidoDeseo,
    pedidoTienda: PedidoTienda,
    pedidoEncomienda: PedidoEncomienda,
    cuentaRider: CuentaRider,
    agregarFactura: AgregarFactura,
    pedidosCliente: PedidosCliente,
    infoPedidoDeseo: InfoPedidoDeseo,
    infoPedidoEncomienda: InfoPedidoEncomienda,
    producto: Producto,
    carrito: Carrito,
    checkout: Checkout,
    infoPedidoTienda: InfoPedidoTienda,
  },
  {
    headerMode: 'none',
    initialRouteName: 'welcome',
  }
);

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});