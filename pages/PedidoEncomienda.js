import React, {Component} from 'react';
import {ActivityIndicator, Alert, RefreshControl, AsyncStorage, Switch, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import {StackActions, NavigationActions} from 'react-navigation'
import Splash from './Splash';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import MapView from 'react-native-maps';

const {width, height} = Dimensions.get('window');

const popAction = StackActions.pop({
  n: 1,
});

const latDelta = 0.0922;
const lonDelta = 0.0421;

export default class PedidoDeseo extends Component {

  constructor(props){
    super(props);

    this.state = {cliente: {}, costos: {}, rider: null, pedido: null, minutes: 0, siguienteEstado: null, loading: false, paradas: [], direccionrecogida: null, direccionentrega: null,
      region: {
        latitude: null,
        longitude: null,
        latitudeDelta: null,
        longitudeDelta: null
      },

    };
  }

  _onRefresh = () => {
    this.setState({refreshing: true});

    fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidoencomienda?pedido=' + this.state.pedido.idpedido_encomienda)
    .then((response) => response.json())
    .then((responseJson) => {
      var estado = responseJson.estado;
      var pedido = this.state.pedido;
      pedido.estado = estado;
      this.setState({refreshing: false});
      this.setState({pedido: pedido}, () => {
        this.refrescarPedido();
      });      
    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión');
    });

  }


refrescarPedido(){

  var now = new Date();
  var pedidoDate = new Date(this.state.pedido.fecha);
  var diffMs = (now - pedidoDate);
  var minutes = Math.floor((diffMs/1000)/60);
  this.setState({minutes: minutes});

  var estadoActual = this.state.pedido.estado;
  var siguienteEstado;
  if(estadoActual == 'Tomado por el Rider'){
    siguienteEstado = 'Rider en el sitio de recogida';
  }else if(estadoActual == 'Rider en el sitio de recogida'){
    siguienteEstado = 'Rider recorriendo paradas';
  }else if(estadoActual == 'Rider recorriendo paradas'){
    siguienteEstado = 'Paradas recorridas';
  }else if(estadoActual == 'Paradas recorridas'){
    siguienteEstado = 'Rider en el sitio de entrega';
  }else if(estadoActual == 'Rider en el sitio de entrega'){
    siguienteEstado = 'Entregado';
  }else if(estadoActual == 'Entregado'){
    siguienteEstado = 'Pedido completado';
  }else if(estadoActual == 'Error'){
    siguienteEstado = 'Pedido con error';
  }else if(estadoActual == 'Cancelado'){
    siguienteEstado = 'Pedido cancelado';
  }

  fetch('http://traigo.com.co:8000/servicios/obtenerclientepedidoencomienda?pedido=' + this.state.pedido.idpedido_encomienda)
    .then((response) => response.json())
    .then((responseJson) => {

      this.setState({refreshing: false});
      this.setState({cliente: responseJson});    
    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión: ' + error);
    }); 

  fetch('http://traigo.com.co:8000/servicios/obtenerparadaspedido?pedido=' + this.state.pedido.idpedido_encomienda)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({paradas: responseJson});      
  })
  .catch((error) => {
    this.setState({refreshing: false});
    alert('Ha ocurrido un error con la conexión');
  });

  fetch('http://traigo.com.co:8000/servicios/obtenerdireccionesencomienda?pedido=' + this.state.pedido.idpedido_encomienda)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({direccionrecogida: responseJson[0], direccionentrega: responseJson[1]}, () => {
      if(this.state.pedido.estado == 'Tomado por el Rider'){
        this.setState({region: {
          latitude: this.state.direccionrecogida.latitude,
          longitude: this.state.direccionrecogida.longitude,
          latitudeDelta: latDelta,
          longitudeDelta: lonDelta
        }})
      }else{
        this.setState({region: {
          latitude: this.state.direccionentrega.latitude,
          longitude: this.state.direccionentrega.longitude,
          latitudeDelta: latDelta,
          longitudeDelta: lonDelta
        }})
      }
    });      
  })
  .catch((error) => {
    this.setState({refreshing: false});
    alert('Ha ocurrido un error con la conexión: ' + error);
  });

  this.setState({siguienteEstado: siguienteEstado, loading: false});
}

cambiarEstadoPedido(){
  var siguienteEstado;
  var estadoActual;

  fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidoencomienda?pedido=' + this.state.pedido.idpedido_encomienda)
  .then((response) => response.json())
  .then((responseJson) => {
    var estado = responseJson.estado;
    var pedido = this.state.pedido;
    pedido.estado = estado;
    this.setState({pedido: pedido}, () => {
      var estadoActual = estado;
      if(estadoActual == 'Tomado por el Rider'){
        siguienteEstado = 'Rider en el sitio de recogida';
      }else if(estadoActual == 'Rider en el sitio de recogida'){
        siguienteEstado = 'Rider recorriendo paradas';
      }else if(estadoActual == 'Rider recorriendo paradas'){
        siguienteEstado = 'Paradas recorridas';
      }else if(estadoActual == 'Paradas recorridas'){
        siguienteEstado = 'Rider en el sitio de entrega';
      }else if(estadoActual == 'Rider en el sitio de entrega'){
        siguienteEstado = 'Entregado';
      }else if(estadoActual == 'Entregado'){
        siguienteEstado = 'Pedido completado';
      }else if(estadoActual == 'Error'){
        siguienteEstado = 'Pedido con error';
      }else if(estadoActual == 'Cancelado'){
        siguienteEstado = 'Pedido cancelado';
      }

      
      var data = new FormData();
      data.append('cedularider', this.state.rider.cedula);
      data.append('idpedidoencomienda', pedido.idpedido_encomienda);
      data.append('estado', siguienteEstado);

      fetch('http://traigo.com.co:8000/servicios/cambiarestadopedidoencomienda', {

        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: data
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
          this.setState({loading: false});
        }else{
          pedido.estado = siguienteEstado;
          this.setState({pedido: pedido}, this.refrescarPedido());
        }
      })
      .catch((error) => {

        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión: ' + error);
      });

    });      
  })
  .catch((error) => {
    this.setState({loading: false});
    alert('Ha ocurrido un error con la conexión: ' + error);
  });
}

componentWillMount(){
  this.setState({rider: this.props.navigation.getParam('rider', 'Ninguno'), pedido: this.props.navigation.getParam('pedido', 'Ninguno')},this.refrescarPedido);
  fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });
}

marker(){
  return {
    latitude: this.state.region.latitude,
    longitude: this.state.region.longitude
  }
}

renderFactura(item, index){
    return(
       <TouchableOpacity>

            <View
              style = {styles.imagen}>
            </View>

          </TouchableOpacity>
    );
  }

render() {
  return(


    <View style = {styles.container}>
    <View style = {styles.header}>

    <View style = {styles.divileft}>

    <TouchableOpacity
    onPress = {() => {
      this.props.navigation.dispatch(popAction);
    }}>
    <Image
    style = {{width: 30, height: 30}}
    source = {require('../resources/socialIcons/back.png')}
    resizeMode = 'center'
    />
    </TouchableOpacity>


    </View>

    <View style = {styles.divi}>
    <Image
    style = {{width: width * 0.25, height: 50}}
    source = {require('../resources/logos/traigoblack.png')}
    resizeMode = 'center'
    />
    </View>

    <View style = {styles.diviright}>

    <Text style = {styles.encurso}>
    Hola, {this.state.rider == null ? 'Sin nombre' : this.state.rider.nombre}
    </Text>


    </View>

    </View>



    <ScrollView style = {styles.scroll}
    contentContainerStyle = {{alignItems: 'center'}}
    refreshControl={
      <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh}
      />}>
        <View style = {styles.activate}>
        

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          TIEMPO:
          </Text>

          {60 - this.state.minutes < 0 ? 
            <View>
              <Text style = {{fontSize: 20, fontWeight: 'bold', color: 'red', marginLeft: 5}}>
                RETRASO {this.state.minutes - 60} MIN
              </Text>
            </View>

           :
            <View>
              <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#41aa54', marginLeft: 5}}>
                  {60- this.state.minutes} MIN
              </Text>
            </View>

         }

        </View>

        <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, alignSelf: 'center', marginVertical: 5}}>
                   </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          CLIENTE:
          </Text>        

        </View>

         <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.cliente.nombre}
          </Text>          
        </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          TELÉFONO:
          </Text>        

        </View>

         <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.cliente.telefono}
          </Text>          
        </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          MANDADO:
          </Text>        

        </View>

         <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.pedido.mandado}
          </Text>          

        </View>

      </View>

      <View style = {styles.resumen}>
        <Text style = {styles.textoResumen}>
          Resumen del pedido
        </Text>
      </View>

      {this.state.pedido.estado == 'Tomado por el Rider' ? <View style = {styles.paso}>
        <View style = {styles.numero}>
          <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
            1
          </Text>
        </View>

        <View style = {styles.infoPaso}>
          <Text style = {[styles.textoPaso, {fontWeight: 'bold'}]}>
            Dirígite a la direccion de recogida:
          </Text>
          <Text style = {[styles.textoPaso]}>
            {this.state.direccionrecogida != null ? this.state.direccionrecogida.direccion + ' | ' + this.state.direccionrecogida.notas : null}
          </Text>
        </View>

      </View> : null}

      

      {this.state.pedido.estado == 'Rider en el sitio de recogida' ? <View style = {styles.paso}>
        <View style = {styles.numero}>
          <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
            2
          </Text>
        </View>

        <View style = {styles.infoPaso}>
          <View style = {{flexDirection: 'row'}}>
            <Text style = {[styles.textoPaso]}>
              Recoge lo solicitado por el cliente. (Leer MANDADO)
            </Text>

          </View>
          

        </View>

      </View> : null}

      {this.state.pedido.estado == 'Rider recorriendo paradas' ? <View style = {styles.paso}>
        <View style = {styles.numero}>
          <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
            3
          </Text>
        </View>

        <View style = {styles.infoPaso}>
          <View>
            <Text style = {[styles.textoPaso, {fontWeight: 'bold'}]}>
              Paradas a recorrer: 
            </Text>

            {this.state.paradas.map((data, i) => {
                
                return(
                <View key = {i}>
                  <Text style = {[styles.textoPaso,]}>
                      {data.direccion + ' | ' + (data.notas != '' ? data.notas : 'Sin notas')} 
                  </Text>
                </View>)
              })}

          </View>
          

        </View>

      </View> : null}

      {this.state.pedido.estado == 'Paradas recorridas' ? <View style = {styles.paso}>
        <View style = {styles.numero}>
          <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
            4
          </Text>
        </View>

        <View style = {styles.infoPaso}>
          <View>
            <Text style = {[styles.textoPaso, {fontWeight: 'bold'}]}>
              Dirígite a la direccion de entrega:
            </Text>

            <Text style = {[styles.textoPaso]}>
              {this.state.direccionentrega != null ? this.state.direccionentrega.direccion + ' | ' + this.state.direccionentrega.notas : null}
            </Text>
          </View>
          

        </View>

      </View> : null}

      {this.state.pedido.estado == 'Rider en el sitio de entrega' ? <View style = {styles.paso}>
        <View style = {styles.numero}>
          <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
            5
          </Text>
        </View>

         <View style = {styles.infoPaso}>
          <Text style = {[styles.textoPaso, {fontWeight: 'bold'}]}>
            Cobrar al cliente
          </Text>

          <Text style = {[{fontSize: 20, color: '#363530'}]}>
            {this.state.pedido.costo_envio + this.state.paradas * this.state.costos.costo_paradas_mandado}
          </Text>
        </View>

      </View> : null}

      {this.state.pedido.estado != 'Entregado' && this.state.pedido.estado != 'Error' ?

           (this.state.region.latitude != null? <MapView
            provider={MapView.PROVIDER_GOOGLE}
            style = {[styles.map]}
            initialRegion = {this.state.region}
            onRegionChange={this.onRegionChange}
            >

            {this.state.pedido.estado == 'Tomado por el Rider' || this.state.pedido.estado == 'Paradas recorridas'?
            <MapView.Marker
            coordinate={this.marker()}
            pinColor='#50dddd'
            /> : null}

            {this.state.pedido.estado == 'Rider recorriendo paradas' ? 
              this.state.paradas.map((data, i) => {
                
                return(
                <View key = {i}>
                <MapView.Marker
                coordinate={{latitude: data.latitude, longitude: data.longitude}}
                pinColor='#50dddd'
                /></View>)
              })
            : null}

            </MapView> : null)

        : null
      }


      {this.state.pedido.estado == 'Error' || this.state.pedido.estado == 'Entregado' || this.state.pedido.estado == 'Cancelado' ? null :
      <Text style = {{width: width * 0.8, textAlign: 'left', fontSize: 20, fontWeight: 'bold', color: '#363530', marginTop: 10}}>
        Marcar pedido como:
      </Text>
      }

      {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

        

      <TouchableOpacity style = {[styles.botonEstado, {backgroundColor: (this.state.pedido.estado == 'Cancelado' || this.state.pedido.estado == 'Entregado' || this.state.pedido.estado == 'Error' ? '#afafaf' : '#41aa54')}]} onPress = {() => {
        
        this.setState({loading: true});
        if( (this.state.pedido.estado == 'Rider obteniendo productos' && this.state.facturas.length == 0) || this.state.pedido.estado == 'Error' || this.state.pedido.estado == 'Entregado' || this.state.pedido.estado == 'Cancelado'){
        Alert.alert(
          'Ups!',
          'No puedes interactuar con el pedido',
          [
          {text: 'Okey', onPress: () => {
            this.setState({loading: false});
           
          }, style: 'cancel'},
          ],
          { cancelable: false }
          )
      }else{
          Alert.alert(
          'Confirmar',
          'Desea cambiar el estado del pedido a: ' + this.state.siguienteEstado,
          [
          {text: 'Confirmar', onPress: () => this.cambiarEstadoPedido(), style: 'cancel'},
          {text: 'Cancelar', onPress: () => {this.setState({loading: false})}, style: 'cancel'},
          ],
          { cancelable: true }
          )
      }
        
      }}>
        <Text style = {styles.TextBotonEstado}>
          {this.state.siguienteEstado == null ? 'Nothing' :this.state.siguienteEstado.toUpperCase()}
        </Text>
      </TouchableOpacity>

      </ScrollView>

      

      </View>



      )
}
}

    const styles = StyleSheet.create({
      container: {
        flex:1,
        alignItems: 'flex-start',
        paddingTop: Platform.OS === 'ios'  ? 20 : 0,
        backgroundColor: '#ffffff'
      },

      header: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 60,
        width: width,
        alignItems: 'center',
        justifyContent: 'space-between'
      },

      traigo: {
        color: '#363530',
        fontSize: 25,
        fontWeight: 'bold'
      },

      divi: {
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      divileft: {
        paddingLeft: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      diviright: {
        paddingRight: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
      },

      scroll: {
        backgroundColor: '#fff',
        flex: 1,
        width: width,
        paddingBottom: 5,
      },

      activate: {
        marginTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: width * 0.8
      },

      tipopedido: {
        width: width * 0.8,
        flexDirection: 'row',
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      terminos: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
      },

      flatlist: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        width: width,
      },

      pedido: {
        width: width * 0.8,
        height: height * 0.35,
        borderRadius: 25,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginVertical: 5,
        borderColor: '#afafaf',
        borderWidth: 1
      },

      infotienda: {
        height: 50,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
      },

      titulosucursal: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'left',

      },

      tiempoentrega: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'right',
        flex: 1,
      },

      partner: {
        marginLeft: 2,
        width: width * 0.25,
        height: 25,
        borderRadius: 25,
        backgroundColor: '#363530',
        justifyContent: 'center',
        alignItems: 'center'
      },

      datafono: {
        paddingLeft: 10,
        color: '#afafaf',
        fontSize: 15,
        textAlign: 'left'
      },

      precio: {
        marginTop: -5,
        paddingLeft: 10,
        color: '#363530',
        fontSize: 20,
        textAlign: 'left'
      },

      destino: {
        paddingLeft: 30,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row'
      },

      textoDestino: {
        fontSize: 15,
        color: '#363530',
        fontWeight: 'bold',
        textAlign: 'left'
      },

      textoDireccion: {
        marginTop: -5,
        fontSize: 15,
        color: '#afafaf',
        textAlign: 'left'
      },

      retenido: {
        marginTop: -10,
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#fba312',
        justifyContent: 'center',
        alignItems: 'center'
      },

      infoganancia: {
        height: 30,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start',
      },

      resumen: {
        width: width * 0.8,
        height: 40,
        backgroundColor: '#41aa54',
        borderRadius: 25,
        marginVertical: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },

      textoResumen: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center'
      },

      paso: {
        width: width * 0.8,
        flexDirection: 'row',
        marginBottom: 10
      },

      numero: {
        width: 30,
        height: 30,
        backgroundColor: '#ff8000',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5,
      },

      infoPaso:{
        width: width * 0.8
      },

      textoPaso: {
        color: '#363530',
        fontSize: 18
      },

      botonEstado: {
        width: width * 0.8,
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20

      },

      TextBotonEstado: {
        fontSize: 25,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
      },

      map: {
        width: width,
        height: height * 0.3,
        
      },

      botonFacturas: {
        width: width * 0.8,
        backgroundColor: '#ff8000',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50
      },
      imagen: {
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#bebebe',
        width: 30,
        height: 30,
        marginBottom: 2,
      },

    });
