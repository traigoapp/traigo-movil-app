import React, {Component} from 'react';
import {ImageBackground, RefreshControl, FlatList, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});

export default class Categoria extends Component {
	constructor(props){
	    super(props);
	    this.state = {tiendas: [], usuario: null, direccion: null, categoria: null};
	  

  	}

  	componentWillMount(){
  		this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    	this.setState({direccion: this.props.navigation.getParam('direccion', null)})
    	this.setState({categoria: this.props.navigation.getParam('categoria', null)}, () => {
    		fetch('http://traigo.com.co:8000/servicios/obtenertiendas?categoria=' + this.state.categoria.idcategoria)
		      .then((response) => response.json())
		      .then((responseJson) => {
		        this.setState({tiendas: responseJson});
		      })
		      .catch((error) => {
		        alert(error);
		      });
    	})
    	
  	}

  	_onRefresh = () => {
    this.setState({refreshing: true});
    fetch('http://traigo.com.co:8000/servicios/obtenertiendas?categoria=' + this.state.categoria.idcategoria)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({tiendas: responseJson});
        this.setState({refreshing: false});
      })
      .catch((error) => {
        alert(error);
      });
	}


  	renderTienda(item, index){

  		return(
  			 <ImageBackground
              style = {styles.tienda}
              imageStyle = {{borderRadius: 8}}
              source = {{uri: 'http://traigo.com.co:8000/static/img/tienda/' + item.imagen}}>

              	<TouchableOpacity style = {styles.trans}
              		onPress = {() => {
              			if(item.abierto)
              				this.props.navigation.navigate('tienda', {usuario: this.state.usuario, direccion: this.state.direccion, tienda: item});
              		}}
              		>
              		{(item.abierto) ? <View style = {[styles.estado, {backgroundColor: '#53c678'}]}>
              					<Text style = {styles.textoEstado}>
              						ABIERTO
              					</Text>
              				</View> 
              				:
           					<View style = {[styles.estado, {backgroundColor: 'red'}]}>
              					<Text style = {styles.textoEstado}>
              						CERRADO
              					</Text>
              				</View>}

              		<Text style = {styles.nombre}>
              			{item.nombre.toUpperCase()}
              		</Text>

              		<Text style = {styles.infotienda}>
              			{item.tiempo_entrega + ' MIN - $' + item.costo_envio + ' COP'}
              		</Text>

              	</TouchableOpacity>


            </ImageBackground>
  		)
  	}

  	render(){
  		return(
  		<View style = {styles.container}>
  			<View style = {styles.header}>

	          <View style = {styles.divi2}>

	            <TouchableOpacity
	              onPress = {() => {
	                Keyboard.dismiss();
	                this.props.navigation.dispatch(popAction);
	              }}
	            >
	              <Image
	              style = {{width: 30, height: 30}}
	              source = {require('../resources/socialIcons/back.png')}
	              resizeMode = 'center'
	              />
	            </TouchableOpacity>

	          
	          </View>

	           <View style = {styles.divi}>
	             <Text style = {styles.title}>
	                {this.state.categoria == null ? 'Sin nombre' : this.state.categoria.nombre}
	             </Text>
	          </View>
	          
	        </View>

  			<View style = {styles.flatlist}>

	            <FlatList 
	            refreshControl={
			        <RefreshControl
			          refreshing={this.state.refreshing}
			          onRefresh={this._onRefresh}
			        />}
	              data = {this.state.tiendas}
	              renderItem = {({item, index}) => this.renderTienda(item, index)}
	              keyExtractor = {(item, index) => index.toString()}
	            />


          </View>
  		</View>
  		)
  	}

}


const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'flex-start',
    paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    backgroundColor: '#ffffff'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  title: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  flatlist: {
  	flex: 1,
  	backgroundColor: '#fff',
  	width: width
  },

  tienda: {
  	marginLeft: 4,
  	marginRight: 4,
  	marginVertical: 2,
  	width: width - 8,
  	height: height * 0.3
  },

  trans: {
  	borderRadius: 8,
  	width: width - 8,
  	height: height * 0.3,
  	backgroundColor: 'rgba(52, 52, 52, 0)',
  	alignItems: 'center',
  	justifyContent: 'center'
  },

  estado: {
  	width: width * 0.22,
  	height: 20,
  	borderRadius: 50,
  	justifyContent: 'center',
  	alignItems: 'center'
  },

  textoEstado: {
  	fontSize: 15,
  	color: '#fff',
  	fontWeight: 'bold'
  },

  nombre: {
  	width: width * 0.7,
  	textAlign: 'center',
  	color: '#fff',
  	fontWeight: 'bold',
  	fontSize: 25
  },

  infotienda: {
  	color: '#fff',
  	fontSize: 15
  },

});