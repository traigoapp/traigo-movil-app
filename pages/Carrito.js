import React, {Component} from 'react';
import {AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class Carrito extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, carrito: [], direccion: null, total: 0, costos: {}};

  }

  componentWillMount () {
    var usuario = this.props.navigation.getParam('usuario', null);
    var direccion = this.props.navigation.getParam('direccion', null);
    var carrito = this.props.navigation.getParam('carrito', null);

    this.setState({
      usuario: usuario,
      direccion: direccion,
      carrito: carrito
    }, () => {
      var total = 0;
      for(var i = 0; i < carrito.length; i++){
        total += carrito[i].total;
      }

      this.setState({total: total});

    });

    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });

  }



  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/back.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Carrito
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

          {this.state.carrito.map((data, index) => {
            return(
              <View key = {index}>
                  <View style = {styles.row}>
                    <TouchableOpacity style = {styles.remove} onPress = {() => {
                      var carrito = this.state.carrito;
                      carrito.splice(index, 1);
                      var total = 0;
                      for(var i = 0; i < carrito.length; i++){
                        total += carrito[i].total;
                      }

                      this.setState({total: total});

                      this.setState({carrito: carrito}, () => {
                        this.props.navigation.state.params.actualizarCarrito({carrito: this.state.carrito});
                      })
                      
                    }}>
                      <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#fff'}}>
                        {'x'}
                      </Text>
                    </TouchableOpacity>

                    <Image
                      style = {styles.producto}
                      source = {{uri: 'http://traigo.com.co:8000/static/img/producto/' + data.imagen}}>
                    </Image>

                    <View style = {styles.informacionRow}>
                      <Text style = {styles.nombreProducto}>
                        {data.nombre}
                      </Text>

                      <Text style = {styles.precioProducto}>
                        ${data.total} COP
                      </Text>

                    </View>
                  </View>

                   <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, alignSelf: 'center'}}>
                   </View>

              </View>
            )
          })}

         <View style = {styles.resumen}>

          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'flex-end'}}>
            <Text style = {styles.total}>
              Total productos:
            </Text>

             <Text style = {styles.totalnum}>
              {' $' + this.state.total}
            </Text>
          </View>

          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'flex-end', marginTop: -8}}>
            <Text style = {styles.total}>
              Costo de envío:
            </Text>

            <Text style = {styles.totalnum}>
              {' $' + this.state.costos.costo_envio_cliente}
            </Text>
          </View>

          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'flex-end', marginBottom: 10, marginTop: -8}}>
            <Text style = {styles.total}>
              Total:
            </Text>

            <Text style = {styles.totalnum}>
              {' $' + (this.state.total + this.state.costos.costo_envio_cliente)}
            </Text>
          </View>

          <TouchableOpacity style = {styles.revisar}
            onPress = {() => {
                if(this.state.carrito.length != 0){
                  this.props.navigation.navigate('checkout',
                  {
                    totalapagar: (this.state.costos.costo_envio_cliente + this.state.total),
                    usuario: this.state.usuario,
                    direccion: this.state.direccion,
                    carrito: this.state.carrito
                  })
                }else{
                  alert('Ups! Debes tener al menos un producto, regresa a la tienda');
                }
            }}
          >
            <Text style = {styles.textoBoton}>
              Continuar al checkout
            </Text>
          </TouchableOpacity>

        </View>

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
    flexDirection: 'row'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  remove: {
    paddingTop: 2,
    flexDirection: 'row',
    width: 30,
    height: 30,
    borderRadius: 25,
    backgroundColor: 'red',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  producto: {
    overflow: 'hidden',
    borderRadius: 10,
    backgroundColor: '#bebebe',
    width: height * 0.12,
    height: height * 0.12,
    marginHorizontal: 10
  },

  informacionRow: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1
  },

  nombreProducto: {
    fontSize: 20,
    color: '#afafaf'
  },

  precioProducto: {
    fontSize: 20,
    color: '#53c678'
  },

  resumen: {
    width: width * 0.9,
    paddingVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  total: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'right',
  },

  totalnum: {
    fontSize: 25,
    textAlign: 'right',
  },

  revisar: {
    width: width * 0.9,
    borderRadius: 10,
    backgroundColor: '#53c678',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },

  textoBoton: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  }

});
