import React, {Component} from 'react';
import {ActivityIndicator, Alert, RefreshControl, AsyncStorage, Switch, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import {StackActions, NavigationActions} from 'react-navigation'
import Splash from './Splash';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import MapView from 'react-native-maps';

const {width, height} = Dimensions.get('window');

const popAction = StackActions.pop({
  n: 1,
});

export default class InfoPedidoDeseo extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, pedido: null, rider: null,
      region: {
        latitude: null,
        longitude: null,
        latitudeDelta: null,
        longitudeDelta: null
      },
    }
  }


  _onRefresh = () => {
    this.setState({refreshing: true});

    fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidodeseo?pedido=' + this.state.pedido.idpedido_deseo)
    .then((response) => response.json())
    .then((responseJson) => {
      var estado = responseJson.estado;
      var pedido = this.state.pedido;
      pedido.estado = estado;
      pedido.idrider = responseJson.idrider;
      this.setState({refreshing: false});
      this.setState({pedido: pedido}, () => {
        this.refrescarPedido();
      });      
    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión');
    });

  }


refrescarPedido(){

  var now = new Date();
  var pedidoDate = new Date(this.state.pedido.fecha);
  var diffMs = (now - pedidoDate);
  var minutes = Math.floor((diffMs/1000)/60);
  this.setState({minutes: minutes});

  var estadoActual = this.state.pedido.estado;
  var siguienteEstado;
  if(estadoActual == 'Tomado por el Rider'){
    siguienteEstado = 'Rider obteniendo productos';
  }else if(estadoActual == 'Rider obteniendo productos'){
    siguienteEstado = 'Rider en camino';
  }else if(estadoActual == 'Rider en camino'){
    siguienteEstado = 'Rider donde el cliente';
  }else if(estadoActual == 'Rider donde el cliente'){
    siguienteEstado = 'Entregado';
  }else if(estadoActual == 'Entregado'){
    siguienteEstado = 'Pedido completado';
  }else if(estadoActual == 'Error'){
    siguienteEstado = 'Pedido con error';
  }else if(estadoActual == 'Cancelado'){
    siguienteEstado = 'Pedido cancelado';
  }


  fetch('http://traigo.com.co:8000/servicios/obtenerfacturas?pedido=' + this.state.pedido.idpedido_deseo)
    .then((response) => response.json())
    .then((responseJson) => {
      var facturas = responseJson;
      var total = 0;
      for(var i = 0; i < facturas.length; i++){
        total += facturas[i].valor;
      }
      this.setState({refreshing: false});
      this.setState({facturas: responseJson, totalFacturas: total});    
    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión');
    });

    if(this.state.pedido.idrider != null){
      fetch('http://traigo.com.co:8000/servicios/obtenerrider?cedula=' + this.state.pedido.idrider)
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({loading: false});
          if(responseJson.code){
            alert('Ha ocurrido un error con la conexión');
          }else{
            if(responseJson.length == 0){
              alert('La cédula ingresada no se encuentra registrada');
            }else{
              var rider = responseJson[0];
              var latDelta = 0.0922;
              var lonDelta = 0.0421;

              this.setState({
                rider: rider,
                region: {
                  latitude: rider.latitude,
                  longitude: rider.longitude,
                  latitudeDelta: latDelta,
                  longitudeDelta: lonDelta

                }
              });
              
            }
            
          }
        })
        .catch((error) => {
          this.setState({loading: false});
          alert('Ha ocurrido un error con la conexión');
        });
    }

  this.setState({siguienteEstado: siguienteEstado, loading: false});
}



componentWillMount(){
  this.setState({usuario: this.props.navigation.getParam('usuario', 'Ninguno'), pedido: this.props.navigation.getParam('pedido', 'Ninguno')},this.refrescarPedido);

}

marker(){
  return {
    latitude: this.state.region.latitude,
    longitude: this.state.region.longitude
  }
}

render() {
  return(


    <View style = {styles.container}>
    <View style = {styles.header}>

    <View style = {styles.divileft}>

    <TouchableOpacity
    onPress = {() => {
      this.props.navigation.dispatch(popAction);
    }}>
    <Image
    style = {{width: 30, height: 30}}
    source = {require('../resources/socialIcons/back.png')}
    resizeMode = 'center'
    />
    </TouchableOpacity>


    </View>

    <View style = {styles.divi}>
    <Image
    style = {{width: width * 0.25, height: 50}}
    source = {require('../resources/logos/traigoblack.png')}
    resizeMode = 'center'
    />
    </View>

    <View style = {styles.diviright}>

    <Text style = {styles.encurso}>
    Hola, {this.state.usuario == null ? 'Sin nombre' : this.state.usuario.nombre}
    </Text>


    </View>

    </View>

    <View style = {{backgroundColor: '#e5e5e5', width: width, height: 1, alignSelf: 'center'}}>
                   </View>

    <View style = {{flex: 1}}><ScrollView style = {styles.scroll}
    contentContainerStyle = {{alignItems: 'center'}}
    refreshControl={
      <RefreshControl
      refreshing={this.state.refreshing}
      onRefresh={this._onRefresh}
      />}>
        <View style = {styles.activate}>
        

        <View style = {styles.resumen}>
          <Text style = {styles.textoResumen}>
            Resumen del pedido
          </Text>
        </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          TIEMPO:
          </Text>

          {60 - this.state.minutes < 0 ? 
            <View>
              <Text style = {{fontSize: 20, fontWeight: 'bold', color: 'red', marginLeft: 5}}>
                RETRASO {this.state.minutes - 60} MIN
              </Text>
            </View>

           :
            <View>
              <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#41aa54', marginLeft: 5}}>
                  {60- this.state.minutes} MIN
              </Text>
            </View>

         }

        </View>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, marginVertical: 10}}>
          </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          Tu pedido:
          </Text>        
        </View>

         <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.pedido.deseo}
          </Text>          
        </View>

        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          Dirección de entrega:
          </Text>        
        </View>

         <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.pedido.direccionentrega}
          </Text>          
        </View>
        <View style = {styles.tipopedido}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.pedido.notasdireccionentrega}
          </Text>          
        </View>

        <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, marginVertical: 10}}>
          </View>        

        {this.state.rider != null ? <View style = {styles.inforider}>
          <Text style = {{fontSize: 20, fontWeight: 'bold', color: '#363530'}}>
          Rider
          </Text>        
        </View> : null}


        {this.state.rider != null ? <View style = {styles.inforider}>
         <Image style = {styles.foto}
              imageStyle = {{borderRadius: 10}}
              source = {{uri: 'http://traigo.com.co:8000/static/img/rider/' + this.state.rider.foto_perfil}}>

          </Image>      
        </View> : null}

         {this.state.rider != null ?<View style = {styles.inforider}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.rider.nombre}
          </Text>          
        </View>: null}


         {this.state.rider != null ?<View style = {styles.inforider}>
          <Text style = {{fontSize: 15, color: '#363530'}}>
            {this.state.rider.telefono}
          </Text>          
        </View> : null}

      </View>
     
      

        {this.state.region.latitude != null ? 

          <MapView
            provider={MapView.PROVIDER_GOOGLE}
            style = {[styles.map]}
            initialRegion = {this.state.region}
            onRegionChange={this.onRegionChange}
            >

            <MapView.Marker

            coordinate={this.marker()}
            pinColor='#50dddd'

            />

            </MapView> : null}
      

      </ScrollView></View>

       <View style = {styles.estadoPedido}>
        <View style = {[styles.botonEstado, {backgroundColor: '#ff8000'}]}>
          <Text style = {{width: width, textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: '#fff'}}>
            Estado del pedido
          </Text>
          <Text style = {styles.TextBotonEstado}>
            {this.state.pedido.estado.toUpperCase()}
          </Text>
        </View>
      </View>

      </View>



      )
}
}

    const styles = StyleSheet.create({
      container: {
        flex:1,
        alignItems: 'flex-start',
        paddingTop: Platform.OS === 'ios'  ? 20 : 0,
        backgroundColor: '#ffffff'
      },

      header: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 60,
        width: width,
        alignItems: 'center',
        justifyContent: 'space-between'
      },

      traigo: {
        color: '#363530',
        fontSize: 25,
        fontWeight: 'bold'
      },

      divi: {
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      divileft: {
        paddingLeft: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      diviright: {
        paddingRight: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
      },

      scroll: {
        backgroundColor: '#fff',
        flex: 1,
        width: width,
        paddingBottom: 5,
      },

      activate: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: width * 0.8
      },

      tipopedido: {
        width: width * 0.8,
        flexDirection: 'row',
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      inforider: {
        width: width * 0.8,
        flexDirection: 'row',
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center'
      },

      terminos: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
      },

      flatlist: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        width: width,
      },

      pedido: {
        width: width * 0.8,
        height: height * 0.35,
        borderRadius: 25,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginVertical: 5,
        borderColor: '#afafaf',
        borderWidth: 1
      },

      infotienda: {
        height: 50,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
      },

      titulosucursal: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'left',

      },

      tiempoentrega: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'right',
        flex: 1,
      },

      partner: {
        marginLeft: 2,
        width: width * 0.25,
        height: 25,
        borderRadius: 25,
        backgroundColor: '#363530',
        justifyContent: 'center',
        alignItems: 'center'
      },

      datafono: {
        paddingLeft: 10,
        color: '#afafaf',
        fontSize: 15,
        textAlign: 'left'
      },

      precio: {
        marginTop: -5,
        paddingLeft: 10,
        color: '#363530',
        fontSize: 20,
        textAlign: 'left'
      },

      destino: {
        paddingLeft: 30,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row'
      },

      textoDestino: {
        fontSize: 15,
        color: '#363530',
        fontWeight: 'bold',
        textAlign: 'left'
      },

      textoDireccion: {
        marginTop: -5,
        fontSize: 15,
        color: '#afafaf',
        textAlign: 'left'
      },

      retenido: {
        marginTop: -10,
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#fba312',
        justifyContent: 'center',
        alignItems: 'center'
      },

      infoganancia: {
        height: 30,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start',
      },

      resumen: {
        width: width * 0.8,
        height: 40,
        backgroundColor: '#41aa54',
        borderRadius: 25,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },

      textoResumen: {
        fontSize: 20,
        color: '#fff',
        textAlign: 'center'
      },

      paso: {
        width: width * 0.8,
        flexDirection: 'row',
        marginBottom: 10
      },

      numero: {
        width: 30,
        height: 30,
        backgroundColor: '#ff8000',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 5,
      },

      infoPaso:{
        width: width * 0.8
      },

      textoPaso: {
        color: '#363530',
        fontSize: 18
      },

      botonEstado: {
        width: width,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',

      },

      TextBotonEstado: {
        fontSize: 25,
        color: '#fff',
        fontWeight: 'bold',
        textAlign: 'center'
      },

      map: {
        width: width,
        height: height * 0.3,
        marginTop: 10
      },

      botonFacturas: {
        width: width * 0.8,
        backgroundColor: '#ff8000',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        height: 50
      },

      imagen: {
        borderRadius: 5,
        overflow: 'hidden',
        backgroundColor: '#bebebe',
        width: 30,
        height: 30,
        marginBottom: 2,
      },

      foto: {
      borderRadius: 25,
      backgroundColor: '#bebebe',
      width: 80,
      height: 80,
      marginBottom: 20
    },

    });
