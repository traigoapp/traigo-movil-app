import React, {Component} from 'react';
import {Alert, BackHandler, Picker, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'
import CheckBox from 'react-native-check-box'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class Producto extends Component {

  constructor(props){
    super(props);

    this.state = {producto: null, usuario: null, checked: false, elementos: [], factores: [], adiciones: [], total: 0};

  }

  componentWillMount(){
    this.setState({producto: this.props.navigation.getParam('producto', null)
      , usuario: this.props.navigation.getParam('usuario', null)}, () => {

        fetch('http://traigo.com.co:8000/servicios/obtenerfactoreselementosadiciones?idproducto=' + this.state.producto.idproducto)
        .then((response) => response.json())
        .then((responseJson) => {
          var factores = responseJson[0], elementos = responseJson[1], adiciones = responseJson[2];
          
          for(var i = 0; i < factores.length; i++){
            factores[i].indexSelected = 0;
          }

          for(var i = 0; i < elementos.length; i++){
            elementos[i].isChecked = true;
          }

          for(var i = 0; i < adiciones.length; i++){
            adiciones[i].isChecked = false;
          }

          this.setState({factores: factores, elementos: elementos, adiciones: adiciones, total: this.state.producto.precio});
        })
        .catch((error) => {
          alert(error);
        });
      });
  }  

   componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    handleBackPress = () => {
      this.props.navigation.goBack();
      this.props.navigation.state.params.onSelect({producto: null});
      return true;
    }

  obtenerTotal(){
    var total = this.state.producto.precio;
    for(var i = 0; i < this.state.factores.length; i++){
      total += this.state.factores[i].selecciones[this.state.factores[i].indexSelected].precioseleccion;
    }

    for(var i = 0; i < this.state.elementos.length; i++){
      total += (this.state.elementos[i].isChecked ? this.state.elementos[i].precio : 0);
    }

    for(var i = 0; i < this.state.adiciones.length; i++){
      total += (this.state.adiciones[i].isChecked ? this.state.adiciones[i].precio : 0);
    }

    this.setState({total: total})
  }

  agregarAlCarrito(){
    Alert.alert(
    'Confirmar',
    'Desea agregar el producto al carrito?',
    [
      {text: 'Confirmar', onPress: () => {
        var producto = this.state.producto;
        producto.factores = this.state.factores;
        producto.elementos = [];
        producto.adiciones = [];
        for(var i = 0; i < this.state.elementos.length; i++){
          if(this.state.elementos[i].isChecked){
            producto.elementos.push(this.state.elementos[i]);
          }
        }

        for(var i = 0; i < this.state.adiciones.length; i++){
          if(this.state.adiciones[i].isChecked){
            producto.adiciones.push(this.state.adiciones[i]);
          }
        }

        producto.total = this.state.total;
        this.props.navigation.state.params.onSelect({producto: producto});
        this.props.navigation.goBack();
      }},
      {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
    ],
    { cancelable: false }
  )
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/back.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.tituloHeader}>
                Producto
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

         

          <View style = {styles.infoProducto}>
            <Image
              style = {styles.producto}
              source = {{uri: 'http://traigo.com.co:8000/static/img/producto/' + this.state.producto.imagen}}>
            </Image>

            <Text style = {styles.nombreProducto}>
              {this.state.producto.nombre}
            </Text>

            <Text style = {styles.descripcion_corta}>
              {this.state.producto.descripcion_corta}
            </Text>

          </View>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>


          {this.state.factores.length != 0 ? <View style = {styles.seccion}>
            <Text style = {styles.tituloSeccion}>
              Selecciona los factores
            </Text>

            {this.state.factores.map((data, index) => {
              return(
                <View key = {index}>

                  <Text>
                    {data.nombre}:
                  </Text>

                  <Picker
                    selectedValue={data.indexSelected}
                    style={{height: (Platform.OS === 'ios' ? 100 : 50), width: width * 0.7, borderRadius: 10}}
                    itemStyle = {{height: (Platform.OS === 'ios' ? 100 : 50)}}
                    onValueChange={(itemValue, itemIndex) => {

                      var factores = this.state.factores;
                      factores[index].indexSelected = itemValue;
                      this.setState({factores: factores},this.obtenerTotal)} 
                    }>

                    {data.selecciones.map((dataSeleccion, indexSeleccion) => {
                      return(
                        <Picker.Item key = {indexSeleccion} label={dataSeleccion.nombreseleccion + (dataSeleccion.precioseleccion != 0 ? ' +$' + dataSeleccion.precioseleccion: '')} value={indexSeleccion} />
                      )
                    })}

                    
                  </Picker>

                </View>
              )
            })}
          </View> : null}



          {this.state.elementos.length != 0 ? <View style = {styles.seccion}>
            <Text style = {styles.tituloSeccion}>
              Personaliza tu producto
            </Text>

            {this.state.elementos.map((data, index) => {
              return(
                <View key = {index} style = {styles.elementosadiciones}>

                   <CheckBox
                    style = {{padding: 10}}
                    onClick = {() => {
                      var elementos = this.state.elementos;
                      elementos[index].isChecked = !elementos[index].isChecked
                      this.setState({elementos: elementos}, this.obtenerTotal)
                    }}

                    isChecked= {data.isChecked}
                  />

                  <Text>
                    {data.nombre + ' ' + (data.precio != 0 ? '+$' + data.precio: '')}
                  </Text>

                </View>
              )
            })}
          </View> : null}


          {this.state.adiciones.length != 0 ? <View style = {styles.seccion}>
            <Text style = {styles.tituloSeccion}>
              Complementa tu producto
            </Text>

            {this.state.adiciones.map((data, index) => {
              return(
                <View key = {index} style = {styles.elementosadiciones}>

                   <CheckBox
                    style = {{padding: 10}}
                    onClick = {() => {
                      var adiciones = this.state.adiciones;
                      adiciones[index].isChecked = !adiciones[index].isChecked
                      this.setState({adiciones: adiciones}, this.obtenerTotal)
                    }}

                    isChecked= {data.isChecked}
                  />

                  <Text>
                    {data.nombre + ' ' + (data.precio != 0 ? '+$' + data.precio: '')}
                  </Text>

                </View>
              )
            })}
          </View> : null}

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

        <View style = {styles.resumen}>
          <Text style = {styles.total}>
            Total: ${this.state.total}
          </Text>

          <TouchableOpacity style = {styles.agregarAlCarrito}
            onPress = {() => {
              this.agregarAlCarrito();
            }}
          >
            <Text style = {styles.textoBoton}>
              Agregar al carrito
            </Text>
          </TouchableOpacity>

        </View>


        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  tituloHeader: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  scroll: {
    width: width
  },

  row: {
    width: width,
    height: height * 0.10,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  seccion: {
    width: width * 0.9,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },

  tituloSeccion: {
    width: width * 0.9,
    color: '#363530',
    fontWeight: 'bold',
    fontSize: 20
  },

   nombreProducto: {
    width: width * 0.9,
    color: '#363530',
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center'
  },

  descripcion_corta: {
    width: width * 0.9,
    color: '#363530',
    fontSize: 18,
    textAlign: 'center',
    marginTop: -3
  },


  elementosadiciones: {

    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },

  factores: {
    width: width * 0.9,
    flex: 1,
  },

  infoProducto: {
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },

  producto: {
    overflow: 'hidden',
    backgroundColor: '#bebebe',
    width: width,
    height: height * 0.25,
    marginBottom: 5
  },

  

  resumen: {
    width: width * 0.9,
    paddingVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  total: {
    width: width * 0.9,
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'right',
    marginBottom: 10
  },

  agregarAlCarrito: {
    width: width * 0.9,
    borderRadius: 10,
    backgroundColor: '#53c678',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },

  textoBoton: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  }

});
