import React, {Component} from 'react';
import {RefreshControl, AsyncStorage, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import { StackNavigator } from 'react-navigation';
import Splash from './Splash';

const {width, height} = Dimensions.get('window');

export default class Home extends Component {

  constructor(props){
    super(props);

    this.state = {timePassed: false, categorias: [], usuario: null, direccion: null, pedidos: [], refreshing: false};

  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    fetch('http://traigo.com.co:8000/servicios/obtenerpedidosencurso?usuario=' + this.state.usuario.correo)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({pedidos: responseJson});
      })
      .catch((error) => {
        alert(error);
      });


    fetch('http://traigo.com.co:8000/servicios/obtenercategorias')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({categorias: responseJson, refreshing: false});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  onSelect = data => {
    
    try {
      AsyncStorage.setItem('direccion', JSON.stringify(data.direccionEntrega)).then(() => {
        this.setState(data);
      });
    } catch (error) {
      alert('Ha ocurrido un error con la aplicación');
    }
  }

  componentWillMount(){
    fetch('http://traigo.com.co:8000/servicios/obtenercategorias')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({categorias: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });


    AsyncStorage.getItem('usuario').then((dato) => {
      if(dato !== null){
        this.setState({usuario: JSON.parse(dato)}, () => {
          fetch('http://traigo.com.co:8000/servicios/obtenerpedidosencurso?usuario=' + this.state.usuario.correo)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({pedidos: responseJson});
          })
          .catch((error) => {
            alert(error);
          });
        });
      }else{
        alert('Error en la aplicacion');
      }
    });


    AsyncStorage.getItem('direccion').then((dato) => {
      if(dato !== null){
        this.setState({direccionEntrega: JSON.parse(dato)});
      }else{
        alert('Error en la aplicacion');
      }
    });

  }


  setTimePassed(){
    this.setState({timePassed: true});
  }

  renderCategoria(item, index){
    return(


          <TouchableOpacity
            onPress = {() => {
              this.props.navigation.navigate('categoria', {usuario: this.state.usuario, direccion: this.state.direccionEntrega, categoria: item})
            }}
            style = {{paddingLeft: (index == 0 ? ((width - width * 0.9) / 2) : 2.5), paddingRight: (index == this.state.categorias.length - 1 ? ((width - width * 0.9) / 2) : 2.5)   }}>

            <ImageBackground

              style = {styles.categoria}
              source = {{uri: 'http://traigo.com.co:8000/static/img/categoria/' + item.imagen}}>

              
             <View style = {styles.infocategoria}>
              <Text style={styles.titulocategoria}>
                {item.nombre}
              </Text>

              <Text style = {styles.descripcioncategoria}>
                {item.descripcion}
              </Text>

             </View>
            
            </ImageBackground>

            

          </TouchableOpacity>

    )
  }

  render() {
    return(


      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi}>

            <TouchableOpacity
              style = {{alignSelf: 'flex-start', flex: 1, paddingLeft: 5}}
              onPress = {() => {
                this.props.navigation.navigate('menuClient', this.state);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/menu.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

         

            

              

            <TouchableOpacity style = {styles.encursofull} onPress = {() => {
              this.props.navigation.navigate('pedidosCliente', {usuario: this.state.usuario});
            }}>
              <Text style = {styles.encurso}>
                en curso
              </Text>

              <View style = {styles.cantidadencurso}>
                <Text style = {{color: '#fff', fontSize: 10}}>
                  {this.state.pedidos.length}
                </Text>
              </View>

            </TouchableOpacity>

          </View>

           <View style = {styles.divi}>
             <Image
              style = {{width: width * 0.25, height: 50}}
              source = {require('../resources/logos/traigoblack.png')}
              resizeMode = 'center'
            />
          </View>

           <View style = {styles.divi}>
        
              <Text style = {styles.encurso}>
                hola, {this.state.usuario == null ? 'usuario' : this.state.usuario.nombre.split(' ')[0]}
              </Text>

          </View>
          
        </View>

       

          <TouchableOpacity style = {styles.direccion}
             onPress = {() => {
                  
                this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: 'entrega', onSelect: this.onSelect});
                
                  
                }}>

            <View style = {styles.selectdireccion}>
                <Image
                  style = {{width: 15, height: 15}}
                  source = {require('../resources/socialIcons/arrowdown.png')}
                  resizeMode = 'center'
                />
              </View>

             <Text style = {{fontSize: 15, textAlign: 'left', marginLeft: 5}}
                numberOfLines={1} >
                {this.state.direccionEntrega == null ? 'Sin dirección seleccionada' : (this.state.direccionEntrega.direccion + (this.state.direccionEntrega.notas.trim() == '' ? '' : ' | ' + this.state.direccionEntrega.notas))}
              </Text>
          </TouchableOpacity>

         <ScrollView style = {styles.scroll}
         refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>




          <TouchableOpacity
            onPress = {() => {
                  this.props.navigation.navigate('loquesea', this.state);
                }}>

            <ImageBackground
              style = {styles.loquesea}
              imageStyle = {{borderRadius: 10}}
              source = {{uri: 'http://traigo.com.co:8000/static/img/banners/deseo' + (Math.floor(Math.random() * 9) + 1) + '.jpg'}}>
             
                   <View style = {styles.info}>
                    <Text style={styles.titulobanner}>
                      ¡Lo que quieras!
                    </Text>

                    <Text style = {styles.descripcionbanner}>
                      Comida de tu restaurante secreto, ropa, accesorios o hasta que merquemos por ti.
                    </Text>

                   </View>
            

            </ImageBackground>

          </TouchableOpacity>
            
          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'center', alignItems: 'center'}}>
            <Text style = {styles.textosecciones}>
              Categorias
            </Text>
            <View style = {{marginLeft: 10, height: 1, flex: 1, backgroundColor: '#e5e5e5'}}/>
          </View>
          <View style = {styles.flatlist}>

            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal = {true}
              data = {this.state.categorias}
              renderItem = {({item, index}) => this.renderCategoria(item, index)}
              keyExtractor = {(item, index) => index.toString()}
            />


          </View>

          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'center', alignItems: 'center'}}>
            <Text style = {styles.textosecciones}>
              Favores
            </Text>
          <View style = {{marginLeft: 10, height: 1, flex: 1, backgroundColor: '#e5e5e5'}}/>
          </View>


          <TouchableOpacity
            onPress = {() => {
                    this.props.navigation.navigate('mandados', this.state);
                }}>

            <ImageBackground
              style = {styles.favor}
              imageStyle = {{borderRadius: 10}}
              source = {{uri: 'http://traigo.com.co:8000/static/img/banners/favor.jpg'}}>
              
                 <View style = {styles.info}>
                  <Text style={styles.titulobanner}>
                    Traer o llevar algo
                  </Text>

                  <Text style = {styles.descripcionbanner}>
                    Encomiendas, sobres, paquetes, cualquier cosa que necesite transporte rapido.
                  </Text>

                 </View>
              

            </ImageBackground>

          </TouchableOpacity>

    
         </ScrollView>

      </View>



    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'flex-start',
    paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    backgroundColor: '#ffffff'
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  traigo: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  cantidadencurso: {
    marginLeft: 3,
    backgroundColor: '#50dddd',
    width: 15,
    height: 15,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  encurso: {
    fontSize: 15
  },

  encursofull: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi: {
    width: width * 0.33,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  direccion: {
    width: width * 0.8,
    height: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginBottom: 10,
  },

  selectdireccion: {
    marginLeft: 20,
    backgroundColor: '#e5e5e5',
    width: 15,
    height: 15,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  loquesea: {
    borderRadius: 10,
    backgroundColor: '#bebebe',
    width: width * 0.9,
    height: height * 0.25
  },

  shadow: {
    borderRadius: 10,
    backgroundColor: 'rgba(52, 52, 52, 0.4)',
    width: width * 0.9,
    height: height * 0.25
  },

  favor: {
    borderRadius: 10,
    backgroundColor: '#bebebe',
    width: width * 0.9,
    height: height * 0.25
  },

  shadowFavor: {
    borderRadius: 10,
    backgroundColor: 'rgba(52, 52, 52, 0.4)',
    width: width * 0.9,
    height: height * 0.2
  },

  scroll: {
    backgroundColor: '#fff',
    flex: 1,
    width: width,
    paddingBottom: 5
  },

  info: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: 15
  },

  titulobanner: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },

  descripcionbanner: {
    color: '#fff',
    fontSize: 15,
    lineHeight: 15
  },

  textosecciones: {
    paddingVertical: 8, 
    textAlign: 'left',
    color: '#363530',
    fontSize: 20
  },

  flatlist: {
    width: width,
    height: height * 0.18
  },

  categoria: {
    overflow: 'hidden',
    borderRadius: 10,
    backgroundColor: '#bebebe',
    width: width * 0.5,
    height: height * 0.18
  },

  titulocategoria: {
    color: '#fff',
    fontSize: 22,
    fontWeight: 'bold'
  },

  descripcioncategoria: {
    color: '#fff',
    fontSize: 13,
    lineHeight: 13
  },

   infocategoria: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    padding: 15
  },

  trans: {
    borderRadius: 10,
    width: width * 0.5,
    height: height * 0.18,
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    alignItems: 'center',
    justifyContent: 'center'
  },

});
