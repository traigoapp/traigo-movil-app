import React, {Component} from 'react';
import {FlatList, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});



export default class SelectDirection extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, direcciones: [], direccion: null};
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
    this.volver = this.volver.bind(this);

  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    

    fetch('http://traigo.com.co:8000/servicios/obtenerdireccionescliente?correo=' + this.state.usuario.correo)
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({direcciones: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });

  }

  onSelect = info => {

    var data = new FormData();
    data.append('correo', this.props.navigation.getParam('usuario', 'desconocido').correo);
    data.append('latitude', info.region.latitude);
    data.append('longitude', info.region.longitude);
    data.append('direccion', info.direccion);
    data.append('notas',info.notas);

    fetch('http://traigo.com.co:8000/servicios/agregardireccion', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
        }else{
          var dir = responseJson[0];
          var direcciones = this.state.direcciones;
          direcciones.push(dir);
          this.setState({direcciones: responseJson});
        }
      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión');
      });
  }


  componentWillMount(){
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccion: this.props.navigation.getParam('direccion', null)});
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  volver(selected){
    
  }

renderDireccion(item){


    return(
          <TouchableOpacity style = {styles.row} onPress = {() => {
            this.props.navigation.goBack();
            if(this.state.direccion == 'entrega'){
              this.props.navigation.state.params.onSelect({direccionEntrega: item});
            }else if(this.state.direccion == 'recogida'){
              this.props.navigation.state.params.onSelect({direccionRecogida: item});
            }else{
              paradas = this.props.navigation.getParam('paradas', null);
              paradas.push(item);
              this.props.navigation.state.params.onSelect({paradas: paradas});
            }
          }}>

           <Text style = {styles.textRow}>
            {item.direccion}
           </Text>

           <Text style = {styles.textRowLow}>
            {item.notas.trim() == '' ? 'Sin notas de la dirección' : item.notas}
           </Text>

            <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, alignSelf: 'center'}}>
          </View>

         </TouchableOpacity>


    )
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                 Dirección de {this.state.direccion}
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center'}}>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, marginBottom: 10}}>
          </View>

         <View style = {{flex: 1}}>

            <FlatList
              ItemSeparatorComponent = {() => <View style = {{width: 5}}/>}
              data = {this.state.direcciones}
              renderItem = {({item}) => this.renderDireccion(item)}
              keyExtractor = {(item, index) => index.toString()}
            />

             <TouchableOpacity style = {styles.row} onPress = {() => {
            this.props.navigation.navigate('mapRegister', {onSelect: this.onSelect});
           }}>

             <Text style = {styles.textRow}>
              Nueva dirección
             </Text>

             <Text style = {styles.textRowLow}>
              Agregar dirección
             </Text>
          <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, alignSelf: 'center'}}>
          </View>
             </TouchableOpacity>

          </View>


        

         

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 20,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingTop: 5,
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  textRowLow: {
    paddingBottom: 5,
    paddingLeft: 20,
    fontSize: 15,
    color: '#363530'
  }

});
