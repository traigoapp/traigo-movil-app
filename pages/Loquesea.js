import React, {Component} from 'react';
import {FlatList, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'
import CameraRollPicker from 'react-native-camera-roll-picker';

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class Loquesea extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, direccion: null, khidden: true, deseo: '', images: [], seleccionar: false, showHelp: false, costos: {}};
    this.siguiente = this.siguiente.bind(this);
    this._pickImage = this._pickImage.bind(this);
    this.cerrar = this.cerrar.bind(this);
    this.getSelectedImages = this.getSelectedImages.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  componentWillReceiveProps(){
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccion: this.props.navigation.getParam('direccionEntrega', null)})
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccion: this.props.navigation.getParam('direccionEntrega', null)})
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentWillMount(){
    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  onSelect = data => {
    this.setState(data);
  }

  siguiente(){
    if(this.state.deseo.trim() != '' || this.state.images.length != 0){
      Keyboard.dismiss();
      this.props.navigation.navigate('confirmDeseo', {usuario: this.state.usuario, deseo: this.state.deseo, direccion: this.state.direccion, images: this.state.images});
    }
    
  }

  _pickImage(){
    Keyboard.dismiss();
    this.setState({seleccionar: true});
  }

   cerrar(){
    Keyboard.dismiss();
    this.setState({seleccionar: false});
  }  

  getSelectedImages(images){
    this.setState({images: images});
  }

  renderImage(item){

    return(

          <TouchableOpacity>

            <Image
              style = {styles.imagen}
              source = {{uri: item.uri}}>
            </Image>

          </TouchableOpacity>

    )
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                Keyboard.dismiss();
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/backwhite.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                iPide lo que que quieras!
             </Text>
          </View>

          <TouchableOpacity style = {styles.divi3} onPress = {() => {Keyboard.dismiss();this.setState({showHelp: true})}}>
             <Image
              style = {{width: 30, height: 30, marginRight: 10}}
              source = {require('../resources/socialIcons/help.png')}
              resizeMode = 'contain'
              />
          </TouchableOpacity>
          
        </View>

        <View style = {styles.scroll}>

         <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: height * 0.45 }]}>

         <Text style = {styles.textName}>
                Hola, {this.state.usuario == null ? 'ups!' : this.state.usuario.nombre}
         </Text>

          <TextInput
            multiline = {true}
            selectionColor={'#8b8b8b'}
            placeholder = {'Puedes pedir comida de tu restaurante secreto, ropa, accesorios o hasta que merquemos por ti.'}
            placeholderTextColor = '#8b8b8b'
            underlineColorAndroid='transparent'
            onChangeText = {(text) => {this.setState({deseo: text})}}
            style = {[styles.textinput]}>
            </TextInput>

            <View style = {{height: 30, width: width * 0.9 - 40, marginBottom: 5}}>
              <FlatList
                horizontal
                ItemSeparatorComponent = {() => <View style = {{width: 5}}/>}
                data = {this.state.images}
                renderItem = {({item}) => this.renderImage(item)}
                keyExtractor = {(item, index) => index.toString()}
              />

            </View>

            <View style = {styles.button}>
              {Platform.OS == 'ios' ? <TouchableOpacity
                  style = {[styles.siguiente, {backgroundColor: (this.state.deseo.trim() == '' && this.state.images.length == 0? '#bfbfbf' : '#53c678')}]}
                  onPress = {this.siguiente}>

                      <Text style = {styles.textButton}>
                        Siguiente
                      </Text>

                </TouchableOpacity> :

                <TouchableOpacity
                  style = {[styles.siguienteAndroid, {backgroundColor: (this.state.deseo.trim() == '' && this.state.images.length == 0? '#bfbfbf' : '#53c678')}]}
                  onPress = {this.siguiente}>

                      <Text style = {styles.textButton}>
                        Siguiente
                      </Text>

                </TouchableOpacity>

              }

                {Platform.OS === 'ios' ? <TouchableOpacity
                  style = {[styles.foto, {backgroundColor: (this.state.deseo.trim() == '' && this.state.images.length == 0? '#afafaf' : '#4ca262')}]}
                  onPress = {this._pickImage}>

                      <Image
                        style = {{width: 50, height: 50}}
                        source = {require('../resources/socialIcons/camerawhite.png')}
                        resizeMode = 'center'
                      />

                   

                </TouchableOpacity>: null}


            </View>
          </View>

          {this.state.seleccionar ? <View style = {{height: height * 0.4}}>
              <TouchableOpacity
                  style = {[styles.cerrar, {backgroundColor: '#53c678'}]}
                  onPress = {this.cerrar}>

                      <Text style = {styles.textButton}>
                        Terminar selección
                      </Text>

                   

                </TouchableOpacity>

              <CameraRollPicker
              selected = {this.state.images}
              callback={this.getSelectedImages} />
            </View> : null}
        </View>


        {this.state.showHelp ? <View style = {styles.help}>

          <View style = {styles.helpInfo}>
            <Image
              style = {{width: 50, height: 50}}
              source = {require('../resources/socialIcons/admiration.png')}
              resizeMode = 'contain'
              />


            <Text style = {styles.parrafoHelp}>
              El costo de este servicio es de ${this.state.costos.costo_envio_deseo} pesos + {this.state.costos.comision_cliente_deseo}%
              del valor de la compra. Conocerás el total tan pronto como el Rider tenga tu pedido
            </Text>


            <Text style = {styles.parrafoHelp}>
              Los Riders van en moto o en bicicleta y pueden llevar hasta 9Kg de peso en sus maletas en un espacio de 42x42x42
            </Text>

            <Text style = {styles.parrafoHelp}>
              Llevaremos cualquier cosa que pidas, siempre y cuando sea legal y pueda transportarse. No transportamos ningún tipo
              de mascota o animal.
            </Text>


            <TouchableOpacity style = {styles.botonCerrar} onPress = {() => {this.setState({showHelp: false})}}>
              <Text style = {styles.textoCerrar}>
                CONTINUAR
              </Text>
            </TouchableOpacity>


          </View>

        </View> : null}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#50dddd',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#50dddd',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  scroll: {
    width: width,
    backgroundColor: '#50dddd',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  siguiente: {
    flex: 8,
    height: 50,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  siguienteAndroid: {
    flex: 1,
    height: 50,
    borderRadius: 15, 
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  foto: {
    flex: 2,
    height: 50,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'

  },

  cerrar: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    width: width

  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },

  nombreCliente: {
    color: '#fff',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    marginVertical: 3,
    borderRadius: 15,
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },

  textinput: {
    color: '#363530', 
    fontSize: 15,
    flex: 1,
    textAlignVertical: 'top'
  },

  button: {
    backgroundColor: '#fff',
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    width: width * 0.9 - 40
  },

  textButton: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    flex: 1,
    fontWeight: 'bold'
  },

  textName: {
    height: 30,
    color: '#000',
    fontSize: 20,
    textAlign: 'left',
    fontWeight: 'bold'
  },

  buttonseleccionar: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    width: width * 0.9,
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButtonseleccionar: {
    flex: 1,
    paddingLeft: 10,
    color: '#363530',
    fontSize: 20,
    textAlign: 'left',
  },

  arrow: {
    color: '#fff',
    fontSize: 15,
    textAlign: 'right',
    right: 0,
  },

  circle: {
    borderRadius: 50,
    width: 20,
    height: 20,
    backgroundColor: '#363530',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },

  imagen: {
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: '#bebebe',
    width: 30,
    height: 30,
    marginBottom: 2,
  },

  help: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute',
    top: 0,
    left: 0
  },

  helpInfo: {
    backgroundColor: '#fff',
    width: width * 0.9,
    borderRadius: 10,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },

  parrafoHelp: {
    fontSize: 15,
    textAlign: 'center',
    color: '#363530',
    marginTop: 30
  },

  botonCerrar: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.7,
    height: 50,
    backgroundColor: '#50dddd',
    borderRadius: 10
  },

  textoCerrar: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  }

});
