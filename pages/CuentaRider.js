import React, {Component} from 'react';
import {Alert, RefreshControl, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class MenuRider extends Component {

  constructor(props){
    super(props);

    this.state = {rider: null, refreshing: false, ganancias: 0, costos: null};
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  componentWillMount(){
    this.setState({rider: this.props.navigation.getParam('rider', 'no rider')}, () => {
      fetch('http://traigo.com.co:8000/servicios/obtenerpedidosrealizadosrider?cedularider=' + this.state.rider.cedula)
    .then((response) => response.json())
    .then((responseJson) => {
      var cuenta = 0;
      var pedidos = responseJson;
      var cont = 0;
      for(var i = 0; i < pedidos.length; i++){
        if(pedidos[i].type == 'deseo'){
          if(pedidos[i].comision_entregada == 'N'){
            cuenta -= pedidos[i].totalfacturas * pedidos[i].comision_cliente / 100;
          }
        }else if(pedidos[i].type == 'mandado'){
          if(pedidos[i].comision_entregada == 'N'){
            if(pedidos[i].totalparadas == null)
              pedidos[i].totalparadas = 0;
            cuenta -= (pedidos[i].totalparadas + pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
          }
        }else if(pedidos[i].type == 'tienda'){
          if(pedidos[i].comision_entregada == 'N'){
            cuenta -= (pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
          }
        }else{
          if(pedidos[i].comision_entregada == 'N'){
            if(pedidos[i].retenido == 'S'){
              cuenta += (pedidos[i].costo_envio) * (100 - pedidos[i].comision_rider) / 100;
            }else{
              cuenta -= (pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
            }
            
          }
        }
      }

      this.setState({ganancias: cuenta, refreshing: false});

    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión: ' + error);
    });
    });
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  _onRefresh = () => {
    this.setState({refreshing: true});

    fetch('http://traigo.com.co:8000/servicios/obtenerpedidosrealizadosrider?cedularider=' + this.state.rider.cedula)
    .then((response) => response.json())
    .then((responseJson) => {
      var cuenta = 0;
      var pedidos = responseJson;
      
      for(var i = 0; i < pedidos.length; i++){
        if(pedidos[i].type == 'deseo'){
          if(pedidos[i].comision_entregada == 'N'){
            cuenta -= pedidos[i].totalfacturas * pedidos[i].comision_cliente / 100;
          }
        }else if(pedidos[i].type == 'mandado'){
          if(pedidos[i].comision_entregada == 'N'){
            if(pedidos[i].totalparadas == null)
              pedidos[i].totalparadas = 0;
            cuenta -= (pedidos[i].totalparadas + pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
          }
        }else if(pedidos[i].type == 'tienda'){
          if(pedidos[i].comision_entregada == 'N'){
            cuenta -= (pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
          }
        }else{
          if(pedidos[i].comision_entregada == 'N'){
            if(pedidos[i].retenido == 'S'){
              cuenta += (pedidos[i].costo_envio) * (100 - pedidos[i].comision_rider) / 100;
            }else{
              cuenta -= (pedidos[i].costo_envio) * pedidos[i].comision_rider / 100;
            }
            
          }
        }
      }

      this.setState({ganancias: cuenta, refreshing: false});

    })
    .catch((error) => {
      this.setState({refreshing: false});
      alert('Ha ocurrido un error con la conexión');
    });
  }

  solicitarPago(){
    this.setState({loading: true});
    var data = new FormData();
    data.append('cedularider', this.state.rider.cedula);
  

    fetch('http://traigo.com.co:8000/servicios/solicitarpago', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.code){
        alert('Ha ocurrido un error con la conexión')
        this.setState({loading: false});
      }else if(responseJson.current){
        alert('Ups! Ya realizaste una solicitud. Espera a que sea procesada.');
        this.setState({loading: false});
      }else{
        alert('Solicitud realizada');
        this.setState({loading: false, refreshing: false});
      }
    })
    .catch((error) => {

      this.setState({loading: false});
      alert('Ha ocurrido un error con la conexión: ' + error);
    });
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Mi Cuenta
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
          refreshControl={
          <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          />}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>


         <View style = {styles.infoPerfil}>
          <Image style = {styles.foto}
              imageStyle = {{borderRadius: 10}}
              source = {{uri: 'http://traigo.com.co:8000/static/img/rider/' + this.state.rider.foto_perfil}}>

          </Image>

          <Text style = {{textAlign: 'center', fontSize: 25, color: '#afafaf'}}>
            {this.props.navigation.getParam('rider', 'Ninguno').nombre}
          </Text>
         </View>

          

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

          <Text style = {styles.titulo}>
            Mis ganancias
          </Text>

          <Text style = {styles.totalganancias}>
            {this.state.ganancias < 0 ? 'Debes a Traigo: $' + -1 * this.state.ganancias : 'Traigo te debe: $' + this.state.ganancias}
          </Text>

          <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

          <TouchableOpacity
          style = {styles.botonSolicitar}
          onPress = {() => {
            if(this.state.ganancias >= 1000){
               Alert.alert(
              'Solicitar',
              'Desea solicitar el pago?',
              [
              {text: 'Confirmar', onPress: () => this.solicitarPago(), style: 'cancel'},
              {text: 'Cancelar', onPress: () => {this.setState({loading: false})}, style: 'cancel'},
              ],
              { cancelable: true }
              )
              
            }else{
              Alert.alert(
              'Ups!',
              'Aún no puedes solicitar el pago Monto mínimo: $45000',
              [
              {text: 'Okey', onPress: () => {
                this.setState({loading: false});
               
              }, style: 'cancel'},
              ],
              { cancelable: false }
              )
            }
          }}>
            <Text style = {{textAlign: 'center', color: '#fff', fontSize: 20, fontWeight: 'bold'}}>
              Solicitar pago
            </Text>
          </TouchableOpacity>


          {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    marginLeft: -30,
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width,
    height: height * 0.10,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  infoPerfil: {
    marginVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  foto: {
    borderRadius: 25,
    backgroundColor: '#bebebe',
    width: 80,
    height: 80,
    marginBottom: 20
  },

  titulo: {
    marginTop: 20,
    fontSize: 40,
    color: '#afafaf',
    textAlign: 'center'
  },

  totalganancias: {
    width: width * 0.8,
    color: '#afafaf',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 20
  },

  botonSolicitar: {
    marginTop: 20,
    borderRadius: 10,
    width: width * 0.9,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#41aa54'
  }

});
