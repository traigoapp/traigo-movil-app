import React, {Component} from 'react';
import {Keyboard, AsyncStorage, StyleSheet, View, Dimensions, Text, Platform, Linking, StatusBar, TouchableOpacity, Image} from 'react-native';

import LogoTextWhite from "../components/logos/LogoTextWhite"
import FormButton from "../components/form/FormButton"
import FormHead from "../components/form/FormHead"
import { StackActions, NavigationActions} from 'react-navigation';

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const {width, height} = Dimensions.get('window');

const popAction = StackActions.pop({
  n: 1,
});


export default class Welcome extends Component {

  constructor(props){
    super(props);
  }

  selectMap(){
    this.props.navigation.navigate('mapRegister', {

      nombre: this.props.navigation.getParam('nombre', 'desconocido'),
      correo: this.props.navigation.getParam('correo', 'desconocido'),
      telefono: this.props.navigation.getParam('telefono', 'desconocido'),
      contrasenia: this.props.navigation.getParam('contrasenia', 'desconocido'),
      onSelect: this.onSelect
    });
  }
  
  saveData(usuario, direccion) {
    try {
      AsyncStorage.setItem('usuario', usuario).then(() => {
        AsyncStorage.setItem('direccion', direccion).then(() => {
          this.props.navigation.dispatch(resetAction);
        });
      });
    } catch (error) {
      alert('Ha ocurrido un error con la aplicacion: ' + error);
    }
  }

  onSelect = info => {

    var data = new FormData();
    data.append('nombre', this.props.navigation.getParam('nombre', 'desconocido'));
    data.append('correo', this.props.navigation.getParam('correo', 'desconocido'));
    data.append('telefono', this.props.navigation.getParam('telefono', 'desconocido'));
    data.append('contrasenia', this.props.navigation.getParam('contrasenia', 'desconocido'));
    data.append('latitude', info.region.latitude);
    data.append('longitude', info.region.longitude);
    data.append('direccion', info.direccion);
    data.append('notas',info.notas);

    fetch('http://traigo.com.co:8000/servicios/agregarcliente', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
        }else{
          var usuario = responseJson[0];
          var dir = responseJson[1];
          this.saveData(JSON.stringify(usuario), JSON.stringify(dir));
        }
      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión');
      });
  }


  render() {
    return (
      <View style = {styles.container}>

      <View style = {{width: width, height: height * 0.3, marginBottom: 50, marginTop: 30}}> 
            <Image
              style = {styles.image}
              source = {require('../resources/logos/textWhite.png')}
              resizeMode = 'contain'
            />

         </View>
 


      <TouchableOpacity

          style = {{alignItems: 'flex-start', flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 10}}

          onPress = {() => {
              Keyboard.dismiss();
              this.props.navigation.dispatch(popAction);
            } }
        >

        <View style = {{width: 30, height: 30, marginTop: 10, marginLeft: 5}}>
         <Image
          style = {styles.image}
          source = {require('../resources/socialIcons/xwhite.png')}
          resizeMode = 'contain'
          />
        </View>

      </TouchableOpacity>

        

          <View style = {[styles.form, {marginTop: 40}]}>
            <FormHead text = {'Ingresa una dirección de entrega'}/>
            <View style = {{height: 1, backgroundColor: '#e5e5e5', width: width * 0.8}}></View>
            <FormButton text = {'Seleccionar ubicación'} icon = {'add'} next = {this.selectMap.bind(this)}/>
          </View>
        

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#50dddd',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  form: {
    marginBottom: 10
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  }

});
