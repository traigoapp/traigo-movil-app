import React, {Component} from 'react';
import {Image, AsyncStorage, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import Splash from './Splash';

import {StackActions, NavigationActions} from 'react-navigation'

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const resetActionRider = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'homeRider'})],
});

const {width, height} = Dimensions.get('window');

export default class Welcome extends Component {

  constructor(props){
    super(props);

    this.state = {timePassed: false};
    
  }


  componentDidMount(){
    setTimeout(() => {
      this.setTimePassed();
    }, 1000);
  }

  setTimePassed(){



    try{
      AsyncStorage.getItem('usuario').then((value) => {
        if(value !== null){
          this.props.navigation.dispatch(resetAction);
        }else{
          AsyncStorage.getItem('rider').then((value) => {
            if(value !== null){
              this.props.navigation.dispatch(resetActionRider);
            }
          });
        }
      });
    }catch(error){
      alert('Error al leer los datos'); 
    }

    this.setState({timePassed: true});
  }

  

  render() {

    if (!this.state.timePassed) {
        return <Splash/>;
    }else {

      return (
        <View style = {styles.container}>

        <TouchableOpacity onPress = {() => {
              this.props.navigation.navigate('loginRider');
            }}>
         <View style = {{width: width, height: height * 0.3, marginBottom: 50, marginTop: 30}}> 
            <Image
              style = {styles.image}
              source = {require('../resources/logos/text.png')}
              resizeMode = 'contain'
            />



         </View>

         </TouchableOpacity>

          <Text style = {styles.hi}>
            ¡Hola!
          </Text>

          <Text style = {styles.message}>
            Inicia sesión para pedir.
          </Text>

          
          <TouchableOpacity
            style = {styles.socialButton}
            onPress = {() => this.props.navigation.navigate('login')}>
              <NormalButton social = {'email'} text = {'Iniciar sesión'} color = {'#0636ce'}/>
          </TouchableOpacity>

          
          <TouchableOpacity
            style = {styles.socialButton}
            onPress = {() => this.props.navigation.navigate('register')}>
              <NormalButton social = {'email'} text = {'Registrarme'} color = {'#50dddd'}/>
          </TouchableOpacity>
          
          <Text style = {{marginTop: 20, color: '#363530'}}>
            Al ingresar aceptarás nuestros
          </Text>

          <View style = {{flex:1, flexDirection: 'row'}}>
             <Text style = {styles.link}
                onPress = {() => Linking.openURL('https://www.traigo.com.co/terminos-y-condiciones')}>
                términos y condiciones 
              </Text>

               <Text style = {{color: '#363530', marginLeft: 2, marginRight: 2}}>
                 y
              </Text>

             <Text style = {styles.link}
              onPress = {() => Linking.openURL('https://www.traigo.com.co/politicas-de-privacidad')}>
              políticas de privacidad
            </Text>
          </View>

          <Text
            style = {styles.rigth}>
            Traigo® 2018
          </Text>

        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    backgroundColor: '#ffffff'
  },

  hi: {
    fontSize: 35,
    fontWeight: 'bold',
    color: '#363530'
  },

  message: {
    fontSize: 25,
    color: '#363530',
    marginBottom: 20
  },

  socialButton: {
    marginTop: 5
  },

  rigth:{
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 5,
    color: '#363530'
  },

  link:{
    textDecorationLine: 'underline',
    color: '#363530'
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  }

});
