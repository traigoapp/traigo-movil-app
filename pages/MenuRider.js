import React, {Component} from 'react';
import {AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class MenuRider extends Component {

  constructor(props){
    super(props);

    this.state = {rider: null};
    this.cerrarSesion = this.cerrarSesion.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  componentWillMount(){
    AsyncStorage.getItem('rider').then((dato) => {
      this.setState({rider: JSON.parse(dato) });
    });
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  

  cerrarSesion(){
    try {
      AsyncStorage.clear().then(() => {
        this.props.navigation.dispatch(resetAction);
      });
    } catch (error) {
      alert(error);
      alert('Ha ocurrido un error con la aplicación');
    }
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
                this.props.navigation.state.params.onRefresh();
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                {this.state.rider != null ? this.state.rider.nombre.split(' ')[0] : 'Rider'}
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>


          <TouchableOpacity style = {styles.row} onPress = {() => {
            this.props.navigation.navigate('pedidosRider', {rider: this.state.rider});
          }}>

           <Text style = {styles.textRow}>
            Pedidos en curso
           </Text>

         </TouchableOpacity>
         

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>


          <TouchableOpacity style = {styles.row} onPress = {() => {
            this.props.navigation.navigate('cuentaRider', {rider: this.state.rider});
          }}>

           <Text style = {styles.textRow}>
            Cuenta
           </Text>

         </TouchableOpacity>
         

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

            <TouchableOpacity style = {styles.row} onPress = {this.cerrarSesion}>

           <Text style = {styles.textRow}>
            Cerrar sesión
           </Text>

         </TouchableOpacity>

          

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width,
    height: height * 0.10,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  }

});
