import React, {Component} from 'react';
import {Alert, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'
import { RNCamera } from 'react-native-camera';

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'welcome'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class AgregarFactura extends Component {

  constructor(props){
    super(props);
    this.state = {pedido: null, valor: '', tomarFoto: false, foto: null, facturas: [], loading: false};

  }

  takePicture = async function() {
    Keyboard.dismiss();
    if (this.camera) {
      const options = { quality: 0.5, base64: true , fixOrientation: true};
      const data = await this.camera.takePictureAsync(options)
      console.log(data.uri);
      this.setState({foto: data, tomarFoto: false});
    }
  };

  componentWillMount(){
    this.setState({pedido: this.props.navigation.getParam('pedido', null), facturas: this.props.navigation.getParam('facturas', null)});
  }

  agregarFactura(){
    Keyboard.dismiss();
    if(this.state.foto == null || this.state.valor.trim() == ''){
      alert('Debe tomar foto de la factura e ingresar un valor');
    }else{
      this.setState({loading: true});
      var data = new FormData();
      data.append('valor', this.state.valor);
      data.append('cantidadFacturas', this.state.facturas.length);
      data.append('idpedido', this.state.pedido.idpedido_deseo);
      data.append('fotofactura', {
          uri: this.state.foto.uri,
          name: 'factura.jpg',
          type: 'image/jpg'
      });
      
      fetch('http://traigo.com.co:8000/servicios/agregarfactura', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión');
          this.setState({loading: false});
        }else{
          
          Alert.alert(
            'Factura agregada',
            'La factura ha sido agregada puede continuar',
            [
          
              {text: 'OK', onPress: () => {
                  this.setState({loading: false});
                  this.props.navigation.goBack();
                  this.props.navigation.state.params.onSelect({});

              }},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión');
      });

    }
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Agregar factura
             </Text>
          </View>
          
        </View>

        <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}>
              <TextInput
                selectionColor={'#8b8b8b'}
                  placeholder = {'Valor de la factura'}
                  placeholderTextColor = '#8b8b8b'
                  underlineColorAndroid='transparent'
                  keyboardType = 'phone-pad'
                  onChangeText = {(text) => {this.setState({valor: text})}}
                  style = {{flex: 1,  fontSize: 15}}
                  >
             </TextInput>
        </View>

        <TouchableOpacity style = {styles.botonFoto} onPress = {() => {Keyboard.dismiss();this.setState({tomarFoto: true})}}>
            <Image
                    style = {{width: 40, height: 40, marginRight: 5}}
                    source = {require('../resources/socialIcons/camerawhite.png')}
                    resizeMode = 'center'
              />

              <Text style = {styles.textoBoton}>
                {this.state.foto != null ? 'Cambiar foto' :'Tomar foto'}
              </Text>
        </TouchableOpacity>

    

        <TouchableOpacity style = {styles.botonSiguiente} onPress = {this.agregarFactura.bind(this)}>
              
              

              <Text style = {styles.textoBoton}>
                Agregar factura
              </Text>
        </TouchableOpacity>

        {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

       {this.state.tomarFoto ? <View style = {styles.viewCapture}>

            <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style = {styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                permissionDialogTitle={'Permiso para usar la cámara'}
                permissionDialogMessage={'Necesitamos permiso para usar la cámara de su dispositivo'}
            />
            <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
            <TouchableOpacity
                onPress={this.takePicture.bind(this)}
                style = {[styles.capture]}
            >
                <Image
                    style = {{width: 50, height: 50}}
                    source = {require('../resources/socialIcons/camerawhite.png')}
                    resizeMode = 'center'
              />
            </TouchableOpacity>


            </View>
        </View> : null}

        <Image
        style = {styles.foto}
        source = {{uri: (this.state.foto == null ? '' : this.state.foto.uri )}}
        resizeMode = 'cover'
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 20,
    paddingLeft: 20,
    flexDirection: 'row',
    borderRadius: 10,
    backgroundColor: '#efefef',
  },

  row: {
    width: width,
    height: height * 0.10,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  botonFoto: {
    flexDirection: 'row',
    width: width * 0.9,
    backgroundColor: '#ff8000',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 50,
    marginVertical: 5
  },

  botonSiguiente: {
    width: width * 0.9,
    backgroundColor: '#41aa54',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 50,
  },

  textoBoton: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  foto: {
    zIndex: -1,
    marginVertical: 10, 
    flex: 1,
    width: width * 0.9,
    borderRadius: 10,
    backgroundColor: '#fff',
  },

  preview: {
    width: width,
    height: height,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    position: 'absolute',
    width: 60,
    height: 60,
    flex: 0,
    backgroundColor: '#afafaf',
    borderRadius: 50,
    paddingHorizontal: 20,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    bottom: 10
  },

  viewCapture: {
    position: 'absolute',
    top: 0
  }

});
