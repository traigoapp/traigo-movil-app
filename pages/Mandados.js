import React, {Component} from 'react';
import {Alert, FlatList, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'
import CameraRollPicker from 'react-native-camera-roll-picker';

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class Mandados extends Component {

  constructor(props){
    super(props);

    this.state = {margenTeclado: 0, showHelp: false,usuario: null, paradas: [], direccion: null, khidden: true, mandado: '', images: [], seleccionar: false, direccionRecogida: null, direccionEntrega: null, costos: null, loading: false};
    this.confirmar = this.confirmar.bind(this);
    this._pickImage = this._pickImage.bind(this);
    this.cerrar = this.cerrar.bind(this);
    this.getSelectedImages = this.getSelectedImages.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  confirmar(){
    this.setState({loading: true});

    if(this.state.direccionRecogida == null || this.state.direccionEntrega == null || this.state.mandado.trim() == ''){
      return;
    }

    var data = new FormData();
    data.append('costo_envio', this.state.costos.costo_envio_mandado);
    data.append('comision_rider', this.state.costos.comision_rider_mandado);
    data.append('direccionOrigen', JSON.stringify(this.state.direccionRecogida));
    data.append('direccionDestino', JSON.stringify(this.state.direccionEntrega));
    data.append('correo', this.state.usuario.correo);
    data.append('mandado', this.state.mandado);
    data.append('paradas', JSON.stringify(this.state.paradas));
    data.append('costoparada', this.state.costos.costo_paradas_mandado);
    
    data.append('imageslength', this.state.images.length);
    
    for(var i = 0; i < this.state.images.length; i++){
      data.append('picture' + i, {
        uri: this.state.images[i].uri,
        name: i + '.jpg',
        type: 'image/jpg'
      });
    }

    
 

    fetch('http://traigo.com.co:8000/servicios/realizarpedidomandado', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
        }else{
          this.setState({loading: false});
          Alert.alert(
            'Pedido realizado',
            'El pedido está siendo procesado, para mas información puede ver los pedidos en curso',
            [
          
              {text: 'OK', onPress: () => {this.props.navigation.dispatch(resetAction)}},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión');
      });
  }

  componentWillReceiveProps(){
    alert('work');
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccion: this.props.navigation.getParam('direccion', null)})
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccion: this.props.navigation.getParam('direccion', null)})
  }

  componentWillMount(){
    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(e){
    this.setState({khidden: false});
    this.setState({
      margenTeclado: e.endCoordinates.height
    });
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
    this.setState({
      margenTeclado: 0
    });
  }

  onSelect = data => {
    this.setState(data);
  }

  siguiente(){
    Keyboard.dismiss();
  }

  _pickImage(){
    Keyboard.dismiss();
    this.setState({seleccionar: true});
  }

   cerrar(){
    Keyboard.dismiss();
    this.setState({seleccionar: false});
  }  

  getSelectedImages(images){
    this.setState({images: images});
  }

  renderImage(item){

    return(

          <TouchableOpacity>

            <Image
              style = {styles.imagen}
              source = {{uri: item.uri}}>
            </Image>

          </TouchableOpacity>

    )
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                Keyboard.dismiss();
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/back.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Traer o llevar algo
             </Text>
          </View>
          
          <TouchableOpacity style = {styles.divi3} onPress = {() => {Keyboard.dismiss();this.setState({showHelp: true})}}>
             <Image
              style = {{width: 30, height: 30, marginRight: 10}}
              source = {require('../resources/socialIcons/helpblack.png')}
              resizeMode = 'contain'
              />
          </TouchableOpacity>

        </View>

        <KeyboardAvoidingView behavior = {'padding'} style = {{flex: 1}}>

          <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'flex-start', alignItems: 'center', flex: 0}}>

         <View style = {styles.info}>
          <Text style = {{textAlign: 'left', fontSize: 10}}>
            Encomiendas, sobres, paquetes, cualquier cosa que necesite transporte rápido. Para compras use "Lo que quieras"
          </Text>
         </View>


         <View style = {styles.info}>
          <Text style = {{textAlign: 'left', fontSize: 18, fontWeight: 'bold'}}>
            Dirección de recogida y entrega
          </Text>
         </View>


         <TouchableOpacity style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}
                onPress = {() => {
                  this.setState({direccion: 'recogida'}, () => {
                    this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: this.state.direccion, onSelect: this.onSelect});
                  });
                  
                }}>
          
             <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/location.png')}
                  resizeMode = 'center'
                />
              </View>

              <View style = {styles.textParadas}>
                <Text style = {{fontSize: 15, color: '#8b8b8b'}}>
                    {this.state.direccionRecogida == null ? 'Dirección de recogida' : this.state.direccionRecogida.direccion}
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}
            onPress = {() => {
                  this.setState({direccion: 'entrega'}, () => {
                    this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: this.state.direccion, onSelect: this.onSelect});
                  });
                  
                }}>
          
              <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/location.png')}
                  resizeMode = 'center'
                />
              </View>

              <View style = {styles.textParadas}>
                <Text style = {{fontSize: 15, color: '#8b8b8b'}}>
                    {this.state.direccionEntrega == null ? 'Dirección de entrega' : this.state.direccionEntrega.direccion}
                </Text>
              </View>

            
            </TouchableOpacity>

            {this.state.paradas.map((data, i) => {
              return (<View key = {i} style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}>
          
               <TouchableOpacity style = {styles.icon}
               onPress = {() => {
                  paradas = this.state.paradas;
                  delete paradas[i];
                  this.setState({paradas: paradas});
                }}>



                 <Image
                    style = {{width: 30, height: 30, flex: 1}}
                    source = {require('../resources/socialIcons/xblack.png')}
                    resizeMode = 'center'
                  />
                </TouchableOpacity>


                <View style = {styles.textParadas}>
                  <Text style = {{fontSize: 15, color: '#8b8b8b'}}>
                      {this.state.paradas[i].direccion}
                  </Text>
                </View>

             
              </View>)
            })}

            <TouchableOpacity style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}
              onPress = {() => {
                  this.setState({direccion: 'parada'}, () => {
                    this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: this.state.direccion, onSelect: this.onSelect, paradas: this.state.paradas});
                  });
                  
                }}>
          
             <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/add.png')}
                  resizeMode = 'center'
                />
              </View>


              <View style = {styles.textParadas}>
                <Text style = {{fontSize: 15, color: '#8b8b8b'}}>
                    Añadir parada adicional (+$1500)
                </Text>
              </View>

           
            </TouchableOpacity>

          <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: height * 0.2, marginTop: 30}]}>

            <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/bookblack.png')}
                  resizeMode = 'center'
                />
              </View>


              <TextInput
                multiline = {true}
                selectionColor={'#8b8b8b'}
                autoCapitalize = 'none'
                placeholder = {'Puedes pedir comida de tu restaurante secreto, ropa, accesorios o hasta que merquemos por ti.'}
                placeholderTextColor = '#8b8b8b'
                underlineColorAndroid='transparent'
                onChangeText = {(text) => {this.setState({mandado: text})}}
                style = {[styles.textinput]}>
                </TextInput>
            </View>

            

            <View style = {styles.rowDeseo}>
              <View style = {styles.infoPago}>
                <Text style = {styles.infoPagoText}>
                  Valor del servicio:
                </Text>
                <Text style = {styles.textInfo}>
                  ${this.state.costos == null ? 0 : this.state.costos.costo_envio_mandado}
                </Text>
              </View>

              <View style = {styles.infoPago}>
                <Text style = {styles.infoPagoText}>
                  {'Valor por paradas (' + this.state.paradas.length + '):'}
                </Text>
                <Text style = {styles.textInfo}>
                  ${this.state.costos == null ? 0 : this.state.costos.costo_paradas_mandado * this.state.paradas.length}
                </Text>
              </View>

               <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, marginVertical: 5}}>
          </View>

              <View style = {styles.infoPago}>
                <Text style = {styles.infoPagoText}>
                  Total:
                </Text>
                <Text style = {styles.textInfo}>
                  ${this.state.costos == null ? 0 : this.state.costos.costo_paradas_mandado * this.state.paradas.length + this.state.costos.costo_envio_mandado}
                </Text>
              </View>

            </View>

          <View style = {{height: 30, width: width * 0.9, marginBottom: 5, marginTop: 20}}>
              <FlatList
                horizontal
                ItemSeparatorComponent = {() => <View style = {{width: 5}}/>}
                data = {this.state.images}
                renderItem = {({item}) => this.renderImage(item)}
                keyExtractor = {(item, index) => index.toString()}
              />

            </View>

          <View style = {{height: 70, width: width * 0.9, marginBottom: 5, marginTop: 20}}>

            </View>

        </ScrollView>

        </KeyboardAvoidingView>

        <View style = {[styles.button, {bottom: this.state.margenTeclado + 10}]}>
              {Platform.OS === 'ios' ? <TouchableOpacity
                  style = {[styles.siguiente, {backgroundColor: (this.state.direccionEntrega == null || this.state.direccionRecogida == null  || this.state.mandado.trim() == ''? '#bfbfbf' : '#53c678')}]}
                  onPress = {this.confirmar}>

                      <Text style = {styles.textButton}>
                        Confirmar pedido
                      </Text>

                </TouchableOpacity> : 

                <TouchableOpacity
                  style = {[styles.siguienteAndroid, {backgroundColor: (this.state.direccionEntrega == null || this.state.direccionRecogida == null  || this.state.mandado.trim() == ''? '#bfbfbf' : '#53c678')}]}
                  onPress = {this.confirmar}>

                      <Text style = {styles.textButton}>
                        Confirmar pedido
                      </Text>

                </TouchableOpacity> 

              }

                {Platform.OS === 'ios' ? <TouchableOpacity
                  style = {[styles.foto, {backgroundColor: (this.state.direccionEntrega == null || this.state.direccionRecogida == null  || this.state.mandado.trim() == ''? '#afafaf' : '#4ca262')}]}
                  onPress = {this._pickImage}>

                      <Image
                        style = {{width: 50, height: 50}}
                        source = {require('../resources/socialIcons/camerawhite.png')}
                        resizeMode = 'center'
                      />

                   

                </TouchableOpacity> : null}


            </View> 

            {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

        {this.state.seleccionar ? <View style = {{height: height * 0.4, position: 'absolute', bottom: 0}}>

                <TouchableOpacity
                    style = {[styles.cerrar, {backgroundColor: '#53c678'}]}
                    onPress = {this.cerrar}>

                        <Text style = {styles.textButton}>
                          Terminar selección
                        </Text>

                     

                  </TouchableOpacity>

                <CameraRollPicker
                selected = {this.state.images}
                callback={this.getSelectedImages} />
              </View> : null}


         {this.state.showHelp ? <View style = {styles.help}>

          <View style = {styles.helpInfo}>
            <Image
              style = {{width: 50, height: 50}}
              source = {require('../resources/socialIcons/admiration.png')}
              resizeMode = 'contain'
              />


            <Text style = {styles.parrafoHelp}>
              El costo de este servicio es de ${this.state.costos.costo_envio_mandado} pesos + ${this.state.costos.costo_paradas_mandado}
              pesos por paradas adicionales.
            </Text>


            <Text style = {styles.parrafoHelp}>
              Los Riders van en moto o en bicicleta y pueden llevar hasta 9Kg de peso en sus maletas en un espacio de 42x42x42
            </Text>

            <Text style = {styles.parrafoHelp}>
              Llevaremos cualquier cosa que pidas, siempre y cuando sea legal y pueda transportarse. No transportamos ningún tipo
              de mascota o animal.
            </Text>


            <TouchableOpacity style = {styles.botonCerrar} onPress = {() => {this.setState({showHelp: false})}}>
              <Text style = {styles.textoCerrar}>
                CONTINUAR
              </Text>
            </TouchableOpacity>


          </View>

        </View> : null}

      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  scroll: {
    backgroundColor: '#fff',
    width: width,
    paddingBottom: 5,
    flex: 1
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  siguiente: {
    flex: 8,
    height: 50,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  foto: {
    flex: 2,
    height: 50,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'

  },

  cerrar: {
    height: 40,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    width: width

  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

 input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#efefef',
  },

  textinput: {
    color: '#363530', 
    fontSize: 15,
    flex: 1,
    textAlignVertical: 'top'
  },

  button: {
    shadowOffset:{  width: 0,  height: 0,  },
    shadowColor: '#efefef',
    shadowOpacity: 1.0,
    position: 'absolute',
    backgroundColor: '#fff',
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    width: width * 0.9,
    marginBottom: 5
  },

  textButton: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    flex: 1,
    fontWeight: 'bold'
  },

  textName: {
    height: 30,
    color: '#000',
    fontSize: 20,
    textAlign: 'left',
    fontWeight: 'bold'
  },

  buttonseleccionar: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    width: width * 0.9,
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },

  textButtonseleccionar: {
    flex: 1,
    paddingLeft: 10,
    color: '#363530',
    fontSize: 20,
    textAlign: 'left',
  },

  arrow: {
    color: '#fff',
    fontSize: 15,
    textAlign: 'right',
    right: 0,
  },

  circle: {
    borderRadius: 50,
    width: 20,
    height: 20,
    backgroundColor: '#363530',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10
  },

  imagen: {
    borderRadius: 5,
    overflow: 'hidden',
    backgroundColor: '#bebebe',
    width: 30,
    height: 30,
    marginBottom: 2,
  },

  info: {
    height: 30,
    width: width,
    paddingHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },

  icon: {
    paddingHorizontal: 8,
    height: 50,
    alignItems: 'center',
    justifyContent:'center'
  },

  textParadas: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    textAlign: 'left'
  },

  infoPago: {
    fontSize: 20,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },

  infoPagoText: {
    color: '#363530',
    fontSize: 20
  },

  textInfo: {
    color: '#363530',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
    flex: 1
  },

  rowDeseo: {
    paddingVertical: 15,
    width: width * 0.9,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  indicator: {
    top: 200,
    position: 'absolute',
    alignSelf: 'center',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  siguienteAndroid: {
    flex: 1,
    height: 50,
    borderRadius: 15, 
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center'
  },

  help: {
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
    position: 'absolute',
    top: 0,
    left: 0
  },

  helpInfo: {
    backgroundColor: '#fff',
    width: width * 0.9,
    borderRadius: 10,
    padding: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },

  parrafoHelp: {
    fontSize: 15,
    textAlign: 'center',
    color: '#363530',
    marginTop: 30
  },

  botonCerrar: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.7,
    height: 50,
    backgroundColor: '#50dddd',
    borderRadius: 10
  },

  textoCerrar: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  }

});
