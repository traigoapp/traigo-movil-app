import React, {Component} from 'react';
import {Image, StyleSheet, View, Dimensions, Platform, StatusBar} from 'react-native';

import Logo from "../components/logos/Logo"

const {width, height} = Dimensions.get('window');

export default class Splash extends Component {
  render() {
    return (
      <View style = {styles.container}>
        <View style = {{width: width, height: height * 0.4, marginBottom: 50, marginTop: 30}}> 
            <Image
              style = {styles.image}
              source = {require('../resources/logos/only.png')}
              resizeMode = 'contain'
            />

         </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  }

});
