import React, {Component} from 'react';
import {Alert, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});



export default class ConfirmDeseo extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, direccionEntrega: null, deseo: null, direccion: null, costos: null, loading: false};
    this.confirmar = this.confirmar.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillMount(){
    this.setState({usuario: this.props.navigation.getParam('usuario', null)});
    this.setState({direccionEntrega: this.props.navigation.getParam('direccion', null)})
    this.setState({deseo: this.props.navigation.getParam('deseo', null)});
    this.setState({images: this.props.navigation.getParam('images', null)});

    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  confirmar(){
    this.setState({loading: true});
    var data = new FormData();
    data.append('costo_envio', this.state.costos.costo_envio_deseo);
    data.append('comision', this.state.costos.comision_cliente_deseo);
    data.append('correo', this.state.usuario.correo);
    data.append('deseo', this.state.deseo);
    data.append('direccion', JSON.stringify(this.state.direccionEntrega));
    data.append('imageslength', this.state.images.length);
    
    for(var i = 0; i < this.state.images.length; i++){
      data.append('picture' + i, {
        uri: this.state.images[i].uri,
        name: i + '.jpg',
        type: 'image/jpg'
      });
    }

    
 

    fetch('http://traigo.com.co:8000/servicios/realizarpedidodeseo', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
        }else{
          this.setState({loading: false});
          Alert.alert(
            'Pedido realizado',
            'El pedido está siendo procesado, para mas información puede ver los pedidos en curso',
            [
          
              {text: 'OK', onPress: () => {this.props.navigation.dispatch(resetAction)}},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({loading: true});
        alert('Ha ocurrido un error con la conexión');
      });
  }


  onSelect = data => {
    this.setState(data);
  }

  

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Confirmar pedido
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

          <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

         <View style = {styles.title}>
          <Text style = {styles.titleText}>
            Tu pedido
          </Text>
         </View>

        
         <View style = {styles.rowDeseo}>

           <Text style = {styles.textRow}>
            {this.state.deseo == null ? 'Sin deseo' : this.state.deseo}
           </Text>
         </View>
        
         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

          <View style = {styles.title}>
              <Text style = {styles.titleText}>
                Dirección de entrega
              </Text>
             </View>

    
             <TouchableOpacity style = {styles.row}
              onPress = {() => {
                  this.setState({direccion: 'entrega'}, () => {
                    this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: this.state.direccion, onSelect: this.onSelect});
                  });
                  
                }}>

              <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/location.png')}
                  resizeMode = 'center'
                />
              </View>

              <View>
               <Text style = {styles.textRow}>
                {this.state.direccionEntrega == null ? 'Sin direccion' : this.state.direccionEntrega.direccion}
               </Text>

               <Text style = {styles.textRowLow}>
                {this.state.direccionEntrega.notas == '' ? 'Sin notas de la dirección' : this.state.direccionEntrega.notas}
               </Text>

               </View>
             </TouchableOpacity>
             
              <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

          <View style = {styles.title}>
              <Text style = {styles.titleText}>
               Información de pago
              </Text>
            </View>

           

            <View style = {styles.rowDeseo}>
              <View style = {styles.infoPago}>
                <Text style = {styles.infoPagoText}>
                  Valor del envío:
                </Text>
                <Text style = {styles.textInfo}>
                  ${this.state.costos == null ? 0 : this.state.costos.costo_envio_deseo}
                </Text>
              </View>

              <View style = {styles.infoPago}>
                <Text style = {styles.infoPagoText}>
                  Comisión del servicio:
                </Text>
                <Text style = {styles.textInfo}>
                  {this.state.costos == null ? 0 : this.state.costos.comision_cliente_deseo}%
                </Text>
              </View>
            </View>

             <View style = {styles.button}>
              <TouchableOpacity
                  style = {[styles.siguiente, {backgroundColor: '#53c678'}]}
                  onPress = {this.confirmar}>

                      <Text style = {styles.textButton}>
                        Confirmar pedido
                      </Text>

                </TouchableOpacity>

            </View>

            {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width * 0.9,
    height: height * 0.10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },

  rowDeseo: {
    paddingVertical: 15,
    width: width * 0.9,
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 20,
    color: '#363530',
    width: width * 0.9
  },

  textRowLow: {
    paddingLeft: 20,
    fontSize: 15,
    color: '#363530'
  },

  title: {
    width: width * 0.9,
    height: 30,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },

  titleText: {
    fontSize: 15,
    color: '#363530'
  },

  icon: {
    paddingHorizontal: 8,
    height: 50,
    alignItems: 'center',
    justifyContent:'center'
  },

  infoPago: {
    fontSize: 20,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },

  infoPagoText: {
    color: '#363530',
    fontSize: 20
  },

  textInfo: {
    color: '#363530',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
    flex: 1
  },

  button: {
    backgroundColor: '#fff',
    height: 50,
    borderRadius: 10,
    alignItems: 'center',
    flexDirection: 'row',
    width: width * 0.9,
    marginTop: 20
  },

  textButton: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    flex: 1,
    fontWeight: 'bold'
  },

  siguiente: {
    flex: 8,
    height: 50,
    borderRadius: 15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },

  indicator: {
    paddingTop: 50,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  scroll: {
    flex: 1,
    width: width,
  }

});
