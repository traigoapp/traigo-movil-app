import React, {Component} from 'react';
import {Alert, AsyncStorage, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, Platform, Linking, KeyboardAvoidingView, TouchableOpacity, TextInput, Image, StatusBar} from 'react-native';

import LogoTextWhite from '../components/logos/LogoTextWhite'
import FormHead from '../components/form/FormHead'
import WidthButton from '../components/button/WidthButton'
import BasicInput from '../components/input/BasicInput'
import ForgetPassword from '../components/text/ForgetPassword'

import {StackActions, NavigationActions} from 'react-navigation'

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'homeRider'})],
});

const popAction = StackActions.pop({
  n: 1,
});

const {width, height} = Dimensions.get('window');

export default class LoginRider extends Component {

  

  constructor(props){
    super(props);

    this.state = {cedula: '', contrasenia: '', khidden: true, loading: false, usuario: null, direccion: 'entrega'};
    
    this.ingresar = this.ingresar.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
   
  }

  onSelect = data => {
    try {
      AsyncStorage.setItem('usuario', JSON.stringify(this.state.usuario)).then(() => {
        AsyncStorage.setItem('direccion', JSON.stringify(data.direccionEntrega)).then(() => {
          this.props.navigation.dispatch(resetAction);
        });
      });
    } catch (error) {
      alert(error);
    }
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  

  ingresar(){
    Keyboard.dismiss();
    this.setState({loading: true});

    fetch('http://traigo.com.co:8000/servicios/obtenerrider?cedula=' + this.state.cedula)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({loading: false});
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión');
        }else{
          if(responseJson.length == 0){
            alert('La cédula ingresada no se encuentra registrada');
          }else if(responseJson[0].listanegra == 'S'){
            Alert.alert(
            'Cuenta inhabilitada',
            'Hola, ' + responseJson[0].nombre + '\n\nUps! Parece que tu cuenta está inhabilitada. Consulta con el equipo de soporte para enontrar una solución.',
            [
          
              {text: 'OK', onPress: () => console.log('okey')},
            ],
            { cancelable: false }
          )
          }else{
            var usuario = responseJson[0];
            if(usuario.contrasenia == this.state.contrasenia){
              this.setState({usuario: usuario});
              try {
                AsyncStorage.setItem('rider', JSON.stringify(this.state.usuario)).then(() => {
                  this.props.navigation.dispatch(resetAction);
                });
              } catch (error) {
                alert(error);
              }
              
            }else{
              alert('Contraseña incorrecta');
            }
            
          }
          
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión');
      });
  }

  render() {
    return (


      <KeyboardAvoidingView style = {styles.container}
      behavior="padding" enabled>



        {this.state.khidden ? (this.state.loading == false ?<View style = {{width: width, height: height * 0.3, marginBottom: 50}}> 
            <Image
              style = {styles.image}
              source = {require('../resources/logos/textWhite.png')}
              resizeMode = 'contain'
            />

         </View>: <ActivityIndicator size="large" color="#fff" />) : null}


        <TouchableOpacity

          style = {{alignItems: 'flex-start', flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 10}}

          onPress = {() => {
              Keyboard.dismiss();
              this.props.navigation.dispatch(popAction);
            } }
        >

        <View style = {{width: 30, height: 30, marginTop: 5}}>
         <Image
          style = {styles.image}
          source = {require('../resources/socialIcons/xwhite.png')}
          resizeMode = 'contain'
          />
        </View>

      </TouchableOpacity>
        
            <Text style = {styles.hi}>
            ¡Hola Rider!
          </Text>

          <Text style = {styles.message}>
            Ingresa tus datos
          </Text>

           <TextInput
            selectionColor={'#8b8b8b'}
            autoCapitalize = 'none'
            placeholder = {'Cédula'}
            placeholderTextColor = '#8b8b8b'
            underlineColorAndroid='transparent'
            onChangeText = {(text) => {this.setState({cedula: text})}}
            style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>
            </TextInput>

            <TextInput
            selectionColor={'#8b8b8b'}
              secureTextEntry={true}
              placeholder = {'Contraseña'}
              placeholderTextColor = '#8b8b8b'
              underlineColorAndroid='transparent'
              autoCapitalize = 'none'
              onChangeText = {(text) => {this.setState({contrasenia: text})}}
              style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>

        </TextInput>

           <TouchableOpacity
            style = {styles.socialButton}
            onPress = {this.ingresar}>

              <WidthButton
                text = {'Entrar'}
                color = {'#160e66'}
              />

            </TouchableOpacity>

          


      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#50dddd',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  form: {
    marginBottom: 10
  },

  hi: {
    fontSize: 35,
    fontWeight: 'bold',
    color: '#fff'
  },

  message: {
    fontSize: 25,
    color: '#fff',
    marginBottom: 20
  },

  input: {
    marginVertical: 3,
    textAlign: 'center',
    color: '#363530', 
    fontSize: 25,
    borderRadius: 15,
    backgroundColor: '#fff'
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  }

});
