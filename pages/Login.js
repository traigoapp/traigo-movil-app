import React, {Component} from 'react';
import {Alert, AsyncStorage, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, Platform, Linking, KeyboardAvoidingView, TouchableOpacity, TextInput, Image, StatusBar} from 'react-native';

import LogoTextWhite from '../components/logos/LogoTextWhite'
import FormHead from '../components/form/FormHead'
import WidthButton from '../components/button/WidthButton'
import BasicInput from '../components/input/BasicInput'
import ForgetPassword from '../components/text/ForgetPassword'

import {StackActions, NavigationActions} from 'react-navigation'

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});

const {width, height} = Dimensions.get('window');

export default class Login extends Component {

  

  constructor(props){
    super(props);

    this.state = {correo: '', contrasenia: '', khidden: true, loading: false, usuario: null, direccion: 'entrega'};
    
    this.ingresar = this.ingresar.bind(this);
    this.recuperarContrasenia = this.recuperarContrasenia.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
  }

  onSelect = data => {
    try {
      AsyncStorage.setItem('usuario', JSON.stringify(this.state.usuario)).then(() => {
        AsyncStorage.setItem('direccion', JSON.stringify(data.direccionEntrega)).then(() => {
          this.props.navigation.dispatch(resetAction);
        });
      });
    } catch (error) {
      alert(error);
    }
  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  

  ingresar(){
    Keyboard.dismiss();
    this.setState({loading: true});

    fetch('http://traigo.com.co:8000/servicios/obtenercliente?correo=' + this.state.correo)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({loading: false});
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión');
        }else{
          if(responseJson.length == 0){
            alert('El correo ingresado no se encuentra registrado');
          }else{
            var usuario = responseJson[0];
            if(usuario.contrasenia == this.state.contrasenia){
              this.setState({usuario: usuario});
              this.props.navigation.navigate('selectDirection', {usuario: usuario, direccion: 'entrega', onSelect: this.onSelect});
            }else{
              alert('Contraseña incorrecta');
            }
            
          }
          
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión');
      });
  }

  recuperarContrasenia(){
    var data = new FormData();
  
    data.append('correo', this.state.correo);
    
 

    fetch('http://traigo.com.co:8000/servicios/recuperarcontrasenia', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
        }else if(responseJson.notfound){
          Alert.alert(
            'Ups!',
            'El correo ingresado no se encuentra registrado',
            [
          
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
          )
        }else{
          this.setState({loading: false});
          Alert.alert(
            'Contraseña enviada',
            'La contraseña ha sido enviada al correo electrónico ingresado',
            [
          
              {text: 'OK', onPress: () => {}},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión: ' + error);
      });
  }

  render() {
    return (


      <KeyboardAvoidingView style = {styles.container}
      behavior="padding" enabled>



        {this.state.khidden ? (this.state.loading == false ?<View style = {{width: width, height: height * 0.3, marginBottom: 50}}> 
            
            <Image
              style = {styles.image}
              source = {require('../resources/logos/textWhite.png')}
              resizeMode = 'contain'
            />
            

         </View>: <ActivityIndicator size="large" color="#fff" />) : null}


        <TouchableOpacity

          style = {{alignItems: 'flex-start', flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 10}}

          onPress = {() => {
              Keyboard.dismiss();
              this.props.navigation.dispatch(popAction);
            } }
        >

        <View style = {{width: 30, height: 30, marginTop: 5}}>
         <Image
          style = {styles.image}
          source = {require('../resources/socialIcons/xwhite.png')}
          resizeMode = 'contain'
          />
        </View>

      </TouchableOpacity>
        
            <Text style = {styles.hi}>
            ¡Hola!
          </Text>

          <Text style = {styles.message}>
            Ingresa tus datos
          </Text>

           <TextInput
            selectionColor={'#8b8b8b'}
            autoCapitalize = 'none'
            placeholder = {'Correo electrónico'}
            placeholderTextColor = '#8b8b8b'
            underlineColorAndroid='transparent'
            keyboardType = {'email-address'}
            onChangeText = {(text) => {this.setState({correo: text})}}
            style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>
            </TextInput>

            <TextInput
            selectionColor={'#8b8b8b'}
              secureTextEntry={true}
              placeholder = {'Contraseña'}
              placeholderTextColor = '#8b8b8b'
              underlineColorAndroid='transparent'
              autoCapitalize = 'none'
              onChangeText = {(text) => {this.setState({contrasenia: text})}}
              style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>

        </TextInput>

        <TouchableOpacity style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}
        onPress = {() => {
          this.recuperarContrasenia();
        }}>
          <Image
            style = {{width: 30, height: 30}}
            source = {require('../resources/socialIcons/lock.png')}
            resizeMode = 'center'
          />


          <Text
          onPress = {this.recuperarContrasenia}
          style = {{color: '#fff'}}>

            Olvidé mi contraseña
          </Text>
        </TouchableOpacity>

           <TouchableOpacity
            style = {styles.socialButton}
            onPress = {this.ingresar}>

              <WidthButton
                text = {'Entrar'}
                color = {'#160e66'}
              />

            </TouchableOpacity>

          


      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#50dddd',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  form: {
    marginBottom: 10
  },

  hi: {
    fontSize: 35,
    fontWeight: 'bold',
    color: '#fff'
  },

  message: {
    fontSize: 25,
    color: '#fff',
    marginBottom: 20
  },

  input: {
    marginVertical: 3,
    textAlign: 'center',
    color: '#363530', 
    fontSize: 25,
    borderRadius: 15,
    backgroundColor: '#fff'
  },

  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  }

});
