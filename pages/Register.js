import React, {Component} from 'react';
import {ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import { StackActions } from 'react-navigation';
import WidthButton from '../components/button/WidthButton'

const popAction = StackActions.pop({
  n: 1,
});

const {width, height} = Dimensions.get('window');

export default class Register extends Component {

  constructor(props){
    super(props);

    this.state = {nombre: '', correo: '', telefono: '', contrasenia: '', khidden: true, loading: false};
    this.registrar = this.registrar.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);

  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({khidden: false});
  }

  _keyboardDidHide(){
    this.setState({khidden: true});
  }

  

  registrar(){



    Keyboard.dismiss();
    this.setState({loading: true});

    if(this.state.correo.trim() == '' || this.state.contrasenia.trim() == '' || this.state.nombre.trim() == '' || this.state.telefono.trim() == ''){
      alert('Debe completar todos los datos');
      this.setState({loading: false});
      return;
    }

    fetch('http://traigo.com.co:8000/servicios/obtenercliente?correo=' + this.state.correo)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({loading: false});
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión');
        }else{
          if(responseJson.length == 0){
            this.props.navigation.navigate('addressSelection', this.state);
          }else{
            alert('El correo ingresado ya se encuentra registrado');
          }
          
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión');
      });
    
  }

  render() {
    return (
      <KeyboardAvoidingView style = {styles.container}
      behavior="padding" enabled>

        {this.state.khidden ? (this.state.loading == false ?<View style = {{width: width, height: height * 0.3, marginBottom: 50}}> 
            <Image
              style = {styles.image}
              source = {require('../resources/logos/text.png')}
              resizeMode = 'contain'
            />

         </View>: <ActivityIndicator size="large" color="#fff" />) : null}


          <TouchableOpacity

          style = {{alignItems: 'flex-start', flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 10}}

          onPress = {() => {
              Keyboard.dismiss();
              this.props.navigation.dispatch(popAction);
            } }
        >

        <View style = {{width: 30, height: 30, marginTop: 5}}>
         <Image
          style = {styles.image}
          source = {require('../resources/socialIcons/xblack.png')}
          resizeMode = 'contain'
          />
        </View>

      </TouchableOpacity>

            

             <Text style = {styles.hi}>
            Regístrate para empezar.
          </Text>

          <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>
          
             <View style = {styles.iconInput}>
               <Image
                  style = {styles.image}
                  source = {require('../resources/socialIcons/personblack.png')}
                  resizeMode = 'center'
                />
              </View>


              <TextInput
                selectionColor={'#8b8b8b'}
                  placeholder = {'Nombre'}
                  placeholderTextColor = '#8b8b8b'
                  underlineColorAndroid='transparent'
                  keyboardType = {this.props.type}
                  onChangeText = {(text) => {this.setState({nombre: text})}}
                  style = {{flex: 1,  fontSize: 25}}
                  >

             </TextInput>
            </View>


            <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>
          
             <View style = {styles.iconInput}>
               <Image
                  style = {styles.image}
                  source = {require('../resources/socialIcons/emailblack.png')}
                  resizeMode = 'center'
                />
              </View>


              <TextInput
                  selectionColor={'#8b8b8b'}
                  placeholder = {'Correo electrónico'}
                  autoCapitalize = 'none'
                  keyboardType = {'email-address'}
                  placeholderTextColor = '#8b8b8b'
                  underlineColorAndroid='transparent'
                  onChangeText = {(text) => {this.setState({correo: text})}}
                  style = {{flex: 1,  fontSize: 25}}
                  >

             </TextInput>
            </View>


            <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50 }]}>
          

              <View style = {styles.iconInput}>
               <Image
                  style = {styles.image}
                  source = {require('../resources/socialIcons/lockblack.png')}
                  resizeMode = 'center'
                />
              </View>


              <TextInput
                selectionColor={'#8b8b8b'}
                  secureTextEntry={true}
                  autoCapitalize = 'none'
                  placeholder = {'Contraseña'}
                  placeholderTextColor = '#8b8b8b'
                  underlineColorAndroid='transparent'
                  keyboardType = {this.props.type}
                  onChangeText = {(text) => {this.setState({contrasenia: text})}}
                  style = {{flex: 1,  fontSize: 25}}
                  >

             </TextInput>
            </View>


            <View style = {[styles.input, {width: (Dimensions.get('window').width * 0.8), height: 50}]}>
          
             <View style = {styles.iconInput}>
               <Image
                  style = {styles.image}
                  source = {require('../resources/socialIcons/bookblack.png')}
                  resizeMode = 'center'
                />
              </View>


              <TextInput
                selectionColor={'#8b8b8b'}
                  placeholder = {'Teléfono'}
                  keyboardType= 'numeric'
                  placeholderTextColor = '#8b8b8b'
                  underlineColorAndroid='transparent'
                  onChangeText = {(text) => {this.setState({telefono: text})}}
                  style = {{flex: 1,  fontSize: 25}}
                  >

             </TextInput>
            </View>          

            <TouchableOpacity
            style = {{marginTop: 10}}
            onPress = {this.registrar}>

              <WidthButton
                text = {'¡REGISTRARME!'}
                color = {'#50dddd'}
              />

            </TouchableOpacity>

         


            

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'center', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },


  image: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },

  iconInput: {width: 30, height: 30, alignSelf: 'center', marginLeft: 5, marginRight: 5}

});
