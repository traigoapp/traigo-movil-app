import React, {Component} from 'react';
import {Alert, RefreshControl, AsyncStorage, Switch, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';


import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

const {width, height} = Dimensions.get('window');

export default class HomeRider extends Component {

  constructor(props){
    super(props);
    this.state = {appcode: 5, opacity: 1, limitePedidos: 0, pedidosRider: [], costos: {}, activado: false, rider: null, tomarPedidos: false, pedidos: [], refreshing: false, costos: {}, paradas: [], listanegra: false};
    this.enviarCoordenadas = this.enviarCoordenadas.bind(this);
    this.refrescarPedidos = this.refrescarPedidos.bind(this);
    this.actualizarLimitePedidos = this.actualizarLimitePedidos.bind(this);
    this.obtenerTotalPedidos = this.obtenerTotalPedidos.bind(this);
    this.tomarPedido = this.tomarPedido.bind(this);
  }

  _onRefresh = () => {
    this.setState({refreshing: true});
    this.actualizarLimitePedidos();


    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
    })
    .catch((error) => {
       this.setState({refreshing: false});
      console.error(error);
    });
  }

  enviarCoordenadas(){
    if(this.state.activado){

      try{
        navigator.geolocation.getCurrentPosition((position) => {
          const lat = position.coords.latitude;
          const lon = position.coords.longitude;

          var data = new FormData();
          data.append('cedula', this.state.rider.cedula);
          data.append('latitude', lat);
          data.append('longitude', lon);

          fetch('http://traigo.com.co:8000/servicios/actualizarcoordenadasrider', {

            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: data
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.code){
              alert('Ha ocurrido un error con la conexión')
            }else{
              setTimeout(() => {
                if(this.state.activado)
                  this.enviarCoordenadas();
              }, 15000);
            }
          })
          .catch((error) => {
            this.setState({activado: false, tomarPedidos: false});
            alert('Ha ocurrido un error con la conexión: ' + error);
          });

        }, (error) => console.log(JSON.stringify(error)),
        {enableHighAccuracy: Platform.OS != 'android', timeout: 20000});
      }catch(error){
        alert(error);
      }
    }
  }

  obtenerTotalPedidos(pedidos){
    return new Promise((resolve, reject) => {
      let fetches = [];
      let totales = [];
      for(let i = 0; i < pedidos.length; i++){
        if(pedidos[i].type == 'tienda'){
          fetches.push(
            fetch('http://traigo.com.co:8000/servicios/obtenertotalpedidocliente?idpedido=' + pedidos[i].idpedido_clientes)
            .then((response) => response.json())
            .then((responseJson) => {
              totales[i] = responseJson.total;
            })
            .catch((error) => {
              alert('Ha ocurrido un error con la conexión: ' + error);
            })
          )
          
        }else{
          fetches.push(totales[i] = 0)
        }
      }

      Promise.all(fetches).then(() => {
        resolve(totales);
      })

    });
  }

  refrescarPedidos(){
    fetch('http://traigo.com.co:8000/servicios/obtenerpedidosenproceso?appcode=' + this.state.appcode )
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code == 1){
          reject(responseJson.err)
        }
        this.obtenerTotalPedidos(responseJson).then((totales) => {
          for(var i = 0; i < responseJson.length; i++){
            if(responseJson[i].type == 'tienda'){
              responseJson[i].total = totales[i];
            }
          }
          this.setState({refreshing: false, pedidos: responseJson});
        }).catch((err) => alert('Ha ocurrido un error con la conexión: ', err));
      })
      .catch((err) => {
        alert('Ha ocurrido un error con la conexión: ', err);
      });
  }

  obtenerLimitePedidos(rider){
    return new Promise((resolve, reject) => {
      fetch('http://traigo.com.co:8000/servicios/obtenerrider?cedula=' + rider.cedula)
      .then((response) => response.json())
      .then((responseJson) => {
        resolve(responseJson[0].limitePedidos);
      })
      .catch((error) => {
        reject('Ha ocurrido un error con la conexión: ' + error);
      });
    });
  }

  contarPedidosTomados(rider){
    return new Promise((resolve, reject) => {
      fetch('http://traigo.com.co:8000/servicios/contarpedidostomados?cedula=' + rider.cedula)
      .then((response) => response.json())
      .then((responseJson) => {
        resolve(responseJson.total);
      })
      .catch((error) => {
        reject('Ha ocurrido un error con la conexión');
      });
    });
  }

  actualizarLimitePedidos(){
      this.refrescarPedidos();
      AsyncStorage.getItem('rider').then((rider) => {
        rider = JSON.parse(rider);
        Promise.all([this.obtenerLimitePedidos(rider), this.contarPedidosTomados(rider)])
        .then((values) => {
          this.setState(
            {
              refreshing: false,
              limitePedidos: values[0],
              pedidosRider: values[1]
            }
          )
        }).catch((err) => {
          this.setState({refreshing: false});
          alert(err);
        });
      })
  }

  componentWillMount(){
    this.actualizarLimitePedidos();
    
    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });


    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });
  }

  componentDidMount(){
    
    this.forceUpdate();

    if(Platform.OS === 'android'){
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message: "<h2>Activar localización ?</h2>Traigo App utiliza la configuración de tu dispositivo<br/><br/>",
        ok: "Permitir",
        cancel: "Denegar",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
            preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
            providerListener: false // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
          }).then(function(success) {
            // success => {alreadyEnabled: true, enabled: true, status: "enabled"} 
           


            AsyncStorage.getItem('rider').then((dato) => {
              if(dato !== null){
                this.setState({rider: JSON.parse(dato)}, () => {
                  fetch('http://traigo.com.co:8000/servicios/obtenerestadorider?cedula=' + this.state.rider.cedula)
                  .then((response) => response.json())
                  .then((responseJson) => {
                    var estado = responseJson.estado;
                    var listanegra = responseJson.listanegra;
                    this.setState({listanegra: listanegra, activado: estado, tomarPedidos: estado}, () => {
                      if(this.state.activado){
                        this.enviarCoordenadas();
                      }
                    });
                    

                  })
                  .catch((error) => {
                    alert(error);
                  });
                })
              }else{
                alert('Error en la aplicacion');
              }
            });
          }.bind(this)
          ).catch((error) => {
            console.log(error.message);
          });
        }else{
          
          AsyncStorage.getItem('rider').then((dato) => {
            if(dato !== null){
              this.setState({rider: JSON.parse(dato)}, () => {
                fetch('http://traigo.com.co:8000/servicios/obtenerestadorider?cedula=' + this.state.rider.cedula)
                .then((response) => response.json())
                .then((responseJson) => {
                  var estado = responseJson.estado;
                  var listanegra = responseJson.listanegra;

                  this.setState({listanegra: listanegra, activado: estado, tomarPedidos: estado}, () => {
                      if(this.state.activado){
                        this.enviarCoordenadas();
                      }
                    });

                })
                .catch((error) => {
                  alert(error);
                });
              })
            }else{
              alert('Error en la aplicacion');
            }
          });

        }

      }


  tomarPedidoMandado(pedido){
    if(pedido.type == 'mandado'){
      fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidoencomienda?pedido=' + pedido.idpedido_encomienda)
      .then((response) => response.json())
      .then((responseJson) => {
        var estado = responseJson.estado;
        if(estado != 'En proceso'){
          Alert.alert(
            'Ups!',
            'El pedido ya fue tomado por un rider ',
            [
            {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
            ],
            { cancelable: false }
            )
        }else{
          var data = new FormData();
          data.append('cedularider', this.state.rider.cedula);
          data.append('idpedidoencomienda', pedido.idpedido_encomienda);
          data.append('estado', 'Tomado por el Rider');

          fetch('http://traigo.com.co:8000/servicios/cambiarestadopedidoencomienda', {

            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: data
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.code){
              alert('Ha ocurrido un error con la conexión')
            }else if(responseJson.update == 0){
                	Alert.alert(
	                'Ups!',
	                'El pedido ya fue tomado por un rider ',
	                [
	                  {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
	                ],
	                  { cancelable: false }
	                )
            }else{
              pedido.estado = 'Tomado por el Rider';
              this.props.navigation.navigate('pedidoEncomienda', {rider: this.state.rider, pedido: pedido, onRefresh: this._onRefresh});
              this.actualizarLimitePedidos();
              
            }
          })
          .catch((error) => {
            this.setState({activado: false, tomarPedidos: false});
            alert('Ha ocurrido un error con la conexión: ' + error);
          });
        }
        
      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión');
      });
    }
  }

  tomarPedidoDeseo(pedido){
    if(pedido.type == 'deseo'){

      fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidodeseo?pedido=' + pedido.idpedido_deseo)
      .then((response) => response.json())
      .then((responseJson) => {
        var estado = responseJson.estado;
        if(estado != 'En proceso'){
          Alert.alert(
            'Ups!',
            'El pedido ya fue tomado por un rider ',
            [
            {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
            ],
            { cancelable: false }
            )
        }else{
          var data = new FormData();
          data.append('cedularider', this.state.rider.cedula);
          data.append('idpedidodeseo', pedido.idpedido_deseo);
          data.append('estado', 'Tomado por el Rider');

          fetch('http://traigo.com.co:8000/servicios/cambiarestadopedidodeseo', {

            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: data
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.code){
              alert('Ha ocurrido un error con la conexión')
            }else if(responseJson.update == 0){
                	Alert.alert(
	                'Ups!',
	                'El pedido ya fue tomado por un rider ',
	                [
	                  {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
	                ],
	                  { cancelable: false }
	                )
            }else{
              pedido.estado = 'Tomado por el Rider';
              this.props.navigation.navigate('pedidoDeseo', {rider: this.state.rider, pedido: pedido, onRefresh: this._onRefresh});
             
              this.actualizarLimitePedidos();
            }
          })
          .catch((error) => {
            this.setState({activado: false, tomarPedidos: false});
            alert('Ha ocurrido un error con la conexión: ' + error);
          });
        }

      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión');
      });
    }
  }

  tomarPedidoTienda(pedido){
    if(pedido.type == 'tienda'){

      fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidocliente?pedido=' + pedido.idpedido_clientes)
      .then((response) => response.json())
      .then((responseJson) => {
        var estado = responseJson.estado;
        if(estado != 'En proceso'){
          Alert.alert(
            'Ups!',
            'El pedido ya fue tomado por un rider ',
            [
            {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
            ],
            { cancelable: false }
            )
        }else{
          var data = new FormData();
          data.append('cedularider', this.state.rider.cedula);
          data.append('idpedidocliente', pedido.idpedido_clientes);
          data.append('estado', 'Tomado por el Rider');

          fetch('http://traigo.com.co:8000/servicios/cambiarestadopedidocliente', {

            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: data
          })
          .then((response) => response.json())
          .then((responseJson) => {
            if(responseJson.code){
              alert('Ha ocurrido un error con la conexión')
            }else if(responseJson.update == 0){
                	Alert.alert(
	                'Ups!',
	                'El pedido ya fue tomado por un rider ',
	                [
	                  {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
	                ],
	                  { cancelable: false }
	                )
             }else{
              pedido.estado = 'Tomado por el Rider';
              this.props.navigation.navigate('pedidoTienda', {rider: this.state.rider, pedido: pedido, onRefresh: this._onRefresh});
              
              this.actualizarLimitePedidos();
            }
          })
          .catch((error) => {
            this.setState({activado: false, tomarPedidos: false});
            alert('Ha ocurrido un error con la conexión: ' + error);
          });
        }

      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión');
      });
    }
  }

  tomarPedido(pedido){
    fetch('http://traigo.com.co:8000/servicios/obtenerrider?cedula=' + this.state.rider.cedula)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({limitePedidos: responseJson[0].limitePedidos}, () => {
          fetch('http://traigo.com.co:8000/servicios/contarpedidostomados?cedula=' + this.state.rider.cedula)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({pedidosRider: responseJson.total}, () => {
              if(this.state.limitePedidos != null && this.state.pedidosRider >= this.state.limitePedidos){
                this.actualizarLimitePedidos();
                alert('Ups! No puedes tomar más pedidos.');
              }else{
                switch(pedido.type){
                  case 'empresariales':
                    this.tomarPedidoEmpresarial(pedido);
                  break;
                  case 'deseo':
                    this.tomarPedidoDeseo(pedido);
                  break;
                  case 'tienda':
                    this.tomarPedidoTienda(pedido);
                  break;
                  case 'mandado':
                    this.tomarPedidoMandado(pedido);
                  break;
                }
              }
            });
          })
          .catch((error) => {
            alert('Ha ocurrido un error con la conexión');
          });
        });
      })
      .catch((error) => {
        alert('Ha ocurrido un error con la conexión: ' + error);
      });
  }

      tomarPedidoEmpresarial(pedido){
        if(pedido.type == 'empresariales'){
          fetch('http://traigo.com.co:8000/servicios/obtenerestadopedidoempresarial?pedido=' + pedido.idpedido_empresarial)
          .then((response) => response.json())
          .then((responseJson) => {
            var estado = responseJson.estado;
            if(estado != 'En proceso'){
              Alert.alert(
                'Ups!',
                'El pedido ya fue tomado por un rider ',
                [
                  {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
                ],
                  { cancelable: false }
                )
            }else{
              var data = new FormData();
              data.append('cedularider', this.state.rider.cedula);
              data.append('idpedidoempresarial', pedido.idpedido_empresarial);
              data.append('estado', 'Rider en camino');

              fetch('http://traigo.com.co:8000/servicios/cambiarestadopedidoempresarial', {

                method: 'POST',
                headers: {
                  'Content-Type': 'multipart/form-data',
                },
                body: data
              })
              .then((response) => response.json())
              .then((responseJson) => {
                if(responseJson.code){
                  alert('Ha ocurrido un error con la conexión')
                }else if(responseJson.update == 0){
                  Alert.alert(
                  'Ups!',
                  'El pedido ya fue tomado por un rider ',
                  [
                    {text: 'Okey', onPress: () => this.actualizarLimitePedidos(), style: 'cancel'}
                  ],
                    { cancelable: false }
                  )
                }else{

                  
                  this.actualizarLimitePedidos();
                  pedido.estado = 'Rider en camino';
                  this.props.navigation.navigate('pedidoEmpresarial', {rider: this.state.rider, pedido: pedido, onRefresh: this._onRefresh});
                  
                }
              })
              .catch((error) => {
                this.setState({activado: false, tomarPedidos: false});
                alert('Ha ocurrido un error con la conexión: ' + error);
              });
            }
            
          })
          .catch((error) => {
            alert('Ha ocurrido un error con la conexión');
          });
        }
    
      }

      cambiarEstado(){
        this.actualizarLimitePedidos();
        
        fetch('http://traigo.com.co:8000/servicios/obtenerestadorider?cedula=' + this.state.rider.cedula)
        .then((response) => response.json())
        .then((responseJson) => {

          var listanegra = responseJson.listanegra;
          if(listanegra){
            Alert.alert(
              'Cuenta inhabilitada',
              'Hola, ' + this.state.rider.nombre + '\n\nUps! Parece que tu cuenta está inhabilitada. Consulta con el equipo de soporte para enontrar una solución.',
              [

              {text: 'OK', onPress: () => {this.setState({activado: false, tomarPedidos: false})}},
              ],
              { cancelable: false }
              )
          }else{
            var data = new FormData();
            data.append('cedula', this.state.rider.cedula);
            data.append('estado', this.state.activado ? 'S' : 'N');

            fetch('http://traigo.com.co:8000/servicios/cambiarestadorider', {

              method: 'POST',
              headers: {
                'Content-Type': 'multipart/form-data',
              },
              body: data
            })
            .then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.code){
                alert('Ha ocurrido un error con la conexión')
              }else{
                this.setState({tomarPedidos: true}, this.enviarCoordenadas);
              }
            })
            .catch((error) => {
              this.setState({activado: false, tomarPedidos: false});
              alert('Ha ocurrido un error con la conexión');
            });
          }

        })
        .catch((error) => {
          alert(error);
        });
        


        
        
      }

      renderPedido(item, index){
        if(item.type == 'deseo'){
          return(
            <TouchableOpacity style = {[styles.pedido, {opacity: this.state.opacity}]}
            onPress = {() => {
              if(this.state.limitePedidos != null && this.state.pedidosRider >= this.state.limitePedidos){
                alert('Ups! No puedes tomar mas pedidos');
                this.actualizarLimitePedidos();
              }else{
                    Alert.alert(
                      'Tomar pedido',
                      'Confirmar',
                      [
                        {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'Tomar', onPress: () => this.tomarPedido(item)},
                      ],
                      { cancelable: false }
                    )
                  }}
                }>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  LO QUE QUIERAS!
                </Text>

                <View>
                  <Text style = {styles.tiempoentrega}>
                    45 min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
              </View>

              <Text style = {styles.datafono}>
                El cliente quiere:
              </Text>

              <Text style = {styles.descripcionPedido}>
                {item.deseo}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Destino
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionentrega + ' | ' + item.notasdireccionentrega}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costo_envio}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'mandado'){

          item.paradas = [];
          for(var i = 0; i < this.state.paradas.length; i++){

            if(this.state.paradas[i].idpencomienda == item.idpedido_encomienda){
              item.paradas.push(this.state.paradas[i]);
            }
          }


          return(
              
            <TouchableOpacity style = {[styles.pedido, {opacity: this.state.opacity}]}
            onPress = {() => {
              if(this.state.limitePedidos != null && this.state.pedidosRider >= this.state.limitePedidos){
                alert('Ups! No puedes tomar mas pedidos');
                this.actualizarLimitePedidos();
              }else{
                    Alert.alert(
                      'Tomar pedido',
                      'Confirmar',
                      [
                        {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'Tomar', onPress: () => this.tomarPedido(item)},
                      ],
                      { cancelable: false }
                    )
                  }}
                }>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  MANDADO
                </Text>

                <View>
                  <Text style = {styles.tiempoentrega}>
                    45 min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
              </View>

              <Text style = {styles.datafono}>
                Mandado
              </Text>

              <Text style = {styles.descripcionPedido}>
                {item.mandado}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de recogida
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionorigen + ' | ' + item.notasdireccionorigen}
                  </Text>
                </View>
              </View>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direcciondestino + ' | ' + item.notasdirecciondestino}
                  </Text>
                </View>
              </View>

               <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Cantidad de paradas adicionales
                  </Text>
                  <Text style = {styles.textoDireccion}>
                    {item.paradas.length}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + (item.costo_envio + item.paradas.length * this.state.costos.costo_paradas_mandado) * (100 - item.comision_rider) / 100}
                  </Text>
                </View>
              </View>



            </TouchableOpacity>
            )
        }else if(item.type == 'tienda'){
          return(
            <TouchableOpacity style = {[styles.pedido, {opacity: this.state.opacity}]}
           onPress = {() => {
                if(this.state.limitePedidos != null && this.state.pedidosRider >= this.state.limitePedidos){
                  alert('Ups! No puedes tomar mas pedidos');
                  this.actualizarLimitePedidos();
                }else{
                    Alert.alert(
                      'Tomar pedido',
                      'Confirmar',
                      [
                        {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                        {text: 'Tomar', onPress: () => this.tomarPedido(item)},
                      ],
                      { cancelable: false }
                    )
                  }}
                }>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  {item.nombresucursal.toUpperCase()}
                </Text>

                
                <View>
                  <Text style = {styles.tiempoentrega}>
                    {item.tiempo_entrega} min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
                
              </View>

              <Text style = {styles.datafono}>
                {'Efectivo'}
              </Text>

              <Text style = {styles.precio}>
                ${item.total}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccion + ' | ' + item.notas}
                  </Text>
                </View>
              </View>


              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costo_envio * (1 - item.comision_rider / 100)}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'empresariales'){
          return(
            <TouchableOpacity style = {[styles.pedido, {opacity: this.state.opacity}]}
                  onPress = {() => {
                    if(this.state.limitePedidos != null && this.state.pedidosRider >= this.state.limitePedidos){
                      alert('Ups! No puedes tomar mas pedidos');
                      this.actualizarLimitePedidos();
                    }else{
                      Alert.alert(
                        'Tomar pedido',
                        'Confirmar',
                        [
                          {text: 'Cancelar', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                          {text: 'Tomar', onPress: () => this.tomarPedido(item)},
                        ],
                        { cancelable: false }
                      )
                    }
                  }}>
              <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                {item.nombresucursal.toUpperCase()}
                </Text>
                
                <View>
                  <Text style = {styles.tiempoentrega}>
                    {item.tiempo_entrega} min
                  </Text>

                  <View style = {styles.partner}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      partner
                    </Text>
                  </View>
                </View>

              </View>


              


              <Text style = {styles.datafono}>
                {item.datafono == 'N' ? 'Efectivo' : 'Datáfono'}
              </Text>

              <Text style = {styles.precio}>
                ${item.valor}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Destino
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionentrega + ' | ' + item.notasdireccionentrega}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costoenvio * (1 - item.comision_rider / 100)}
                  </Text>

                  {item.retiene == 'S' ? 

                    <View style = {styles.retenido}>
                        <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 10}}>
                          Retenido
                        </Text>
                    </View>
                  : null}
                 

                </View>
              </View>

            </TouchableOpacity>
            )

        }
      }

      render() {
        return(


          <View style = {styles.container}>
          <View style = {styles.header}>

          <View style = {styles.divileft}>

          <TouchableOpacity
            onPress = {() => {
              this.props.navigation.navigate('menuRider', {onRefresh: this._onRefresh});
            }}>
            <Image
            style = {{width: 30, height: 30}}
            source = {require('../resources/socialIcons/menu.png')}
            resizeMode = 'center'
          />
          </TouchableOpacity>


          </View>

          <View style = {styles.divi}>
          <Image
          style = {{width: width * 0.25, height: 50}}
          source = {require('../resources/logos/traigoblack.png')}
          resizeMode = 'center'
          />
          </View>

          <View style = {styles.diviright}>

          <Text style = {styles.encurso}>
          Hola, {this.state.rider == null ? 'Sin nombre' : this.state.rider.nombre.split(' ')[0]}
          </Text>


          </View>
          
          </View>

          <View style = {{backgroundColor: '#e5e5e5', width: width, height: 1, alignSelf: 'center'}}>
                   </View>

          <View style = {styles.scroll}>

          <View style = {styles.activate}>
            <Switch
              onValueChange = {(value) => { this.setState({activado: value}, this.cambiarEstado) }}
              value = {this.state.activado}
            />

            <View style = {styles.holarider}>
              <Text style = {{fontSize: 25, fontWeight: 'bold', marginBottom: -8, color: '#363530'}}>
                Hola Rider
              </Text>

              <Text style = {{fontSize: 20, color: '#363530'}}>
                {this.state.activado == true ? (this.state.pedidosRider < this.state.limitePedidos || this.state.limitePedidos == null ? 'Puedes tomar pedidos' : 'Ups! limite alcanzado'): 'Activate para empezar'}
              </Text>

              {this.state.limitePedidos != null ? 
                <Text style = {{fontSize: 10, color: '#363530'}}>
                  {this.state.pedidosRider + " pedidos tomados | Limite simultaneos: " + this.state.limitePedidos}
                </Text>
              :null}

            </View>
          </View>

          

          {this.state.activado ? 



            <View style = {styles.flatlist}>

            <FlatList 
            
            refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
              />}
              data = {this.state.pedidos}
              extraData = {this.state}
              renderItem = {({item, index}) => this.renderPedido(item, index)}
              keyExtractor = {(item, index) => index.toString()}
              />


              </View>

              :

              <View style = {styles.terminos}>


              <Text style = {{fontSize: 12, color: '#363530'}}>
              Al activarte aceptas nuestros
              </Text>

              <Text style = {{fontSize: 12, textDecorationLine: 'underline', color: '#363530'}}
              onPress = {() => Linking.openURL('https://www.traigo.com.co/terminos-y-condiciones')}>
              terminos y condiciones
              </Text>

              </View>

            }


            </View>

            </View>



            )
      }
    }

    const styles = StyleSheet.create({
    	container: {
    		flex:1,
    		alignItems: 'flex-start',
    		paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    		backgroundColor: '#ffffff'
    	},

    	header: {
    		flexDirection: 'row',
    		backgroundColor: '#fff',
    		height: 60,
    		width: width,
    		alignItems: 'center',
    		justifyContent: 'space-between'
    	},

    	traigo: {
    		color: '#363530',
    		fontSize: 25,
    		fontWeight: 'bold'
    	},

    	divi: {
    		width: width * 0.33,
    		flexDirection: 'row',
    		alignItems: 'center',
    		justifyContent: 'center'
    	},

    	divileft: {
    		paddingLeft: 10,
    		width: width * 0.33,
    		flexDirection: 'row',
    		alignItems: 'center',
    		justifyContent: 'flex-start'
    	},

    	diviright: {
    		paddingRight: 10,
    		width: width * 0.33,
    		flexDirection: 'row',
    		alignItems: 'center',
    		justifyContent: 'flex-end'
    	},

    	scroll: {
    		backgroundColor: '#fff',
    		flex: 1,
    		width: width,
    		paddingBottom: 5
    	},

    	activate: {
    		marginTop: 10,
    		flexDirection: 'row',
    		alignItems: 'center',
    		justifyContent: 'center'
    	},

    	holarider: {
    		marginLeft: 10,
    	},

    	terminos: {
    		marginTop: 30,
    		alignItems: 'center',
    		justifyContent: 'center'
    	},

    	flatlist: {
    		marginTop: 10,
    		flex: 1,
    		backgroundColor: '#fff',
    		width: width,
    	},

    	pedido: {
    		width: width * 0.8,
    		borderRadius: 15,
    		backgroundColor: '#fff',
    		alignSelf: 'center',
    		marginVertical: 5,
    		borderColor: '#afafaf',
        borderWidth: 1,
      },
    
    	infotienda: {

    		width: width * 0.8,
    		flexDirection: 'row',
    		paddingVertical: 5,
    		alignItems: 'center',
    		paddingRight: 5,
    		paddingLeft: 5,
    		justifyContent: 'flex-start'
    	},

    	titulosucursal: {
    		width: width * 0.6,
    		fontSize: 20,
    		fontWeight: 'bold',
    		color: '#363530',
    		textAlign: 'left',

    	},

    	tiempoentrega: {
    		fontSize: 15,
    		fontWeight: 'bold',
    		color: '#363530',
    		textAlign: 'center',
    		flex: 1,
    	},

    	partner: {
    		marginLeft: 2,
    		width: width * 0.15,
    		height: 15,
    		borderRadius: 25,
    		backgroundColor: '#363530',
    		justifyContent: 'center',
    		alignItems: 'center',
    		paddingBottom: 2
    	},

    	app: {
    		marginLeft: 2,
    		width: width * 0.15,
    		height: 15,
    		borderRadius: 25,
    		backgroundColor: '#50dddd',
    		justifyContent: 'center',
    		alignItems: 'center',
    		paddingBottom: 2
    	},

    	datafono: {
    		paddingLeft: 10,
    		color: '#afafaf',
    		fontSize: 15,
    		textAlign: 'left'
    	},

    	precio: {
    		marginTop: -5,
    		paddingLeft: 10,
    		color: '#363530',
    		fontSize: 20,
    		textAlign: 'left'
    	},


    	descripcionPedido:{
    		marginTop: -5,
    		paddingLeft: 10,
    		paddingRight: 5,
    		color: '#363530',
    		fontSize: 15,
    		textAlign: 'left'
    	},

    	destino: {
    		paddingLeft: 30,
    		height: 60,
    		alignItems: 'center',
    		flexDirection: 'row'
    	},

    	textoDestino: {
    		fontSize: 15,
    		color: '#363530',
    		fontWeight: 'bold',
    		textAlign: 'left'
    	},

    	textoDireccion: {
    		marginTop: -5,
    		fontSize: 15,
    		color: '#afafaf',
    		textAlign: 'left',
    		marginRight: 10
    	},

    	retenido: {
    		marginTop: -10,
    		marginLeft: 2,
    		width: width * 0.15,
    		height: 15,
    		borderRadius: 25,
    		backgroundColor: '#fba312',
    		justifyContent: 'center',
    		alignItems: 'center'
    	},

    	infoganancia: {
    		height: 30,
    		width: width * 0.8,
    		flexDirection: 'row',
    		paddingVertical: 5,
    		alignItems: 'center',
    		paddingHorizontal: 10,
    		justifyContent: 'flex-start',
    	},

    	textoCantidadPedido:{
    		color: '#fff',
    		fontSize: 15,
    		textAlign: 'center'
    	},

    	cantidadproductos: {
    		width: 30,
    		height: 30,
    		backgroundColor: '#50dddd',
    		alignItems: 'center',
    		justifyContent: 'center',
    		borderRadius: 25,
    		marginLeft: 10,
    		marginBottom: 10
    	}

    });
