import React, {Component} from 'react';
import {AsyncStorage, Keyboard, TextInput, StyleSheet, View, Dimensions, Text, Platform, Linking, StatusBar, TouchableOpacity, Image} from 'react-native';

import LogoTextWhite from "../components/logos/LogoTextWhite"
import FormButton from "../components/form/FormButton"
import FormHead from "../components/form/FormHead"
import MapView from 'react-native-maps';
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import Geocoder from 'react-native-geocoding';

Geocoder.init('AIzaSyAKwCnkuKfwRNZUEDhXGDM6FHzsM9AD-Js'); 

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});


const {width, height} = Dimensions.get('window');

export default class Welcome extends Component {

  constructor(props){
    super(props);
    this.onPress = this.onPress.bind(this);
    this.registrar = this.registrar.bind(this);
    this._keyboardDidShow = this._keyboardDidShow.bind(this);
    this._keyboardDidHide = this._keyboardDidHide.bind(this);
    this.state = {
      region: {
        latitude: null,
        longitude: null,
        latitudeDelta: null,
        longitudeDelta: null
      },

      tecladoactivo: false,

      direccion: '',
      notas: '',
      width: width

    }
  }

  registrar(){
    if(this.state.direccion.trim() == ''){
      alert('Debe ingresar una direccion');
      return;
    }
    this.props.navigation.goBack();
    this.props.navigation.state.params.onSelect({region: this.state.region, direccion: this.state.direccion, notas: this.state.notas});
  }

  calcDelta(lat, lon, acc){
    const oneDegreeOfLongitudeInMeters = 111.32;
    const circunference = (40075/360);

    const latDelta = 0.0122;
    const lonDelta = 0.0121;

    this.setState({
      region: {
        latitude: lat,
        longitude: lon,
        latitudeDelta: latDelta,
        longitudeDelta: lonDelta

      }
    });

  }

  componentDidMount(){
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow(){
    this.setState({tecladoactivo: true});
  }

  _keyboardDidHide(){
    this.setState({tecladoactivo: false});
  }

  componentWillMount(){
    
    

    if(Platform.OS === 'android'){
      LocationServicesDialogBox.checkLocationServicesIsEnabled({
        message: "<h2>Activar localización ?</h2>Traigo App utiliza la configuración de tu dispositivo<br/><br/>",
        ok: "Permitir",
        cancel: "Denegar",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
            preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
            providerListener: true // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
          }).then(function(success) {
            navigator.geolocation.getCurrentPosition((position) => {
              const lat = position.coords.latitude;
              const lon = position.coords.longitude;
              const accuracy = position.coords.accuracy;
              this.calcDelta(lat, lon, accuracy);

              Geocoder.from(lat, lon)
              .then(json => {
                var addressComponent = json.results[0].formatted_address
                this.setState({direccion: addressComponent});
              })
              .catch(error => console.warn(error));
            }, (error) => console.log(JSON.stringify(error)),
            {enableHighAccuracy: Platform.OS != 'android', timeout: 20000});

          }.bind(this)
          ).catch((error) => {
            this.calcDelta(1, 1, 1);
            console.log(error.message);
          });
        }else{
          navigator.geolocation.getCurrentPosition((position) => {
              const lat = position.coords.latitude;
              const lon = position.coords.longitude;
              const accuracy = position.coords.accuracy;
              this.calcDelta(lat, lon, accuracy);

              Geocoder.from(lat, lon)
              .then(json => {
                var addressComponent = json.results[0].formatted_address
                this.setState({direccion: addressComponent});
              })
              .catch(error => console.warn(error));


            }, (error) => console.log(JSON.stringify(error)),
            {enableHighAccuracy: Platform.OS != 'android', timeout: 20000});
        }

      }

      marker(){
        return {
          latitude: this.state.region.latitude,
          longitude: this.state.region.longitude
        }
      }

      onPress(event) {
        var region = JSON.parse(JSON.stringify(this.state.region));
        region.latitude = event.nativeEvent.coordinate.latitude;
        region.longitude = event.nativeEvent.coordinate.longitude;

        Geocoder.from(event.nativeEvent.coordinate)
        .then(json => {
          var addressComponent = json.results[0].formatted_address;
          this.setState({direccion: addressComponent});
        })
        .catch(error => console.warn(error));

        this.setState({region: region})
        
      }

      render() {
        return (



          <View style = {[styles.container]}>

         


          {this.state.region.latitude != null ? 

            <MapView
            provider={MapView.PROVIDER_GOOGLE}
            style = {[{width: this.state.width}, {height: (this.state.tecladoactivo ? height * 0.3 : height * 0.65)}]}
            initialRegion = {this.state.region}
            onPress={this.onPress}
            onMapReady = {() => this.setState({width: width - 1})}
            showsMyLocationButton = {true}
            showsUserLocation = {true}
            >

            <MapView.Marker

            coordinate={this.marker()}
            pinColor='#50dddd'
            
            

            />

            </MapView>
            : null
          }

           <TouchableOpacity

              style = {{alignItems: 'flex-start', flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 0}}

              onPress = {() => {
                  Keyboard.dismiss();
                  this.props.navigation.dispatch(popAction);
                } }
            >

            <View style = {{width: 30, height: 30, marginTop: 20}}>
             <Image
              style = {styles.image}
              source = {require('../resources/socialIcons/xblack.png')}
              resizeMode = 'contain'
              />
            </View>

          </TouchableOpacity>



          <View style = {[styles.inputdown, {width: (Dimensions.get('window').width * 0.8), height: 50}]}>
            <TextInput
              selectionColor={'#8b8b8b'}
              placeholder = {'Dirección'}
              placeholderTextColor = '#8b8b8b'
              underlineColorAndroid='transparent'
              keyboardType = {this.props.type}
              onChangeText = {(text) => {this.setState({direccion: text})}}
              style = {{flex: 1,  fontSize: 18}}
              value = {this.state.direccion}
            >
            </TextInput>
          </View>

          <View style = {[styles.inputdown, {width: (Dimensions.get('window').width * 0.8), height: 50}]}>
          <TextInput
          selectionColor={'#8b8b8b'}
          placeholder = {'Notas de la dirección ejm. Piso, apartamentos, etc'}
          placeholderTextColor = '#8b8b8b'
          underlineColorAndroid='transparent'
          keyboardType = {this.props.type}
          onChangeText = {(text) => {this.setState({notas: text})}}
          style = {{flex: 1,  fontSize: 18}}
          
          >
          </TextInput>
          </View>


          <TouchableOpacity
          style = {{marginTop: 10}}
          onPress = {this.registrar}>

          <WidthButton
          text = {'iUsar esta dirección'}
          color = {'#50dddd'}
          />

          </TouchableOpacity>





          </View>
          );
      }
    }

    const styles = StyleSheet.create({
      container: {
        backgroundColor: '#fff',
        flex:1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingTop: 0
      },

      form: {
        marginBottom: 10
      },


      form: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
      },

      seleccionar: {
        backgroundColor: '#50dddd',
        flex:1,
        width: width,
        alignItems: 'center',
        justifyContent: 'center'
      },

      textoseleccionar: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold'
      },

      input: {
        position: 'absolute',
        top: 40,
        paddingRight: 10,
        paddingLeft: 10,
        flexDirection: 'row',
        marginBottom: 4,
        borderRadius: 10,
        backgroundColor: '#fff'
      },

      inputdown: {
        marginTop: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        borderRadius: 10,
        backgroundColor: '#fff'
      },

      image: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
      }

    });
