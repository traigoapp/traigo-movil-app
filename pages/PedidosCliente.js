import React, {Component} from 'react';
import {Alert, RefreshControl, AsyncStorage, Switch, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import {StackActions, NavigationActions} from 'react-navigation'
import Splash from './Splash';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

const {width, height} = Dimensions.get('window');

const popAction = StackActions.pop({
  n: 1,
});

export default class pedidosRider extends Component {

  constructor(props){
    super(props);

    this.state = {activado: false, usuario: null, tomarPedidos: false, pedidos: [], refreshing: false, costos: {}, paradas: []};
    this.refrescarPedidos = this.refrescarPedidos.bind(this);
    this.obtenerTotalPedidos = this.obtenerTotalPedidos.bind(this);

  }

  _onRefresh = () => {

    this.refrescarPedidos();
    this.setState({refreshing: true});
    

    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
      this.setState({refreshing: false});
    })
    .catch((error) => {
       this.setState({refreshing: false});
      console.error(error);
    });
  }

  obtenerTotalPedidos(pedidos){
    return new Promise((resolve, reject) => {
      let fetches = [];
      let totales = [];
      
      for(let i = 0; i < pedidos.length; i++){
        
        if(pedidos[i].type == 'tienda'){
          fetches.push(
            fetch('http://traigo.com.co:8000/servicios/obtenertotalpedidocliente?idpedido=' + pedidos[i].idpedido_clientes)
            .then((response) => response.json())
            .then((responseJson) => {
              totales[i] = responseJson.total;
            })
            .catch((error) => {
              alert('Ha ocurrido un error con la conexión: ' + error);
            })
          )
          
        }else{
          fetches.push(totales[i] = 0)
        }
      }

    

      Promise.all(fetches).then(() => {
        resolve(totales);
      })

    });
  }


  refrescarPedidos(){
    fetch('http://traigo.com.co:8000/servicios/obtenerpedidosencurso?correo=' + this.state.usuario.correo)
    .then((response) => response.json())
    .then((responseJson) => {
      this.obtenerTotalPedidos(responseJson).then((totales) => {
        for(var i = 0; i < responseJson.length; i++){
          if(responseJson[i].type == 'tienda'){
            responseJson[i].total = totales[i];
          }
        }
        this.setState({pedidos: responseJson});
      }).catch((err) => console.log(err));
    })
    .catch((error) => {
      alert('Ha ocurrido un error con la conexión: ' + error);
    });
  }

componentWillMount(){
    
    this.setState({usuario: this.props.navigation.getParam('usuario', 'Ninguno')}, () => {
      this.refrescarPedidos();
    });
    
  
    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    }); 


    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
      this.setState({refreshing: false});
    })
    .catch((error) => {
       this.setState({refreshing: false});
      console.error(error);
    });
}


     

      renderPedido(item, index){
        if(item.type == 'deseo'){
          return(
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {
              this.props.navigation.navigate('infoPedidoDeseo', {usuario: this.state.usuario, pedido: item});
            }}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  LO QUE QUIERAS
                </Text>
                
              </View>

              <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                <View style = {styles.estadopedido}>
                  <Text style = {styles.textoestadopedido}>
                    {item.estado}
                  </Text>
                </View>
              </View>

            <View style = {{backgroundColor: '#afafaf', width: width * 0.8, height: 1, alignSelf: 'center', marginTop: 10}}>
                   </View>

              <Text style = {styles.datafono}>
                Tu pedido:
              </Text>

              <Text style = {styles.descripcionPedido}>
                {item.deseo}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 40, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                    {item.direccionentrega + (item.notasdireccionentrega.trim() != '' ? ' | ' + item.notasdireccionentrega : '')}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'mandado'){
          item.paradas = [];
          for(var i = 0; i < this.state.paradas.length; i++){

            if(this.state.paradas[i].idpencomienda == item.idpedido_encomienda){
              item.paradas.push(this.state.paradas[i]);
            }
          }


          return(
              
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {this.props.navigation.navigate('infoPedidoEncomienda', {usuario: this.state.usuario, pedido: item, paradas: this.state.paradas});}}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  MANDADO
                </Text>
              </View>

              <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                <View style = {styles.estadopedido}>
                  <Text style = {styles.textoestadopedido}>
                    {item.estado}
                  </Text>
                </View>
              </View>

            <View style = {{backgroundColor: '#afafaf', width: width * 0.8, height: 1, alignSelf: 'center', marginTop: 10}}>
                   </View>



              <Text style = {styles.datafono}>
                Mandado
              </Text>



              <Text style = {styles.descripcionPedido}>
                {item.mandado}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 40, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de recogida
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionorigen + (item.direccionorigen.trim() != '' ? ' | ' + item.direccionorigen : '')}
                  </Text>
                </View>
              </View>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 40, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direcciondestino + (item.notasdirecciondestino.trim() != '' ? ' | ' + item.notasdirecciondestino : '')}
                  </Text>
                </View>
              </View>

               <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 40, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Cantidad de paradas adicionales
                  </Text>
                  <Text style = {styles.textoDireccion}>
                    {item.paradas.length}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'tienda'){
          return(
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {
              this.props.navigation.navigate('infoPedidoTienda', {usuario: this.state.usuario, pedido: item});
            }}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  {item.nombresucursal.toUpperCase()}
                </Text>
              </View>

              <View style = {{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start'}}>
                <View style = {styles.estadopedido}>
                  <Text style = {styles.textoestadopedido}>
                    {item.estado}
                  </Text>
                </View>
              </View>

            <View style = {{backgroundColor: '#afafaf', width: width * 0.8, height: 1, alignSelf: 'center', marginTop: 10}}>
                   </View>

              <Text style = {styles.datafono}>
                {'Efectivo'}
              </Text>

              <Text style = {styles.precio}>
                ${item.total}
              </Text>

              <Text style = {styles.datafono}>
                {'Costo de envío'}
              </Text>

              <Text style = {styles.precio}>
                ${item.costo_envio}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 40, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccion + (item.notas.trim() != '' ? ' | ' + item.notas : '')}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }
      }

      render() {
        return(


          <View style = {styles.container}>
          <View style = {styles.header}>

          <View style = {styles.divileft}>

          <TouchableOpacity
          onPress = {() => {
            this.props.navigation.dispatch(StackActions.popToTop());
          }}>
          <Image
          style = {{width: 30, height: 30}}
          source = {require('../resources/socialIcons/back.png')}
          resizeMode = 'center'
          />
          </TouchableOpacity>


          </View>

          <View style = {styles.divi}>
          <Image
          style = {{width: width * 0.25, height: 50}}
          source = {require('../resources/logos/traigoblack.png')}
          resizeMode = 'center'
          />
          </View>

          <View style = {styles.diviright}>

          <Text style = {styles.encurso}>
          Hola, {this.state.usuario == null ? 'Sin nombre' : this.state.usuario.nombre}
          </Text>


          </View>
          
          </View>

          <View style = {{backgroundColor: '#e5e5e5', width: width, height: 1, alignSelf: 'center'}}>
                   </View>

          <View style = {styles.scroll}>


          <View style = {styles.activate}>
         

          <View style = {styles.holausuario}>
          <Text style = {{fontSize: 25, fontWeight: 'bold', color: '#363530', textAlign: 'center'}}>
            Tus pedidos en curso
          </Text>

         

          </View>


          </View>

       



            <View style = {styles.flatlist}>

            <FlatList 
            refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
              />}
              data = {this.state.pedidos}
              renderItem = {({item, index}) => this.renderPedido(item, index)}
              keyExtractor = {(item, index) => index.toString()}
              />


              </View>


            </View>

            </View>



            )
      }
    }

    const styles = StyleSheet.create({
      container: {
        flex:1,
        alignItems: 'flex-start',
        paddingTop: Platform.OS === 'ios'  ? 20 : 0,
        backgroundColor: '#ffffff'
      },

      header: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 60,
        width: width,
        alignItems: 'center',
        justifyContent: 'space-between'
      },

      traigo: {
        color: '#363530',
        fontSize: 25,
        fontWeight: 'bold'
      },

      divi: {
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      divileft: {
        paddingLeft: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      diviright: {
        paddingRight: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
      },

      scroll: {
        backgroundColor: '#fff',
        flex: 1,
        width: width,
        paddingBottom: 5
      },

      activate: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      holausuario: {
        alignItems: 'center',
        justifyContent: 'center'
      },

      terminos: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
      },

      flatlist: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        width: width,
      },

      pedido: {
        width: width * 0.8,
        borderRadius: 15,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginVertical: 5,
        borderColor: '#afafaf',
        borderWidth: 1
      },

      infotienda: {
       
        width: width * 0.8,
        flexDirection: 'row',
        paddingTop: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start'
      },

      titulosucursal: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'left',

      },

      tiempoentrega: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'right',
        flex: 1,
      },

      partner: {
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#363530',
        justifyContent: 'center',
        alignItems: 'center'
      },

      datafono: {
        paddingLeft: 10,
        color: '#afafaf',
        fontSize: 15,
        textAlign: 'left'
      },

      precio: {
        marginTop: -5,
        paddingLeft: 10,
        color: '#363530',
        fontSize: 20,
        textAlign: 'left'
      },

      destino: {
        paddingLeft: 30,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row'
      },

      textoDestino: {
        fontSize: 15,
        color: '#363530',
        fontWeight: 'bold',
        textAlign: 'left'
      },

      textoDireccion: {
        marginTop: -5,
        fontSize: 15,
        color: '#afafaf',
        textAlign: 'left',
        marginRight: 10
      },

      retenido: {
        marginTop: -10,
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#fba312',
        justifyContent: 'center',
        alignItems: 'center'
      },

      infoganancia: {
        height: 30,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start',
      },


      descripcionPedido:{
        marginTop: -5,
        paddingLeft: 10,
        paddingRight: 5,
        color: '#363530',
        fontSize: 15,
        textAlign: 'left'
      },

      textoCantidadPedido:{
        color: '#fff',
        fontSize: 15,
        textAlign: 'center'
      },

      cantidadproductos: {
        width: 30,
        height: 30,
        backgroundColor: '#50dddd',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginLeft: 10,
        marginBottom: 10
      },

      estadopedido: {
        marginLeft: 10,
        borderRadius: 25,
        backgroundColor: '#50dddd',
        paddingHorizontal: 8,
        paddingVertical: 2,
        alignItems: 'center',
        justifyContent: 'center',
      },

      textoestadopedido: {
        color: '#fff',
        fontSize: 15,
      }

    });
