import React, {Component} from 'react';
import {Alert, RefreshControl, AsyncStorage, Switch, ScrollView, FlatList, Image, ImageBackground, StyleSheet, View, Dimensions, Text, Platform, Linking, TouchableOpacity, Button, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import SocialButton from "../components/button/SocialButton"
import NormalButton from "../components/button/NormalButton"
import {StackActions, NavigationActions} from 'react-navigation'
import Splash from './Splash';
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";

const {width, height} = Dimensions.get('window');

const popAction = StackActions.pop({
  n: 1,
});

export default class pedidosRider extends Component {

  constructor(props){
    super(props);

    this.state = {costos: {}, activado: false, rider: null, tomarPedidos: false, pedidos: [], refreshing: false, costos: {}, paradas: []};
    this.refrescarPedidos = this.refrescarPedidos.bind(this);
    this.obtenerTotalPedidos = this.obtenerTotalPedidos.bind(this);

  }

  _onRefresh = () => {

    this.refrescarPedidos();
    this.setState({refreshing: true});
    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
      this.setState({refreshing: false});
    })
    .catch((error) => {
       this.setState({refreshing: false});
      console.error(error);
    });
  }

    obtenerTotalPedidos(pedidos){
    return new Promise((resolve, reject) => {
      let fetches = [];
      let totales = [];
      
      for(let i = 0; i < pedidos.length; i++){
        
        if(pedidos[i].type == 'tienda'){
          fetches.push(
            fetch('http://traigo.com.co:8000/servicios/obtenertotalpedidocliente?idpedido=' + pedidos[i].idpedido_clientes)
            .then((response) => response.json())
            .then((responseJson) => {
              totales[i] = responseJson.total;
            })
            .catch((error) => {
              alert('Ha ocurrido un error con la conexión: ' + error);
            })
          )
          
        }else{
          fetches.push(totales[i] = 0)
        }
      }

    

      Promise.all(fetches).then(() => {
        resolve(totales);
      })

    });
  }


  refrescarPedidos(){
    fetch('http://traigo.com.co:8000/servicios/obtenerpedidosrider?cedula=' + this.state.rider.cedula)
    .then((response) => response.json())
    .then((responseJson) => {
      this.obtenerTotalPedidos(responseJson).then((totales) => {
        for(var i = 0; i < responseJson.length; i++){
          if(responseJson[i].type == 'tienda'){
            responseJson[i].total = totales[i];
          }
        }
        this.setState({pedidos: responseJson});
      }).catch((err) => console.log(err));
    })
    .catch((error) => {
      alert('Ha ocurrido un error con la conexión');
    });
  }

componentWillMount(){
    
    this.setState({rider: this.props.navigation.getParam('rider', 'Ninguno')}, () => {
      this.refrescarPedidos();
    });
    
  
    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    }); 


    fetch('http://traigo.com.co:8000/servicios/obtenerparadas')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({paradas: responseJson});
      this.setState({refreshing: false});
    })
    .catch((error) => {
       this.setState({refreshing: false});
      console.error(error);
    });
}


     

      renderPedido(item, index){
        if(item.type == 'deseo'){
          return(
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {
              this.props.navigation.navigate('pedidoDeseo', {rider: this.state.rider, pedido: item});
            }}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  LO QUE QUIERAS!
                </Text>

                <View>
                  <Text style = {styles.tiempoentrega}>
                    45 min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
              </View>

              <Text style = {styles.datafono}>
                El cliente quiere:
              </Text>

              <Text style = {styles.descripcionPedido}>
                {item.deseo}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Destino
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionentrega + ' | ' + item.notasdireccionentrega}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costo_envio}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'mandado'){
          item.paradas = [];
          for(var i = 0; i < this.state.paradas.length; i++){

            if(this.state.paradas[i].idpencomienda == item.idpedido_encomienda){
              item.paradas.push(this.state.paradas[i]);
            }
          }


          return(
              
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {this.props.navigation.navigate('pedidoEncomienda', {rider: this.state.rider, pedido: item, paradas: this.state.paradas});}}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  MANDADO
                </Text>

                <View>
                  <Text style = {styles.tiempoentrega}>
                    45 min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
              </View>

              <Text style = {styles.datafono}>
                Mandado
              </Text>

              <Text style = {styles.descripcionPedido}>
                {item.mandado}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de recogida
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionorigen + ' | ' + item.notasdireccionorigen}
                  </Text>
                </View>
              </View>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direcciondestino + ' | ' + item.notasdirecciondestino}
                  </Text>
                </View>
              </View>

               <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Cantidad de paradas adicionales
                  </Text>
                  <Text style = {styles.textoDireccion}>
                    {item.paradas.length}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + (item.costo_envio + item.paradas.length * this.state.costos.costo_paradas_mandado) * (100 - item.comision_rider) / 100}
                  </Text>
                </View>
              </View>



            </TouchableOpacity>
            )
        }else if(item.type == 'tienda'){
          return(
            <TouchableOpacity style = {styles.pedido}
            onPress = {() => {
              this.props.navigation.navigate('pedidoTienda', {rider: this.state.rider, pedido: item});
            }}>

            <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                  {item.nombresucursal.toUpperCase()}
                </Text>

                
                <View>
                  <Text style = {styles.tiempoentrega}>
                    {item.tiempo_entrega} min
                  </Text>

                  <View style = {styles.app}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      app
                    </Text>
                  </View>
                </View>
                
              </View>

              <Text style = {styles.datafono}>
                {'Efectivo'}
              </Text>

              <Text style = {styles.precio}>
                ${item.total}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Dirección de entrega
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccion + ' | ' + item.notas}
                  </Text>
                </View>
              </View>


              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costo_envio * (1 - item.comision_rider / 100)}
                  </Text>
                </View>
              </View>

            </TouchableOpacity>
            )
        }else if(item.type == 'empresariales'){

          return(
            <TouchableOpacity style = {styles.pedido}
                  onPress = {() => {
                    this.props.navigation.navigate('pedidoEmpresarial', {rider: this.state.rider, pedido: item});
                  }}>
              <View style = {styles.infotienda}>
                <Text style = {styles.titulosucursal}>
                {item.nombresucursal.toUpperCase()}
                </Text>
                
                <View>
                  <Text style = {styles.tiempoentrega}>
                    {item.tiempo_entrega} min
                  </Text>

                  <View style = {styles.partner}>
                    <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 12}}>
                      partner
                    </Text>
                  </View>
                </View>

              </View>


              


              <Text style = {styles.datafono}>
                {item.datafono == 'N' ? 'Efectivo' : 'Datáfono'}
              </Text>

              <Text style = {styles.precio}>
                ${item.valor}
              </Text>

              <View style = {styles.destino}>

              <View style = {{width: 1, backgroundColor: '#afafaf', height: 50, marginRight: 5}}/>

                <View>
                  <Text style = {styles.textoDestino}>
                  Destino
                  </Text>
                  <Text style = {styles.textoDireccion}>
                  {item.direccionentrega + ' | ' + item.notasdireccionentrega}
                  </Text>
                </View>
              </View>

              <View style = {{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text style = {{paddingHorizontal: 10, color: '#363530', fontWeight: 'bold', textAlign: 'left', width: width * 0.8}}>
                  Ganas al finalizar
                </Text>

                <View style = {styles.infoganancia}>
                  <Text style = {{color: '#363530', fontWeight: 'bold', marginTop: -10, fontSize: 20}}>
                    {'$' + item.costoenvio * (1 - item.comision_rider / 100)}
                  </Text>

                  {item.retiene == 'S' ? 

                    <View style = {styles.retenido}>
                        <Text style = {{color: '#fff', alignSelf: 'center', fontSize: 10}}>
                          Retenido
                        </Text>
                    </View>
                  : null}
                 

                </View>
              </View>

            </TouchableOpacity>
            )

        }
      }

      render() {
        return(


          <View style = {styles.container}>
          <View style = {styles.header}>

          <View style = {styles.divileft}>

          <TouchableOpacity
          onPress = {() => {
            this.props.navigation.goBack();
          }}>
          <Image
          style = {{width: 30, height: 30}}
          source = {require('../resources/socialIcons/back.png')}
          resizeMode = 'center'
          />
          </TouchableOpacity>


          </View>

          <View style = {styles.divi}>
          <Image
          style = {{width: width * 0.25, height: 50}}
          source = {require('../resources/logos/traigoblack.png')}
          resizeMode = 'center'
          />
          </View>

          <View style = {styles.diviright}>

          <Text style = {styles.encurso}>
          Hola, {this.state.rider == null ? 'Sin nombre' : this.state.rider.nombre}
          </Text>


          </View>
          
          </View>

          <View style = {{backgroundColor: '#e5e5e5', width: width, height: 1, alignSelf: 'center'}}>
                   </View>

          <View style = {styles.scroll}>


          <View style = {styles.activate}>
         

          <View style = {styles.holarider}>
          <Text style = {{fontSize: 25, fontWeight: 'bold', color: '#363530', textAlign: 'center'}}>
            Tus pedidos por completar
          </Text>

         

          </View>


          </View>

       



            <View style = {styles.flatlist}>

            <FlatList 
            refreshControl={
              <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
              />}
              data = {this.state.pedidos}
              renderItem = {({item, index}) => this.renderPedido(item, index)}
              keyExtractor = {(item, index) => index.toString()}
              />


              </View>


            </View>

            </View>



            )
      }
    }

    const styles = StyleSheet.create({
      container: {
        flex:1,
        alignItems: 'flex-start',
        paddingTop: Platform.OS === 'ios'  ? 20 : 0,
        backgroundColor: '#ffffff'
      },

      header: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        height: 60,
        width: width,
        alignItems: 'center',
        justifyContent: 'space-between'
      },

      traigo: {
        color: '#363530',
        fontSize: 25,
        fontWeight: 'bold'
      },

      divi: {
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      divileft: {
        paddingLeft: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
      },

      diviright: {
        paddingRight: 10,
        width: width * 0.33,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
      },

      scroll: {
        backgroundColor: '#fff',
        flex: 1,
        width: width,
        paddingBottom: 5
      },

      activate: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      },

      holarider: {
        marginLeft: 10,
      },

      terminos: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
      },

      flatlist: {
        marginTop: 10,
        flex: 1,
        backgroundColor: '#fff',
        width: width,
      },

      pedido: {
        width: width * 0.8,
        borderRadius: 15,
        backgroundColor: '#fff',
        alignSelf: 'center',
        marginVertical: 5,
        borderColor: '#afafaf',
        borderWidth: 1,
      },

      infotienda: {
       
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingRight: 5,
        paddingLeft: 5,
        justifyContent: 'flex-start'
      },

      titulosucursal: {
        width: width * 0.6,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'left',

      },

      tiempoentrega: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#363530',
        textAlign: 'center',
        flex: 1,
      },

      partner: {
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#363530',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 2
      },

      app: {
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#50dddd',
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 2
      },

      datafono: {
        paddingLeft: 10,
        color: '#afafaf',
        fontSize: 15,
        textAlign: 'left'
      },

      precio: {
        marginTop: -5,
        paddingLeft: 10,
        color: '#363530',
        fontSize: 20,
        textAlign: 'left'
      },


      descripcionPedido:{
        marginTop: -5,
        paddingLeft: 10,
        paddingRight: 5,
        color: '#363530',
        fontSize: 15,
        textAlign: 'left'
      },

      destino: {
        paddingLeft: 30,
        height: 60,
        alignItems: 'center',
        flexDirection: 'row'
      },

      textoDestino: {
        fontSize: 15,
        color: '#363530',
        fontWeight: 'bold',
        textAlign: 'left'
      },

      textoDireccion: {
        marginTop: -5,
        fontSize: 15,
        color: '#afafaf',
        textAlign: 'left',
        marginRight: 10
      },

      retenido: {
        marginTop: -10,
        marginLeft: 2,
        width: width * 0.15,
        height: 15,
        borderRadius: 25,
        backgroundColor: '#fba312',
        justifyContent: 'center',
        alignItems: 'center'
      },

      infoganancia: {
        height: 30,
        width: width * 0.8,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        paddingHorizontal: 10,
        justifyContent: 'flex-start',
      },

      textoCantidadPedido:{
        color: '#fff',
        fontSize: 15,
        textAlign: 'center'
      },

      cantidadproductos: {
        width: 30,
        height: 30,
        backgroundColor: '#50dddd',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginLeft: 10,
        marginBottom: 10
      }

    });
