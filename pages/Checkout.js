import React, {Component} from 'react';
import {Alert, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});




export default class Checkout extends Component {

  constructor(props){
    super(props);

    this.state = {usuario: null, carrito: [], direccion: null, totalapagar: 0, costos: {}, direccionEntrega: null, loading: false};

  }

  componentWillMount () {

    fetch('http://traigo.com.co:8000/servicios/obtenercostos')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({costos: responseJson});
    })
    .catch((error) => {
      console.error(error);
    });

    var usuario = this.props.navigation.getParam('usuario', null);
    var direccion = this.props.navigation.getParam('direccion', null);
    var totalapagar = this.props.navigation.getParam('totalapagar', null);
    var carrito = this.props.navigation.getParam('carrito', null);

    this.setState({
      usuario: usuario,
      direccionEntrega: direccion,
      totalapagar: totalapagar,
      carrito: carrito
    });
  }

   onSelect = data => {
    this.setState(data);
  }


  realizarPedido(){
   

    this.setState({loading: true});

    var data = new FormData();
    data.append('carrito', JSON.stringify(this.state.carrito));
    data.append('correocliente', this.state.usuario.correo);
    data.append('costo_envio', this.state.costos.costo_envio_cliente);
    data.append('comision_rider', this.state.costos.comision_rider_cliente);
    data.append('iddireccion', this.state.direccionEntrega.iddireccion);


    fetch('http://traigo.com.co:8000/servicios/realizarpedidocliente', {

      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.code){
          alert('Ha ocurrido un error con la conexión')
          this.setState({loading: false});
        }else{
          this.setState({loading: false});
          Alert.alert(
            'Pedido realizado',
            'El pedido está siendo procesado, para mas información puede ver los pedidos en curso',
            [
          
              {text: 'OK', onPress: () => {
                this.props.navigation.dispatch(resetAction)
              }},
            ],
            { cancelable: false }
          )
        }
      })
      .catch((error) => {
        this.setState({loading: false});
        alert('Ha ocurrido un error con la conexión: ' + error);
      });
  }

  render() {
    return (
      <View style = {styles.container}>
        <View style = {styles.header}>

          <View style = {styles.divi2}>

            <TouchableOpacity
              onPress = {() => {
                this.props.navigation.dispatch(popAction);
              }}
            >
              <Image
              style = {{width: 30, height: 30}}
              source = {require('../resources/socialIcons/back.png')}
              resizeMode = 'center'
              />
            </TouchableOpacity>

          
          </View>

           <View style = {styles.divi}>
             <Text style = {styles.nombreCliente}>
                Revisión del pedido
             </Text>
          </View>
          
        </View>

        <ScrollView style = {styles.scroll}
         contentContainerStyle = {{justifyContent: 'space-between', alignItems: 'center'}}>

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

           <Text style = {styles.tituloSeccion}>
              Dirección de entrega
            </Text>

          <TouchableOpacity style = {[styles.input, {width: (Dimensions.get('window').width * 0.9), height: 50 }]}
                onPress = {() => {
                  this.setState({direccion: 'entrega'}, () => {
                    this.props.navigation.navigate('selectDirection', {usuario: this.state.usuario, direccion: this.state.direccion, onSelect: this.onSelect});
                  });
                  
                }}>
          
             <View style = {styles.icon}>
               <Image
                  style = {{width: 30, height: 30, flex: 1}}
                  source = {require('../resources/socialIcons/location.png')}
                  resizeMode = 'center'
                />
              </View>

              <View style = {styles.textParadas}>
                <Text style = {{fontSize: 15, color: '#8b8b8b'}}>
                    {this.state.direccionEntrega == null ? 'Dirección de entrega' : this.state.direccionEntrega.direccion}
                </Text>
              </View>
            </TouchableOpacity>


         <View style = {styles.resumen}>
          <View style = {{flexDirection: 'row', width: width * 0.9, justifyContent: 'flex-end'}}>
            <Text style = {styles.total}>
              Total a pagar: 
            </Text>

            <Text style = {styles.totalnum}>
              {' $' + this.state.totalapagar}
            </Text> 
          </View>

          <TouchableOpacity style = {styles.revisar}
            onPress = {() => {
              this.realizarPedido();
            }}
          >
            <Text style = {styles.textoBoton}>
              Realizar pedido
            </Text>
          </TouchableOpacity>

        </View>

        {this.state.loading ? <View style = {styles.indicator}>
              <ActivityIndicator size="large" color="#53c678" />
            </View> : null}

         <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1}}>
          </View>

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex:1,
    alignItems: 'center',
    justifyContent: 'flex-start', 
    paddingTop: Platform.OS === 'ios'  ? 20 : 0
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },

  nombreCliente: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  form: {
    marginBottom: 10
  },

   hi: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#363530',
    marginBottom: 15
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginBottom: 4,
    borderRadius: 15,
    backgroundColor: '#e5e5e5'
  },

  row: {
    width: width,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: 20,
    flexDirection: 'row'
  },

  textRow: {
    paddingLeft: 20,
    fontSize: 25,
    color: '#363530'
  },

  remove: {
    width: 30,
    height: 30,
    borderRadius: 25,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center'
  },

  producto: {
    overflow: 'hidden',
    borderRadius: 10,
    backgroundColor: '#bebebe',
    width: height * 0.12,
    height: height * 0.12,
    marginHorizontal: 10
  },

  informacionRow: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1
  },

  nombreProducto: {
    fontSize: 20,
    color: '#afafaf'
  },

  precioProducto: {
    fontSize: 20,
    color: '#53c678'
  },

  resumen: {
    width: width * 0.9,
    paddingVertical: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  total: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'right',
    marginBottom: 10
  },

  totalnum: {
    fontSize: 25,
    textAlign: 'right',
    marginBottom: 10
  },

  revisar: {
    width: width * 0.9,
    borderRadius: 10,
    backgroundColor: '#53c678',
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },

  textoBoton: {
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },

  icon: {
    paddingHorizontal: 8,
    height: 50,
    alignItems: 'center',
    justifyContent:'center'
  },

  textParadas: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    textAlign: 'left'
  },

  input: {
    paddingRight: 10,
    flexDirection: 'row',
    marginTop: 0,
    borderRadius: 15,
    backgroundColor: '#efefef',
  },

  textinput: {
    color: '#363530', 
    fontSize: 15,
    flex: 1,
    textAlignVertical: 'top'
  },

  tituloSeccion: {
    marginTop: 20,
    marginBottom: 10, 
    width: width * 0.9,
    color: '#363530',
    fontWeight: 'bold',
    fontSize: 18
  },


});
