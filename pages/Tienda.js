import React, {Component} from 'react';
import {Alert, BackHandler,ImageBackground, RefreshControl, FlatList, AsyncStorage, ScrollView, ActivityIndicator, Keyboard, StyleSheet, View, Dimensions, Text, TextInput, Platform, Linking, KeyboardAvoidingView, Image, TouchableOpacity, StatusBar} from 'react-native';

import LogoText from "../components/logos/LogoText"
import FormButton from "../components/form/FormButton"
import FormInput from "../components/form/FormInput"
import FormHead from "../components/form/FormHead"
import ButtonWhite from "../components/button/ButtonWhite"
import WidthButton from '../components/button/WidthButton'
import {StackActions, NavigationActions} from 'react-navigation'

const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({routeName: 'home'})],
});

const popAction = StackActions.pop({
  n: 1,
});

export default class Tienda extends Component {
  constructor(props){
      super(props);
      this.state = {productos: [], usuario: null, direccion: null, tienda: null, categoriasProductos: [], carrito: []};
    }

    componentWillMount(){
      this.setState({usuario: this.props.navigation.getParam('usuario', null)});
      this.setState({direccion: this.props.navigation.getParam('direccion', null)});
      this.setState({tienda: this.props.navigation.getParam('tienda', null)}, () => {
        fetch('http://traigo.com.co:8000/servicios/obtenerproductos?tienda=' + this.state.tienda.idsucursal)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({productos: responseJson}, () => {
              fetch('http://traigo.com.co:8000/servicios/obtenercategoriasproductos')
              .then((response) => response.json())
              .then((responseJson) => {
                var contador = 0;
                var categorias = [];
                for(var j = 0; j < responseJson.length; j++){
                  for(var i = 0; i < this.state.productos.length; i++){
                    if(this.state.productos[i].idcategoriaproducto == responseJson[j].idcategoriaproducto){
                      categorias.push(responseJson[j]);
                      break;
                    }
                  }
                }
                
                this.setState({categoriasProductos: categorias});
              })
              .catch((error) => {
                alert(error);
              });
            });
          })
          .catch((error) => {
            alert(error);
          });
      })
      
    }


    componentDidMount(){
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount(){
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }    

    handleBackPress = () => {

      if(this.state.carrito.length == 0){
        this.props.navigation.dispatch(popAction)
      }else{
        Alert.alert(
          'Atención',
          'Si regresa perderá su carrito de compra actual',
          [
            {text: 'Continuar en la tienda', onPress: () => console.log('Ask me later pressed')},
            {text: 'Salir', onPress: () => this.props.navigation.dispatch(popAction), style: 'cancel'},
          ],
          { cancelable: false }
        )
        return true;
      }

      
    }

    _onRefresh = () => {
    this.setState({refreshing: true});
    fetch('http://traigo.com.co:8000/servicios/obtenerproductos?tienda=' + this.state.tienda.idsucursal)
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({productos: responseJson}, () => {
              fetch('http://traigo.com.co:8000/servicios/obtenercategoriasproductos')
              .then((response) => response.json())
              .then((responseJson) => {
                var contador = 0;
                var categorias = [];
                for(var j = 0; j < responseJson.length; j++){
                  for(var i = 0; i < this.state.productos.length; i++){
                    if(this.state.productos[i].idcategoriaproducto == responseJson[j].idcategoriaproducto){
                      categorias.push(responseJson[j]);
                      break;
                    }
                  }
                }
                
                this.setState({categoriasProductos: categorias});
                this.setState({refreshing: false});
              })
              .catch((error) => {
                this.setState({refreshing: false});
                alert(error);
              });
            });
          })
          .catch((error) => {
            this.setState({refreshing: false});
            alert(error);
          });
  }


  onSelect = data => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    var carrito = this.state.carrito;
    if(data.producto != null){
      var producto = JSON.parse(JSON.stringify(data.producto));
      carrito.push(producto);

      this.setState({carrito: carrito});
    }
  }


  actualizarCarrito = data => {
    this.setState(data);
  }



    renderProducto(item, index, productosCategoria){
      return(<TouchableOpacity
            onPress = {() => {
              BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
              this.props.navigation.navigate('producto', {usuario: this.state.usuario, producto: item, onSelect: this.onSelect, manejador: this.handleBackPress})
            }}
            style = {{paddingLeft: (index == 0 ? ((width - width * 0.9) / 2) : 2.5), paddingRight: (index == productosCategoria.length - 1 ? ((width - width * 0.9) / 2) : 2.5)   }}>
            
              <Image
                style = {styles.producto}
                source = {{uri: 'http://traigo.com.co:8000/static/img/producto/' + item.imagen}}>
              </Image>

               <View style = {styles.infoproducto}>

                    <Text style={styles.tituloproducto}>
                      {item.nombre}
                    </Text>

                    <Text style = {styles.precio}>
                      ${item.precio}
                   </Text>
                </View>
            

          </TouchableOpacity>)
    }

    renderCategoria(item, index){
      var now = new Date().toLocaleTimeString();
      var contador = 0;
      var productosCategoria = [];
      for(var i = 0; i < this.state.productos.length; i++){
        if(this.state.productos[i].idcategoriaproducto == item.idcategoriaproducto){
         
          contador++;
          productosCategoria.push(this.state.productos[i]);
        }
      }

      return(
          
          contador == 0 ? <View/> :
            <View>

                {index == 0 ? <View style = {styles.banner}>
                  <ImageBackground
                    style = {styles.imagenBanner}
                    source = {{uri: 'http://traigo.com.co:8000/static/img/tienda/' + this.state.tienda.imagen}}>
                    <View style = {styles.trans}>
                      {(this.state.tienda.abierto) ? <View style = {[styles.estado, {backgroundColor: '#53c678'}]}>
                            <Text style = {styles.textoEstado}>
                              ABIERTO
                            </Text>
                          </View> 
                          :
                        <View style = {[styles.estado, {backgroundColor: 'red'}]}>
                            <Text style = {styles.textoEstado}>
                              CERRADO
                            </Text>
                          </View>}

                      <Text style = {styles.nombreTienda}>
                        {this.state.tienda.nombre.toUpperCase()}
                      </Text>

                      <Text style = {styles.infotienda}>
                        {this.state.tienda.tiempo_entrega + ' MIN - $' + this.state.tienda.costo_envio + ' COP'}
                      </Text>

                    </View>
                  </ImageBackground>
                </View> : null}

               <Text style = {styles.textosecciones}>
                  {item.nombre}
                </Text>

                <View style = {styles.flatlistproductos}>

                  <FlatList
                    showsHorizontalScrollIndicator={false}
                    horizontal = {true}
                    data = {productosCategoria}
                    renderItem = {({item, index}) => this.renderProducto(item, index, productosCategoria)}
                    keyExtractor = {(item, index) => index.toString()}
                  />


                </View>
                <View style = {{backgroundColor: '#e5e5e5', width: width * 0.9, height: 1, marginVertical: 5, alignSelf: 'center'}}/>
          </View>
          
      )
    }

    render(){
      return(
      <View style = {styles.container}>
        <View style = {styles.header}>

            <View style = {styles.divi2}>

              <TouchableOpacity
                onPress = {() => {
                  this.handleBackPress();
                }}
              >
                <Image
                style = {{width: 30, height: 30}}
                source = {require('../resources/socialIcons/xblack.png')}
                resizeMode = 'center'
                />
              </TouchableOpacity>

            
            </View>

             <View style = {styles.divi}>
               <Text style = {styles.title}>
                  {this.state.tienda == null ? 'Sin nombre' : this.state.tienda.nombre}
               </Text>
            </View>

            <View style = {styles.divi2}>
              <TouchableOpacity onPress = {() => {
                if(this.state.carrito.length != 0)
                  this.props.navigation.navigate('carrito', {direccion: this.state.direccion, usuario: this.state.usuario, carrito: this.state.carrito, actualizarCarrito: this.actualizarCarrito})
              }}>
               <Image
                style = {{width: width * 0.25, height: 50}}
                source = {require('../resources/socialIcons/shoppingcart.png')}
                resizeMode = 'center'
              />
              </TouchableOpacity>
            </View>
            
          </View>

        <View style = {styles.flatlist}>

              <FlatList 
              refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />}
                data = {this.state.categoriasProductos}
                renderItem = {({item, index}) => this.renderCategoria(item, index)}
                keyExtractor = {(item, index) => index.toString()}
              />


          </View>
      </View>
      )
    }

}


const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'flex-start',
    paddingTop: Platform.OS === 'ios'  ? 20 : 0,
    backgroundColor: '#ffffff'
  },

  divi2: {
    paddingLeft: 5,
    width: width * 0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },

  divi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },

  header: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 60,
    width: width,
    alignItems: 'center',
    justifyContent: 'space-between'
  },

  title: {
    color: '#363530',
    fontSize: 25,
    fontWeight: 'bold'
  },

  flatlist: {
    flex: 1,
    backgroundColor: '#fff',
    width: width
  },

  tienda: {
    marginLeft: 4,
    marginRight: 4,
    marginVertical: 2,
    width: width - 8,
    height: height * 0.3
  },

  estado: {
    width: width * 0.2,
    height: 20,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textoEstado: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold'
  },

  nombre: {
    width: width * 0.7,
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 25
  },

  infotienda: {
    color: '#fff',
    fontSize: 15
  },

  flatlistproductos: {
    width: width,
    height: height * 0.25
  },

  textosecciones: {
    fontWeight: 'bold',
    paddingVertical: 8, 
    width: width * 0.9,
    textAlign: 'left',
    color: '#afafaf',
    fontSize: 25,
    marginLeft: (width - width * 0.9) / 2
  },

  producto: {
    overflow: 'hidden',
    borderRadius: 5,
    backgroundColor: '#bebebe',
    width: height * 0.18,
    height: height * 0.18
  },

   tituloproducto: {
    color: '#afafaf',
    fontSize: 15,
    fontWeight: 'bold',
    width: height * 0.18
  },

  descripcionproducto: {
    color: '#fff',
    fontSize: 10,
  },

   infoproducto: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  precio: {
    color: '#50dddd',
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'left'
  },

  banner: {
    width: width,
    height: height * 0.3
  },

  imagenBanner: {
    overflow: 'hidden',
    backgroundColor: '#bebebe',
    width: width,
    height: height * 0.3,
    alignItems: 'center',
    justifyContent: 'center'
  },


  trans: {
    borderRadius: 8,
    width: width,
    height: height * 0.3,
    backgroundColor: 'rgba(52, 52, 52, 0)',
    alignItems: 'center',
    justifyContent: 'center'
  },

  estado: {
    width: width * 0.2,
    height: 20,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },

  textoEstado: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold'
  },

  nombreTienda: {
    width: width * 0.7,
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 25
  },

  infotienda: {
    color: '#fff',
    fontSize: 15
  },


});